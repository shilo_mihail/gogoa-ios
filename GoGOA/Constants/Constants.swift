//
//  Constants.swift
//  GoGOA
//
//  Created by m.shilo on 31/01/2019.
//  Copyright © 2019 m.shilo. All rights reserved.
//

import UIKit

struct Constants {
    static let mobilePhoneLength = 12
    static let minNameLength = 1
    static let minPasswordLength = 6
    
    static let mobilePhoneCode = "+91"
    static let mobilePhoneMask = "+##(###)###-##-##"
    
    struct color {
        struct loginGroup {
            static let dark = UIColor(red: 80, green: 80, blue: 80)
            static let light = UIColor(red: 255, green: 255, blue: 255)
        }
        
        struct textField {
            static let defaultBorderColor = UIColor(red: 204, green: 204, blue: 204)
            static let warningBorderColor = UIColor.red
            
            static let activeItemColor = UIColor(red: 249, green: 194, blue: 71)
            static let nonActiveItemColor = UIColor.lightGray
        }
        
        struct background {
            static let dark = UIColor(red: 33, green: 35, blue: 46)
        }
        
        struct button {
            struct loginGroup {
                static let leftGragient = UIColor(red: 175, green: 122, blue: 141)
                static let rightGradient = UIColor(red: 192, green: 193, blue: 197)
                
                static let title = UIColor.white
            }
            struct profileGroup {
                static let leftGragient = UIColor(red: 250, green: 122, blue: 141)
                static let rightGradient = UIColor(red: 250, green: 193, blue: 197)
                
                static let title = UIColor.white
            }
            static let lightOnDark = UIColor(red: 233, green: 235, blue: 246)
        }
    }
    
    struct image {
        static var cart: UIImage {
            return UIImage(named: "cart")!
        }
        
        static var background: UIImage {
            return UIImage(named: "login_parallax")!
        }
        
        static var backArrow: UIImage {
            return UIImage(named: "back")!
        }
    }
    
    struct icon {
        static var location: UIImage {
            return UIImage(named: "icon_location")!
        }
        static var people: UIImage {
            return UIImage(named: "icon_people")!
        }
        static var time: UIImage {
            return UIImage(named: "icon_time")!
        }
        static var party: UIImage {
            return UIImage(named: "icon_party")!
        }
        static var user_man: UIImage {
            return UIImage(named: "icon_user_man")!
        }
        static var photo_cam: UIImage {
            return UIImage(named: "icon_photo_cam")!
        }
    }
}
