//
//  AppError.swift
//  GoGOA
//
//  Created by m.shilo on 18/06/2018.
//  Copyright © 2018 m.shilo. All rights reserved.
//

import UIKit

enum AppError: Error {
    
    case networkError
    case mappingError
    case authHeaderNotExist
    case generateVerificationCodeError
    case verificationCodeNotMatch
    case photoGalleryAuthError
    case serverError(String)
    
    var title: String {
        switch self {
        case .networkError:
            return "Упс! Сетевая ошибочка\nИспользуются устаревшие данные"
        case .mappingError:
            return "Упс! Плохие данные пришли от сервера"
        case .authHeaderNotExist:
            return "Упс! Отсутствует токен"
        case .generateVerificationCodeError:
            return "Упс! Ошибка при генерации кода"
        case .verificationCodeNotMatch:
            return "Упс! Проверочный код не совпадает.\nПопробуйте снова"
        case .serverError(let message):
            return message
        case .photoGalleryAuthError:
            return "Упс! Нет разрешения на пользование галереей фотографий"
        }
    }

}
