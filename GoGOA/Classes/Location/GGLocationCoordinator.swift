//
//  MapCoordinator.swift
//  GoGOA
//
//  Created by m.shilo on 05/02/2019.
//  Copyright © 2019 m.shilo. All rights reserved.
//

import UIKit
import RxSwift

enum LocationCoordinationResult {
    case back
}

class GGLocationCoordinator: BaseCoordinator<Void> {
    
    private let webService: WebService
    private let dataService: DataService
    private let navigationController: UINavigationController
    private let location: Location
    
    init(_ webService: WebService, _ dataService: DataService, _ navigationController: UINavigationController, _ location: Location) {
        self.webService = webService
        self.dataService = dataService
        self.navigationController = navigationController
        self.location = location
    }
    
    override func start() -> Observable<Void> {
        let viewController = GGLocationViewController.initFromXib()
        
        let viewModel = GGLocationViewModel(webService, dataService, location: location)
        viewController.viewModel = viewModel
        
        viewModel.output.selectedPhotoCellViewModel
            .flatMap { viewModel -> Observable<GGPhotoCoordinatorResult> in
                return self.showViewPhoto(on: viewController, with: viewModel.photo)
            }
            .subscribe()
            .disposed(by: disposeBag)
        
        navigationController.pushViewController(viewController, animated: true)
        
        return viewModel.output.back
            .take(1)
    }
    
    private func showViewPhoto(on parentViewController: UIViewController, with photo: Photo) -> Observable<GGPhotoCoordinatorResult> {
        let photoCoordinator = GGViewPhotoCoordinator(parentViewController, webService, dataService, photo)
        return coordinate(to: photoCoordinator)
    }
}
