//
//  MapViewController.swift
//  GoGOA
//
//  Created by m.shilo on 05/02/2019.
//  Copyright © 2019 m.shilo. All rights reserved.
//

import UIKit
import GoogleMaps
import RxGoogleMaps
import RxSwift
import RxCocoa

enum GGLocationInfoPosition: Equatable {
    case middle(height: CGFloat? = GGLocationInfoView.height)
    case full(gap: CGFloat? = 50)
    case none
}

class GGLocationViewController: ParallaxViewController, ControllerType, XibInitializable  {
    
    let markerScale: CGFloat = 1.2
    
    typealias ViewModelType = GGLocationViewModel
    var viewModel: ViewModelType!

    let disposeBag = DisposeBag()

    private let goaCenterCoordinate = CLLocationCoordinate2D(latitude: 15.614673, longitude: 73.736870)//Tortles beach in Morjim



    @IBOutlet weak var mapView: GMSMapView!
    
    @IBOutlet weak var backgroundView: ClickableView! {
        didSet {
            backgroundView.alpha = 0
        }
    }
    @IBOutlet weak var locationInfoView: GGLocationInfoView!
    @IBOutlet weak var locationInfoViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var locationInfoViewHeightConstraint: NSLayoutConstraint!


    private let locationManager = CLLocationManager()
    private var _marker: GGMarker?
    var marker: GGMarker? {
        get {
            return _marker
        }
        set {
            _marker = newValue
        }
    }


    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = ""

        setupMap()
        
        currentLine = .second
        
        bind()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print("resources: \(RxSwift.Resources.total)")
    }
    
    func bind() {
        
        backgroundView.tap.rx.event
            .map { _ -> Void in }
            .bind(to: viewModel.input.backgroundDidTap)
            .disposed(by: disposeBag)
        
        mapView.rx.handleTapMarker { [weak self] marker -> Bool in
            guard let strongSelf = self else { return true }

            if let marker = strongSelf.marker, !marker.isSelected, !marker.isDataLoading {
                strongSelf.viewModel.input.markerDidTap.onNext(())
                
                //увеличиваем иконку маркера
                if let container = marker.iconView, let imageView = container.subviews.first as? UIImageView {
                    let newSize = CGSize(width: container.bounds.size.width * strongSelf.markerScale, height: container.bounds.size.height * strongSelf.markerScale)
                    container.bounds.size = newSize//согласно документации меняем только bounds
                    
                    imageView.center = CGPoint(x: container.bounds.width / 2, y: container.bounds.height - imageView.frame.height + imageView.frame.height / 2)
                    UIView.animate(withDuration: TimeInterval(UINavigationController.hideShowBarDuration)) {
                        imageView.frame.size = newSize
                        imageView.center = CGPoint(x: container.bounds.width / 2, y: container.bounds.height - imageView.frame.height + imageView.frame.height / 2)
                    }
                }
                                
                //сдвигаем кнопку Моя позиция наверх
                UIView.animate(withDuration:  TimeInterval(UINavigationController.hideShowBarDuration)) {
                    strongSelf.mapView.padding = UIEdgeInsets(top: GGLocationInfoView.height, left: 0, bottom: GGLocationInfoView.height, right: 0)
                }
                
                //сдвигаем камеру наверх
                let markerPoint = strongSelf.mapView.projection.point(for: marker.position)
                if markerPoint.y > strongSelf.mapView.bounds.height - GGLocationInfoView.height {
                    strongSelf.mapView.animate(with: GMSCameraUpdate.scrollBy(x: 0, y: GGLocationInfoView.height))
                }
                
                strongSelf.marker?.isSelected = true
            }
            
            return true
        }


        mapView.rx.didTapAt
            .map { _ -> Void in }
            .do(onNext: { [weak self] _ in
                guard let strongSelf = self else { return }
                if let marker = strongSelf.marker, marker.isSelected, let container = marker.iconView, let imageView = container.subviews.first as? UIImageView {
                    let newSize = CGSize(width: container.frame.size.width / strongSelf.markerScale, height: container.frame.size.height / strongSelf.markerScale)
                                        
                    UIView.animate(withDuration: TimeInterval(UINavigationController.hideShowBarDuration), animations: {
                        imageView.frame.size = newSize
                        imageView.center = CGPoint(x: container.bounds.width / 2, y: container.bounds.height - imageView.frame.height + imageView.frame.height / 2)
                    }) { _ in
                        container.bounds.size = newSize
                        imageView.center = container.center
                    }
                    strongSelf.marker?.isSelected = false
                }
                UIView.animate(withDuration:  TimeInterval(UINavigationController.hideShowBarDuration)) {
                    strongSelf.mapView.padding = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
                }
            })
            .bind(to: viewModel.input.mapOutsideMarkerDidTap)
            .disposed(by: disposeBag)
        
        viewModel.output.location
            .bind(to: rx.marker)
            .disposed(by: disposeBag)
        
        
        viewModel.output.locationInfoPosition
            .bind(to: rx.infoViewPosition)
            .disposed(by: disposeBag)
        
        viewModel.output.locationInfoView
            .bind(to: locationInfoView.rx.viewModel)
            .disposed(by: disposeBag)
        
        viewModel.output.loading
            .bind(to: rx.markerDisabled)
            .disposed(by: disposeBag)
        
//        navigationController?.rx.willShow
//            .subscribe(onNext: { [weak self] (viewController, _) in
//                if let navigationController = viewController.navigationController {
//                    let isContainSelf = navigationController.viewControllers.contains(where: { $0 == self })
//                    if  !isContainSelf {
//                        self?.viewModel.input.backButtonDidTap.onNext(())
//                    }
//                }
//            })
//            .disposed(by: disposeBag)
    }

    private func setupMap() {
        locationManager.requestWhenInUseAuthorization()
        
//        guard locationManager.location != nil else {
//            return
//        }

        mapView.camera = GMSCameraPosition.camera(withLatitude: goaCenterCoordinate.latitude, longitude: goaCenterCoordinate.longitude, zoom: 12.0)
        mapView.isMyLocationEnabled = true
        mapView.settings.myLocationButton = true
    }
}

