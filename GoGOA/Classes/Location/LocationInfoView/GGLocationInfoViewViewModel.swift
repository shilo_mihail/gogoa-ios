//
//  GGLocationInfoViewViewModel.swift
//  GoGOA
//
//  Created by m.shilo on 11.08.2020.
//  Copyright © 2020 m.shilo. All rights reserved.
//

import Foundation
import RxSwift

class GGLocationInfoViewViewModel: ViewModelProtocol {
    
    struct Input {
        let cellDidSelect: AnyObserver<GGPhotoCollectionCellViewModel>
        let toggleDidTap: AnyObserver<Void>
    }
    struct Output {
        let photoCellViewModels: Observable<[GGPhotoCellViewModelsSection]>
        let selectedPhotoCellViewModel: Observable<GGPhotoCollectionCellViewModel>
        let title: Observable<String>
        let rating: Observable<Int>
        let toggle: Observable<Void>
    }
    
    let input: Input
    let output: Output
    
    private let cellDidSelectSubject = PublishSubject<GGPhotoCollectionCellViewModel>()
    private let toggleDidTapSubject = PublishSubject<Void>()
    
    private let photoCellViewModelsSubject = ReplaySubject<[GGPhotoCellViewModelsSection]>.create(bufferSize: 1)
    private let selectedPhotoCellViewModelSubject = PublishSubject<GGPhotoCollectionCellViewModel>()
    private let titleSubject = ReplaySubject<String>.create(bufferSize: 1)
    private let ratingSubject = ReplaySubject<Int>.create(bufferSize: 1)
    private let toggleSubject = PublishSubject<Void>()
    
    private let disposeBag = DisposeBag()
    
//    var photoIconViewModels: [PhotoIconCellViewModel] = []
    
    
    init(_ webService: WebService, _ dataService: DataService, location: Location) {
        var cellViewModels: [GGPhotoCollectionCellViewModel] = []
        for (i, photo) in location.photos.enumerated() {
            print(photo.smallPhotoBinary!.data)
            let cellViewModel = GGPhotoCollectionCellViewModel(webService, dataService, photo, id: i)
            cellViewModels.append(cellViewModel)
        }
        
        input = Input(cellDidSelect: cellDidSelectSubject.asObserver(),
                      toggleDidTap: toggleDidTapSubject.asObserver())
        
        output = Output(photoCellViewModels: photoCellViewModelsSubject.asObservable(),
                        selectedPhotoCellViewModel: selectedPhotoCellViewModelSubject.asObservable(),
                        title: titleSubject.asObservable(),
                        rating: ratingSubject.asObservable(),
                        toggle: toggleSubject.asObservable())
        
        photoCellViewModelsSubject.onNext([GGPhotoCellViewModelsSection(items: cellViewModels)])
        titleSubject.onNext(location.title)
        ratingSubject.onNext(location.rating)
        
        toggleDidTapSubject
            .bind(to: toggleSubject)
            .disposed(by: disposeBag)
        
        cellDidSelectSubject
            .bind(to: selectedPhotoCellViewModelSubject)
            .disposed(by: disposeBag)
    }
    
    deinit {
        photoCellViewModelsSubject.on(.completed)
        titleSubject.on(.completed)
        ratingSubject.on(.completed)
    }
}

