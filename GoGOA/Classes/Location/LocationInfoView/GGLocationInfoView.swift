//
//  GGLocationInfoView.swift
//  GoGOA
//
//  Created by m.shilo on 11.08.2020.
//  Copyright © 2020 m.shilo. All rights reserved.
//

import UIKit
import Cosmos
import RxSwift
import RxCocoa
import RxDataSources


class GGLocationInfoView: UIView {
    
    let widthHeightScale: CGFloat = 1.77
    let distanceBetweenPhoto: CGFloat = 10
    let topCornerRadius: CGFloat = 15
    static let height: CGFloat = 215
    
    typealias ViewModelType = GGLocationInfoViewViewModel
    
    var viewModel: ViewModelType! {
        didSet {
            panableLine.tap.rx.event
                .map { _ in }
                .bind(to: viewModel.input.toggleDidTap)
                .disposed(by: disposeBag)
            
            viewModel.output.title
                .bind(to: titleLabel.rx.text)
                .disposed(by: disposeBag)
            
            viewModel.output.rating
                .bind(to: ratingView.rx.rating)
                .disposed(by: disposeBag)
            
            let dataSource = RxCollectionViewSectionedAnimatedDataSource<GGPhotoCellViewModelsSection>(configureCell: { dataSource, collectionView, indexPath, photoCellViewModel in
                
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: GGPhotoCollectionViewCell.self), for: indexPath) as! GGPhotoCollectionViewCell
                
                if let sectionModel = dataSource.sectionModels.first {
                    let viewModel = sectionModel.items[indexPath.row]
                    cell.viewModel = viewModel
                }
                
                return cell
            })
            
            viewModel.output.photoCellViewModels
                .bind(to: photoCollectionView.rx.items(dataSource: dataSource))
                .disposed(by: disposeBag)
            

            photoCollectionView.rx.modelSelected(GGPhotoCollectionCellViewModel.self)
                .bind(to: viewModel.input.cellDidSelect)
                .disposed(by: disposeBag)
        }
    }
    
    var disposeBag = DisposeBag()
    

    @IBOutlet var contentView: UIView!
    @IBOutlet weak var containerView: GGTopCornersRoundedView! {
        didSet {
            containerView.cornerRadius = topCornerRadius
        }
    }
    @IBOutlet weak var panableLine: GGPanableLine!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var photoCollectionView: UICollectionView! {
        didSet {
            photoCollectionView.register(UINib(nibName: String(describing: GGPhotoCollectionViewCell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: GGPhotoCollectionViewCell.self))
        }
    }
    
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    
    private func commonInit() {
        Bundle.main.loadNibNamed(String(describing: GGLocationInfoView.self), owner: self, options: nil)
        
        addSubview(contentView)
        
        contentView.translatesAutoresizingMaskIntoConstraints = false
        contentView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        contentView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        contentView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        contentView.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
    }
    
    func resetDisposeBag () {
        disposeBag = DisposeBag()
    }
}

