//
//  MapViewModel.swift
//  GoGOA
//
//  Created by m.shilo on 05/02/2019.
//  Copyright © 2019 m.shilo. All rights reserved.
//

import RxSwift
import Foundation


class GGLocationViewModel: ViewModelProtocol {
    
    struct Input {
        let markerDidTap: AnyObserver<Void>
        let mapOutsideMarkerDidTap: AnyObserver<Void>
        let toggleLocationInfoView: Observable<Void>
        let backgroundDidTap: AnyObserver<Void>
    }
    struct Output {
        let location: Observable<Location>
        let loading: Observable<Bool>
        let locationInfoView: Observable<GGLocationInfoViewViewModel>
        let locationInfoPosition: Observable<GGLocationInfoPosition>
        let selectedPhotoCellViewModel: Observable<GGPhotoCollectionCellViewModel>
        let back: Observable<Void>
    }
    
    let input: Input
    let output: Output
    private var location: Location!
    
    //INPUT
    private let markerDidTapSubject = PublishSubject<Void>()
    private let mapOutsideMarkerDidTapSubject = PublishSubject<Void>()
    private let toggleLocationInfoViewSubject = PublishSubject<Void>()
    private let backgroundDidTapSubject = PublishSubject<Void>()
    //OUTPUT
    private let locationInfoViewSubject = PublishSubject<GGLocationInfoViewViewModel>()
    private let locationSubject = ReplaySubject<Location>.create(bufferSize: 1)
    private let loadingSubject = ReplaySubject<Bool>.create(bufferSize: 1)
    private let locationInfoPositionSubject = PublishSubject<GGLocationInfoPosition>()
    private let selectedPhotoCellViewModelSubject = PublishSubject<GGPhotoCollectionCellViewModel>()
    private let backSubject = PublishSubject<Void>()
    
    private let disposeBag = DisposeBag()
    
    
    init(_ webService: WebService, _ dataService: DataService, location: Location) {
        self.location = location
        input = Input(markerDidTap: markerDidTapSubject.asObserver(),
                      mapOutsideMarkerDidTap: mapOutsideMarkerDidTapSubject.asObserver(),
                      toggleLocationInfoView: toggleLocationInfoViewSubject.asObserver(),
                      backgroundDidTap: backgroundDidTapSubject.asObserver())
        
        output = Output(location: locationSubject.asObservable(),
                        loading: loadingSubject.asObservable(),
                        locationInfoView: locationInfoViewSubject.asObservable(),
                        locationInfoPosition: locationInfoPositionSubject.asObservable(),
                        selectedPhotoCellViewModel: selectedPhotoCellViewModelSubject.asObservable(),
                        back: backSubject.asObservable())
        
        loadingSubject.onNext(true)
        webService.getLocation(by: location.id)
            .stopLoading(subject: loadingSubject)
            .subscribe(onNext: { result in
                switch result {
                case .success(let resultLocation):
                    self.location = resultLocation
                    dataService.saveLocation(resultLocation)
                default:
                    break
                }
            })
        .disposed(by: disposeBag)
        
        locationSubject.onNext(location)
        
        markerDidTapSubject
            .map({ _ -> GGLocationInfoPosition in
                let locationInfoViewViewModel = GGLocationInfoViewViewModel(webService, dataService, location: self.location)
                self.locationInfoViewSubject.onNext(locationInfoViewViewModel)
                
                locationInfoViewViewModel.output.toggle
                    .withLatestFrom(self.locationInfoPositionSubject)
                    .subscribe(onNext: { position in
                        self.locationInfoPositionSubject.onNext(position == .full() ? .middle() : .full())
                    })
                    .disposed(by: self.disposeBag)
                
                locationInfoViewViewModel.output.selectedPhotoCellViewModel
                    .bind(to: self.selectedPhotoCellViewModelSubject)
                    .disposed(by: self.disposeBag)
                
                return GGLocationInfoPosition.middle()
            })
            .bind(to: locationInfoPositionSubject)
            .disposed(by: disposeBag)

        mapOutsideMarkerDidTapSubject
            .map { GGLocationInfoPosition.none }
            .bind(to: locationInfoPositionSubject)
            .disposed(by: disposeBag)
        
        backgroundDidTapSubject
            .bind(to: mapOutsideMarkerDidTapSubject)
            .disposed(by: disposeBag)
    }
    
    deinit {
        markerDidTapSubject.on(.completed)
        mapOutsideMarkerDidTapSubject.on(.completed)
        backgroundDidTapSubject.on(.completed)
        
        locationSubject.on(.completed)
        loadingSubject.on(.completed)
        locationInfoPositionSubject.on(.completed)
        backSubject.on(.completed)
    }
}
