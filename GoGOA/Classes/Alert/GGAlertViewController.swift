//
//  ErrorViewController.swift
//  GoGOA
//
//  Created by m.shilo on 18/06/2018.
//  Copyright © 2018 m.shilo. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class GGAlertViewController: UIViewController, XibInitializable {
    
    typealias ViewModelType = GGAlertViewModel
    var viewModel: ViewModelType!
    
    var alertType: GGAlertType!
    
    @IBOutlet weak var backgroundView: UIView!
    
    @IBOutlet weak var alertView: GGAlertView!
        
    var disposeBag = DisposeBag()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.alpha = 0
        alertView.alertType = alertType
        
        configure()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print("resources: \(RxSwift.Resources.total)")
    }
    
    func configure() {
        alertView.okBtn.rx.tap.asObservable()
            .bind(to: viewModel.input.okBtnDidTap)
            .disposed(by: disposeBag)
        
        alertView.codeTextField.rx.text.asObservable()
            .subscribe(viewModel.input.code)
            .disposed(by: disposeBag)
    }

}

//extension GGAlertViewController: UINavigationControllerDelegate {
//
//    func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationController.Operation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
//
//        switch operation {
//        case .push:
//            return AlertAnimator(
//                duration: TimeInterval(UINavigationController.hideShowBarDuration),
//                isPresenting: true,
//                originFrame: view.frame)
//        default:
//            return AlertAnimator(
//                duration: TimeInterval(UINavigationController.hideShowBarDuration),
//                isPresenting: false,
//                originFrame: view.frame)
//        }
//    }
//}
