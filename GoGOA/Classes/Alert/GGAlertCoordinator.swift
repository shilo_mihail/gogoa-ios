//
//  ErrorCoordinator.swift
//  GoGOA
//
//  Created by m.shilo on 29/12/2018.
//  Copyright © 2018 m.shilo. All rights reserved.
//

import UIKit
import RxSwift

enum GGAlertType {
    case error(AppError)
    case code
}

enum GGAlertCoordinationResult {
    case none
    case code(String)
}


class GGAlertCoordinator: BaseCoordinator<GGAlertCoordinationResult> {
    
    let alertType: GGAlertType
    private lazy var alertPresentationManager = AlertPresentationManager()
    private let parentViewController: UIViewController
    
    init(_ alertType: GGAlertType, _ parentViewController: UIViewController) {
        self.alertType = alertType
        self.parentViewController = parentViewController
    }
    
    
    override func start() -> Observable<CoordinationResult> {
        let viewController = GGAlertViewController.initFromXib()
        viewController.alertType = alertType
        let viewModel = GGAlertViewModel()
        viewModel.alertType = alertType
        viewController.viewModel = viewModel
        
        let none = viewModel.output.none.map { _ in CoordinationResult.none }
        let code = viewModel.output.code.map { CoordinationResult.code($0) }
        
        viewController.transitioningDelegate = alertPresentationManager
        viewController.modalPresentationStyle = .custom
        parentViewController.present(viewController, animated: true)
        
        return Observable.merge(none, code)
            .take(1)
            .do(onNext: { [weak self] _ in
                self?.parentViewController.dismiss(animated: true)
            })
    }
}

extension GGAlertCoordinationResult: Equatable {
    
    public static func ==(lhs: GGAlertCoordinationResult, rhs:GGAlertCoordinationResult) -> Bool {
        switch lhs {
        case .code(let a):
            switch rhs {
            case .code(let b):
                return a == b
            case .none:
                return false
            }
        case .none:
            switch rhs {
            case .none:
                return true
            case .code:
                return false
            }
        }
    }
}

