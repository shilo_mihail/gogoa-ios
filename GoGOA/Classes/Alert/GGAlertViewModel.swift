//
//  GGErrorViewModel.swift
//  GoGOA
//
//  Created by m.shilo on 24/02/2019.
//  Copyright © 2019 m.shilo. All rights reserved.
//

import RxSwift
import Foundation

class GGAlertViewModel: ViewModelProtocol {
    
    var alertType: GGAlertType!
    
    struct Input {
        let okBtnDidTap: AnyObserver<Void>
        let code: AnyObserver<String?>
    }
    struct Output {
        let none: Observable<Void>
        let code: Observable<String>
    }
    
    let input: Input
    let output: Output
    
    private let okBtnDidTapSubject = PublishSubject<Void>()
    private let codeInputSubject = BehaviorSubject<String?>(value: "")
    
    private let noneSubject = PublishSubject<Void>()
    private let codeOutputSubject = PublishSubject<String>()
    
    private let disposeBag = DisposeBag()
    
    
    init() {
        input = Input(okBtnDidTap: okBtnDidTapSubject.asObserver(),
                      code: codeInputSubject.asObserver())
        output = Output(none: noneSubject.asObservable(),
                        code: codeOutputSubject.asObservable())
        
        okBtnDidTapSubject
            .withLatestFrom(codeInputSubject)
            .subscribe(onNext: { [weak self] code in
                if let alertType = self?.alertType {
                    switch alertType {
                    case .error:
                        self?.noneSubject.onNext(())
                    case .code:
                        self?.codeOutputSubject.onNext(code!)
                    }
                }
            })
            .disposed(by: disposeBag)
    }
    
    deinit {
        okBtnDidTapSubject.on(.completed)
        codeInputSubject.on(.completed)
        
        noneSubject.on(.completed)
        codeOutputSubject.on(.completed)
    }
    
    
}

