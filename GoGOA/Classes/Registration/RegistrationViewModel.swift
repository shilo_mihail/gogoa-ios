//
//  RegistrationViewModel.swift
//  GoGOA
//
//  Created by m.shilo on 30/01/2019.
//  Copyright © 2019 m.shilo. All rights reserved.
//

import RxSwift
import Foundation
import Firebase

class RegistrationViewModel: ViewModelProtocol {
    
    struct Input {
        let login: AnyObserver<String?>
        let name: AnyObserver<String?>
        let password: AnyObserver<String?>
        let confirmPassword: AnyObserver<String?>
        let registrationBtnDidTap: AnyObserver<Void>
        let backButtonDidTap: AnyObserver<Void>
        let codeDidEnter: AnyObserver<CheckingPhoneCredentials>
        let checkingPhoneDidFinish: AnyObserver<Void>
    }
    struct Output {
        let showTabBar: Observable<[String : Int]>
        let error: Observable<AppError>
        let loading: Observable<Bool>
        let isLoginWarning: Observable<Bool>
        let isNameWarning: Observable<Bool>
        let isPasswordWarning: Observable<Bool>
        let isConfirmPasswordWarning: Observable<Bool>
        let isRegisterBtnActive: Observable<Bool>
        let showEnterCodeAlert: Observable<String>
        let back: Observable<Void>
    }
    
    struct ConfirmPassValidationStruct {
        var password: String
        var confirmPassword: String
    }
    
    let input: Input
    let output: Output
    
    //INPUT
    private let loginSubject = PublishSubject<String?>()
    private let nameSubject = PublishSubject<String?>()
    private let passwordSubject = PublishSubject<String?>()
    private let confirmPasswordSubject = PublishSubject<String?>()
    private let registrationBtnDidTapSubject = PublishSubject<Void>()
    private let backButtonDidTapSubject = PublishSubject<Void>()
    private let codeDidEnterSubject = PublishSubject<CheckingPhoneCredentials>()
    private let checkingPhoneDidFinishSubject = PublishSubject<Void>()
    
    //OUTPUT
    private let showTabBarSubject = PublishSubject<[String : Int]>()
    private let errorSubject = PublishSubject<AppError>()
    private let loadingSubject = PublishSubject<Bool>()
    private let isLoginWarningSubject = PublishSubject<Bool>()
    private let isNameWarningSubject = PublishSubject<Bool>()
    private let isPasswordWarningSubject = PublishSubject<Bool>()
    private let isConfirmPasswordWarningSubject = PublishSubject<Bool>()
    private let isRegisterBtnActiveSubject = BehaviorSubject<Bool>(value: false)
    private let showEnterCodeAlertSubject = PublishSubject<String>()
    private let backSubject = PublishSubject<Void>()
    
    private let disposeBag = DisposeBag()
    
    private var credentialsObservable: Observable<RegCredentials> {
        return Observable.combineLatest(loginSubject.asObservable(),
                                        nameSubject.asObservable(),
                                        passwordSubject.asObservable()) {
                                            RegCredentials(login: $0!, name: $1!, password: $2!)
        }
    }
    
    private var confirmationPasswordObservable: Observable<ConfirmPassValidationStruct> {
        return Observable.combineLatest(passwordSubject.asObservable(),
                                        confirmPasswordSubject.asObservable()) {
                                            ConfirmPassValidationStruct(password: $0!, confirmPassword: $1!)
        }
    }
    
    init(_ webService: WebService, _ dataService: DataService) {
        
        input = Input(login: loginSubject.asObserver(),
                      name: nameSubject.asObserver(),
                      password: passwordSubject.asObserver(),
                      confirmPassword: confirmPasswordSubject.asObserver(),
                      registrationBtnDidTap: registrationBtnDidTapSubject.asObserver(),
                      backButtonDidTap: backButtonDidTapSubject.asObserver(),
                      codeDidEnter: codeDidEnterSubject.asObserver(),
                      checkingPhoneDidFinish: checkingPhoneDidFinishSubject.asObserver())
        
        output = Output(showTabBar: showTabBarSubject.asObservable(),
                        error: errorSubject.asObservable(),
                        loading: loadingSubject.asObservable(),
                        isLoginWarning: isLoginWarningSubject.asObservable(),
                        isNameWarning: isNameWarningSubject.asObservable(),
                        isPasswordWarning: isPasswordWarningSubject.asObservable(),
                        isConfirmPasswordWarning: isConfirmPasswordWarningSubject.asObservable(),
                        isRegisterBtnActive: isRegisterBtnActiveSubject.asObservable(),
                        showEnterCodeAlert: showEnterCodeAlertSubject.asObservable(),
                        back: backSubject.asObservable())
        
        loginSubject
            .subscribe(onNext: { [weak self] login in
                if let _ = login {
                    self?.isLoginWarningSubject.onNext(!Utils.checkLogin(login!))
                }
            })
            .disposed(by: disposeBag)
        
        nameSubject
            .subscribe(onNext: { [weak self] name in
                if let _ = name {
                    self?.isNameWarningSubject.onNext(!Utils.checkName(name!))
                }
            })
            .disposed(by: disposeBag)
        
        passwordSubject
            .subscribe(onNext: { [weak self] pass in
                if let _ = pass {
                    self?.isPasswordWarningSubject.onNext(!Utils.checkPassword(pass!))
                }
            })
            .disposed(by: disposeBag)
        
        confirmPasswordSubject
            .withLatestFrom(confirmationPasswordObservable)
            .subscribe(onNext: { [weak self] confirmPassValidationStruct in
                if Utils.checkConfirmPassword(confirmPassValidationStruct.password, confirmPassValidationStruct.confirmPassword) {
                    self?.isConfirmPasswordWarningSubject.onNext(false)
                } else {
                    self?.isConfirmPasswordWarningSubject.onNext(true)
                }
            })
            .disposed(by: disposeBag)
        
        Observable.combineLatest(isLoginWarningSubject, isNameWarningSubject, isPasswordWarningSubject, isConfirmPasswordWarningSubject)
            .map({ (isLoginWarning, isNameWarning, isPasswordWarning, isConfirmPasswordWarning) -> Bool in
                return isLoginWarning || isNameWarning || isPasswordWarning || isConfirmPasswordWarning
            })
            .subscribe(onNext: { [weak self] isWarning in
                guard let strongSelf = self else {
                    return
                }
                strongSelf.isRegisterBtnActiveSubject.onNext(!isWarning)
            })
            .disposed(by: disposeBag)
        
        
        registrationBtnDidTapSubject
            .withLatestFrom(loginSubject)
            .subscribe(onNext: { [weak self] login in
                self?.loadingSubject.onNext(true)
                
                PhoneAuthProvider.provider().verifyPhoneNumber("+79161969959", uiDelegate: nil) { (verificationID, error) in
                    self?.loadingSubject.onNext(false)
                    if let _ = error {
                        self?.errorSubject.onNext(.generateVerificationCodeError)
                        return
                    }
                    print("verificationID: \(verificationID!)")
                    self?.showEnterCodeAlertSubject.onNext(verificationID!)
                }
            })
            .disposed(by: disposeBag)

        
        codeDidEnterSubject
            .subscribe(onNext: { [weak self] checkCredentials in
                self?.loadingSubject.onNext(true)
                print(checkCredentials.verificationId, checkCredentials.verificationCode)
                let credential = PhoneAuthProvider.provider().credential(
                    withVerificationID: checkCredentials.verificationId,
                    verificationCode: checkCredentials.verificationCode)
                
                Auth.auth().signInAndRetrieveData(with: credential) { (authResult, error) in
                    if let _ = error {
                        self?.loadingSubject.onNext(false)
                        self?.errorSubject.onNext(.verificationCodeNotMatch)
                        return
                    }
                    self?.checkingPhoneDidFinishSubject.onNext(())
                }
            })
            .disposed(by: disposeBag)
        
        
        checkingPhoneDidFinishSubject
            .withLatestFrom(credentialsObservable)
            .flatMapLatest { credentials -> Observable<GGResult<GGAuthResponse>> in
                let user = GGUser()
                user.login = Utils.clean(credentials.login)
                user.name = credentials.name
                user.password = credentials.password
                user.role = GGRole.USER.rawValue
                
                return webService.registration(user)
            }
            .subscribe(onNext: { [weak self] result in
                self?.loadingSubject.onNext(false)
                switch result {
                case .success(let authResponse):
                    dataService.saveCurrentUser(authResponse.user)
                    self?.showTabBarSubject.onNext(authResponse.todayFutureEventsCount)
                case .failure(let error):
                    self?.errorSubject.onNext(error)
                }
            })
            .disposed(by: disposeBag)
        
        
        backButtonDidTapSubject
            .subscribe(onNext: { [weak self] in
                self?.backSubject.onNext(())
            })
            .disposed(by: disposeBag)
        
        
    }
    
    deinit {
        loginSubject.on(.completed)
        nameSubject.on(.completed)
        passwordSubject.on(.completed)
        confirmPasswordSubject.on(.completed)
        registrationBtnDidTapSubject.on(.completed)
        backButtonDidTapSubject.on(.completed)
        codeDidEnterSubject.on(.completed)
        checkingPhoneDidFinishSubject.on(.completed)
        
        showTabBarSubject.on(.completed)
        errorSubject.on(.completed)
        loadingSubject.on(.completed)
        isLoginWarningSubject.on(.completed)
        isNameWarningSubject.on(.completed)
        isPasswordWarningSubject.on(.completed)
        isConfirmPasswordWarningSubject.on(.completed)
        isRegisterBtnActiveSubject.on(.completed)
        showEnterCodeAlertSubject.on(.completed)
        backSubject.on(.completed)
    }
}
