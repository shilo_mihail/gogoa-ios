//
//  RegistrationViewController.swift
//  GoGOA
//
//  Created by m.shilo on 30/01/2019.
//  Copyright © 2019 m.shilo. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class RegistrationViewController: ParallaxViewController, ControllerType, XibInitializable {
    
    typealias ViewModelType = RegistrationViewModel
    
    var viewModel: ViewModelType!
    
    @IBOutlet weak var backButton: UIButton! {
        didSet {
            let templateImage = Constants.image.backArrow.withRenderingMode(.alwaysTemplate)
            backButton.setImage(templateImage, for: .normal)
            backButton.tintColor = Constants.color.loginGroup.dark
        }
    }
    @IBOutlet weak var loginTextField: GGMobilePhoneTextField! {
        didSet {
            loginTextField.setColor(.login)
            loginTextField.formatPattern = "+##(###)###-##-##"
            loginTextField.text = "+91"
            loginTextField.position(from: loginTextField.beginningOfDocument, offset: 3)
        }
    }
    @IBOutlet weak var nameTextField: GGLineTextField! {
        didSet {
            nameTextField.setColor(.login)
        }
    }
    @IBOutlet weak var passwordTextField: GGLineTextField! {
        didSet {
            passwordTextField.setColor(.login)
//            passwordTextField.text = "qwerty123"
        }
    }
    @IBOutlet weak var confirmPasswordTextField: GGLineTextField! {
        didSet {
            confirmPasswordTextField.setColor(.login)
//            confirmPasswordTextField.text = "qwerty123"
        }
    }
    @IBOutlet weak var registerButton: GGRoundedButtonView! {
        didSet {
            registerButton.label.text = "SIGN UP"
            registerButton.startColor = Constants.color.button.loginGroup.leftGragient
            registerButton.finishColor = Constants.color.button.loginGroup.rightGradient
        }
    }
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    
    var disposeBag = DisposeBag()
    
    override func viewDidLoad(){
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
        bind()
        
        currentLine = .second
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.handleKeyboardNotification(notification:)),
                                               name: UIResponder.keyboardWillChangeFrameNotification,
                                               object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print("resources: \(RxSwift.Resources.total)")
    }
    
    
    func bind() {
        
        //INPUT
        loginTextField.rx.text.asObservable()
            .subscribe(viewModel.input.login)
            .disposed(by: disposeBag)
        
        nameTextField.rx.text.asObservable()
            .subscribe(viewModel.input.name)
            .disposed(by: disposeBag)
        
        passwordTextField.rx.text.asObservable()
            .subscribe(viewModel.input.password)
            .disposed(by: disposeBag)
        
        confirmPasswordTextField.rx.text.asObservable()
            .subscribe(viewModel.input.confirmPassword)
            .disposed(by: disposeBag)
        
        registerButton.tap.rx.event
            .map { _ -> Void in }
            .bind(to: viewModel.input.registrationBtnDidTap)
            .disposed(by: disposeBag)
        
        
        backButton.rx.tap.asObservable()
            .bind(to: viewModel.input.backButtonDidTap)
            .disposed(by: disposeBag)
        
        //OUTPUT
        viewModel.output.loading
            .bind(to: registerButton.rx.loading)
            .disposed(by: disposeBag)
        
        viewModel.output.isLoginWarning
            .bind(to: loginTextField.rx.isWarning)
            .disposed(by: disposeBag)
        
        viewModel.output.isNameWarning
            .bind(to: nameTextField.rx.isWarning)
            .disposed(by: disposeBag)
        
        viewModel.output.isPasswordWarning
            .bind(to: passwordTextField.rx.isWarning)
            .disposed(by: disposeBag)
        
        viewModel.output.isConfirmPasswordWarning
            .bind(to: confirmPasswordTextField.rx.isWarning)
            .disposed(by: disposeBag)
        
        viewModel.output.isRegisterBtnActive
            .bind(to: registerButton.rx.isActive)
            .disposed(by: disposeBag)
        
    }
    
    @objc func handleKeyboardNotification(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            let endFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            let endFrameY = endFrame?.origin.y ?? 0
            let duration:TimeInterval = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let animationCurveRawNSN = userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as? NSNumber
            let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIView.AnimationOptions.curveEaseInOut.rawValue
            let animationCurve:UIView.AnimationOptions = UIView.AnimationOptions(rawValue: animationCurveRaw)
            if endFrameY >= UIScreen.main.bounds.size.height {
                bottomConstraint.constant = 70.0
            } else {
                bottomConstraint.constant = endFrame?.size.height ?? 0.0
            }
            UIView.animate(withDuration: duration,
                           delay: TimeInterval(0),
                           options: animationCurve,
                           animations: { self.view.layoutIfNeeded() },
                           completion: nil)
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
}

