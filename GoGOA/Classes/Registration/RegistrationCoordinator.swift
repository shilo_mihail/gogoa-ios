//
//  RegistrationCoordinator.swift
//  GoGOA
//
//  Created by m.shilo on 30/01/2019.
//  Copyright © 2019 m.shilo. All rights reserved.
//

import UIKit
import RxSwift

enum RegistrationCoordinationResult {
    case login
    case back
}

class RegistrationCoordinator: BaseCoordinator<Void> {
    
    private let webService: WebService
    private let dataService: DataService
    private let window: UIWindow
    private let navigationController: UINavigationController
    
    init(_ webService: WebService, _ dataService: DataService, _ window: UIWindow, _ navigationController: UINavigationController) {
        self.webService = webService
        self.dataService = dataService
        self.window = window
        self.navigationController = navigationController
    }
    
    override func start() -> Observable<CoordinationResult> {
        let viewController = RegistrationViewController.initFromXib()
        
        let viewModel = RegistrationViewModel(webService, dataService)
        viewController.viewModel = viewModel
        
        
        let registrationResult = viewModel.output.showTabBar
            .flatMap { [weak self] todayFutureEventsCountOnAuth -> Observable<Void> in
                guard let strongSelf = self else {
                    return .empty()
                }
                return strongSelf.showTabBar(todayFutureEventsCountOnAuth)
            }
            .debug()
//            .map({ [weak self] result -> Observable<Void> in
//                guard let strongSelf = self else {
//                    return .empty()
//                }
//                return strongSelf.showTabBar()
//            })
//            .map { [weak self] observable -> Void in
//                guard let strongSelf = self else {
//                    return
//                }
//                observable
//                    .subscribe()
//                    .disposed(by: strongSelf.disposeBag)
//            }
        
        viewModel.output.error
            .flatMap({ [weak self] error-> Observable<GGAlertCoordinationResult> in
                guard let strongSelf = self else { return .empty() }
                return strongSelf.showError(error, viewController)
            })
            .subscribe()
            .disposed(by: disposeBag)
        
        viewModel.output.showEnterCodeAlert
            .subscribe(onNext:  { [weak self] verificationID -> Void in
                guard let strongSelf = self else {
                    return
                }
                strongSelf.showCodeAlert(verificationID, viewController)
                    .subscribe(onNext: { result in
                        switch result {
                        case .code(let code):
                            let checkingPhoneCredentials = CheckingPhoneCredentials(verificationId: verificationID, verificationCode: code)
                            viewModel.input.codeDidEnter.onNext(checkingPhoneCredentials)
                        default:
                            break
                        }
                    })
                    .disposed(by: strongSelf.disposeBag)
            })
            .disposed(by: disposeBag)
        
//        viewModel.output.showEnterCodeAlert
//            .flatMap { [weak self] verificationID -> Observable<GGAlertCoordinationResult> in
//                guard let strongSelf = self else {
//                    return .empty()
//                }
//                return strongSelf.showCodeAlert(verificationID, viewController)
//            }
//            .map({ result -> String in
//                switch result {
//                case .code(let code):
//                    return code
//                default:
//                    break
//                }
//                return ""
//            })
//            .subscribe(onNext: { code in
//                viewModel.input.codeDidEnter.onNext(code)
//            })
//            .disposed(by: disposeBag)
        
        
        let back = viewModel.output.back
            .debug()
            .map({ [weak self] _ -> Void in
                self?.navigationController.popViewController(animated: true)
            })
        
        navigationController.pushViewController(viewController, animated: true)
        
        return Observable.merge(registrationResult, back)
            .debug()
            .take(1)
    }
    
    private func showTabBar(_ todayFutureEventsCountOnAuth: [String : Int]) -> Observable<Void> {
        let tabBarCoordinator = GGTabBarCoordinator(webService, dataService, window, todayFutureEventsCountOnAuth)
        return coordinate(to: tabBarCoordinator)
    }
    
    private func showCodeAlert(_ verificationID: String, _ viewController: UIViewController) -> Observable<GGAlertCoordinationResult> {
        let codeAlertCoordinator = GGAlertCoordinator(.code, viewController)
        return coordinate(to: codeAlertCoordinator)
    }
}


