//
//  LoginViewController.swift
//  GoGOA
//
//  Created by m.shilo on 08/12/2018.
//  Copyright © 2018 m.shilo. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class LoginViewController: ParallaxViewController, ControllerType, XibInitializable {
    
    typealias ViewModelType = LoginViewModel
    
    var viewModel: ViewModelType!

    @IBOutlet weak var backButton: UIButton! {
        didSet {
            let templateImage = Constants.image.backArrow.withRenderingMode(.alwaysTemplate)
            backButton.setImage(templateImage, for: .normal)
            backButton.tintColor = Constants.color.loginGroup.dark
        }
    }
    @IBOutlet weak var loginTextField: GGMobilePhoneTextField! {
        didSet {
            loginTextField.setColor(.login)
            loginTextField.formatPattern = Constants.mobilePhoneMask
//            loginTextField.text = Constants.mobilePhoneCode
            loginTextField.text = "+91(123)456-78-90"
            loginTextField.position(from: loginTextField.beginningOfDocument, offset: Constants.mobilePhoneCode.count)
        }
    }
    @IBOutlet weak var passwordTextField: GGLineTextField! {
        didSet {
            passwordTextField.setColor(.login)
            passwordTextField.text = "qwerty123"
        }
    }
    @IBOutlet weak var loginButton: GGRoundedButtonView! {
        didSet {
            loginButton.label.text = "LOG IN"
            loginButton.startColor = Constants.color.button.loginGroup.leftGragient
            loginButton.finishColor = Constants.color.button.loginGroup.rightGradient
        }
    }
    @IBOutlet weak var forgotPasswordButton: UIButton! {
        didSet {
            forgotPasswordButton.setTitleColor(Constants.color.button.loginGroup.title, for: .normal)
        }
    }
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    
    var disposeBag = DisposeBag()
    
    
    override func viewDidLoad(){
        super.viewDidLoad()
        navigationItem.title = ""
        navigationController?.isNavigationBarHidden = true
        currentLine = .second
        
        hideKeyboardWhenTappedAround()
        bind()

        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.handleKeyboardNotification(notification:)),
                                               name: UIResponder.keyboardWillChangeFrameNotification,
                                               object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print("resources: \(RxSwift.Resources.total)")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        print("DISMISS!!!")

    }

    func bind() {
        
//        loginTextField.rx.text.asObservable()
//            .subscribe(onNext: loginTextField.setPreservingCursor())
//            .disposed(by: disposeBag)
        
        backButton.rx.tap.asObservable()
            .bind(to: viewModel.input.backButtonDidTap)
            .disposed(by: disposeBag)

        loginTextField.rx.text.asObservable()
            .subscribe(viewModel.input.login)
            .disposed(by: disposeBag)
        
        passwordTextField.rx.text.asObservable()
            .subscribe(viewModel.input.password)
            .disposed(by: disposeBag)
        
        loginButton.tap.rx.event
            .map { _ -> Void in }
            .bind(to: viewModel.input.loginBtnDidTap)
            .disposed(by: disposeBag)

        viewModel.output.loading
            .bind(to: loginButton.rx.loading)
            .disposed(by: disposeBag)
        
        viewModel.output.isLoginWarning
            .bind(to: loginTextField.rx.isWarning)
            .disposed(by: disposeBag)
        
        viewModel.output.isPasswordWarning
            .bind(to: passwordTextField.rx.isWarning)
            .disposed(by: disposeBag)
        
        viewModel.output.isLoginBtnActive
            .bind(to: loginButton.rx.isActive)
            .disposed(by: disposeBag)
        
        
//        navigationController?.rx.animationControllerFor.asObservable()
//            .subscribe(onNext: { operation in
//                print(operation)
//            })
//            .disposed(by: disposeBag)
        
    }
    
    @objc func handleKeyboardNotification(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            let endFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            let endFrameY = endFrame?.origin.y ?? 0
            let duration:TimeInterval = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let animationCurveRawNSN = userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as? NSNumber
            let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIView.AnimationOptions.curveEaseInOut.rawValue
            let animationCurve:UIView.AnimationOptions = UIView.AnimationOptions(rawValue: animationCurveRaw)
            if endFrameY >= UIScreen.main.bounds.size.height {
                bottomConstraint.constant = 20.0
            } else {
                bottomConstraint.constant = endFrame?.size.height ?? 0.0
            }
            UIView.animate(withDuration: duration,
                           delay: TimeInterval(0),
                           options: animationCurve,
                           animations: { self.view.layoutIfNeeded() },
                           completion: nil)
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }

}
