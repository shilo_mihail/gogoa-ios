//
//  LoginViewModel.swift
//  GoGOA
//
//  Created by m.shilo on 08/12/2018.
//  Copyright © 2018 m.shilo. All rights reserved.
//

import RxSwift
import Foundation

class LoginViewModel: ViewModelProtocol {
    
    struct Input {
        let login: AnyObserver<String?>
        let password: AnyObserver<String?>
        let loginBtnDidTap: AnyObserver<Void>
        let forgotPasswordBtnDidTap: AnyObserver<Void>
        let backButtonDidTap: AnyObserver<Void>
    }
    struct Output {
        let showTabBar: Observable<[String : Int]>
        let error: Observable<AppError>
        let loading: Observable<Bool>
        let isLoginWarning: Observable<Bool>
        let isPasswordWarning: Observable<Bool>
        let isLoginBtnActive: Observable<Bool>
        let back: Observable<Void>
    }
    
    let input: Input
    let output: Output
    
    private let loginSubject = PublishSubject<String?>()
    private let passwordSubject = PublishSubject<String?>()
    private let loginBtnDidTapSubject = PublishSubject<Void>()
    private let forgotPasswordBtnDidTapSubject = PublishSubject<Void>()
    private let backButtonDidTapSubject = PublishSubject<Void>()
    
    private let showTabBarSubject = PublishSubject<[String : Int]>()
    private let errorSubject = PublishSubject<AppError>()
    private let loadingSubject = PublishSubject<Bool>()
    private let isLoginWarningSubject = PublishSubject<Bool>()
    private let isPasswordWarningSubject = PublishSubject<Bool>()
    private let isLoginBtnActiveSubject = PublishSubject<Bool>()
    private let backSubject = PublishSubject<Void>()
    
    private let disposeBag = DisposeBag()
    
    private var credentialsObservable: Observable<Credentials> {
        return Observable.combineLatest(loginSubject.asObservable(), passwordSubject.asObservable()) { (login, password) in
            return Credentials(login: login!, password: password!)
        }
    }
    
    init(_ webService: WebService, _ dataService: DataService) {
        
        input = Input(login: loginSubject.asObserver(),
                      password: passwordSubject.asObserver(),
                      loginBtnDidTap: loginBtnDidTapSubject.asObserver(),
                      forgotPasswordBtnDidTap: forgotPasswordBtnDidTapSubject.asObserver(),
                      backButtonDidTap: backButtonDidTapSubject.asObserver())
        
        output = Output(showTabBar: showTabBarSubject.asObservable(),
                        error: errorSubject.asObservable(),
                        loading: loadingSubject.asObservable(),
                        isLoginWarning: isLoginWarningSubject.asObservable(),
                        isPasswordWarning: isPasswordWarningSubject.asObserver(),
                        isLoginBtnActive: isLoginBtnActiveSubject.asObserver(),
                        back: backSubject.asObservable())
        
        loginSubject
            .subscribe(onNext: { [weak self] login in
                if let _ = login {
                    self?.isLoginWarningSubject.onNext(!Utils.checkLogin(login!))
                }
            })
            .disposed(by: disposeBag)
        
        passwordSubject
            .subscribe(onNext: { [weak self] pass in
                if let _ = pass {
                    self?.isPasswordWarningSubject.onNext(!Utils.checkPassword(pass!))
                }
            })
            .disposed(by: disposeBag)
        
        Observable.combineLatest(isLoginWarningSubject, isPasswordWarningSubject)
            .map({ (isLoginWarning, isPasswordWarning) -> Bool in
                return isLoginWarning || isPasswordWarning
            })
            .subscribe(onNext: { [weak self] isWarning in
                self?.isLoginBtnActiveSubject.onNext(!isWarning)
            })
            .disposed(by: disposeBag)

        
        loginBtnDidTapSubject
            .withLatestFrom(credentialsObservable)
            .startLoading(subject: loadingSubject)
            .flatMapLatest { credentials -> Observable<GGResult<GGAuthResponse>> in
                return webService.authenticate(Utils.clean(credentials.login), credentials.password)
            }
            .catchErrorJustReturn(.failure(.networkError))
            .stopLoading(subject: loadingSubject)
            .subscribe(onNext: { [weak self] result in
                switch result {
                case .success(let authReponse):
                    dataService.saveCurrentUser(authReponse.user)
                    self?.showTabBarSubject.onNext(authReponse.todayFutureEventsCount)
                case .failure(let error):
                    self?.errorSubject.onNext(error)
                }
            })
            .disposed(by: disposeBag)
        
        
        backButtonDidTapSubject
            .subscribe(onNext: { [weak self] in
                self?.backSubject.onNext(())
            })
            .disposed(by: disposeBag)
    }
    
    deinit {
        loginSubject.on(.completed)
        passwordSubject.on(.completed)
        loginBtnDidTapSubject.on(.completed)
        forgotPasswordBtnDidTapSubject.on(.completed)
        
        showTabBarSubject.on(.completed)
        errorSubject.on(.completed)
        loadingSubject.on(.completed)
        isLoginWarningSubject.on(.completed)
        isPasswordWarningSubject.on(.completed)
        isLoginBtnActiveSubject.on(.completed)
        backSubject.on(.completed)
    }
    
    
}
