//
//  LoginCoordinator.swift
//  GoGOA
//
//  Created by m.shilo on 16/12/2018.
//  Copyright © 2018 m.shilo. All rights reserved.
//

import UIKit
import RxSwift

enum LoginCoordinationResult {
    case login
    case registration
}


class LoginCoordinator: BaseCoordinator<Void> {
    
    private let webService: WebService
    private let dataService: DataService
    private let window: UIWindow
    private let navigationController: UINavigationController
    
    init(_ webService: WebService, _ dataService: DataService, _ window: UIWindow, _ navigationController: UINavigationController) {
        self.webService = webService
        self.dataService = dataService
        self.window = window
        self.navigationController = navigationController
    }
    
    override func start() -> Observable<CoordinationResult> {
        let viewController = LoginViewController.initFromXib()
        let viewModel = LoginViewModel(webService, dataService)
        
        viewController.viewModel = viewModel
        
        let login = viewModel.output.showTabBar
            .flatMap { [weak self] todayFutureEventsCountOnAuth -> Observable<Void> in
                guard let strongSelf = self else {
                    return .empty()
                }
                return strongSelf.showTabBar(todayFutureEventsCountOnAuth)
            }
//            .do(onCompleted: { [weak self] in
//                guard let strongSelf = self else {
//                    return
//                }
//                strongSelf.showTabBar()
//                    .subscribe()
//                    .disposed(by: strongSelf.disposeBag)
//            })
//            .map { _ -> Void in
//                return
//            }
        
        viewModel.output.error
            .flatMap({ [weak self] error-> Observable<GGAlertCoordinationResult> in
                guard let strongSelf = self else { return .empty() }
                return strongSelf.showError(error, viewController)
            })
            .subscribe()
            .disposed(by: disposeBag)
        
        
        navigationController.pushViewController(viewController, animated: true)
        
//        let subject = PublishSubject<Void>()
//        login
//            .subscribe { (event) in
//                switch event {
//                case .completed:
//                    subject.onCompleted()
//                default: break
//                }
//            }
//            .disposed(by: disposeBag)
        
//        showRegistration
//            .subscribe { (event) in
//                switch event {
//                case let .next(elem):
//                    subject.onNext(elem)
//                default: break
//                }
//            }
//            .disposed(by: disposeBag)

        
        let back = viewModel.output.back
            .debug()
            .map({ [weak self] _ -> Void in
                self?.navigationController.popViewController(animated: true)
            })
        
        
        return Observable.merge(login, back)
            .debug()
            .take(1)
        
    }
    
    private func showTabBar(_ todayFutureEventsCountOnAuth: [String : Int]) -> Observable<Void> {
        let tabBarCoordinator = GGTabBarCoordinator(webService, dataService, window, todayFutureEventsCountOnAuth)
        return coordinate(to: tabBarCoordinator)
    }
    
    private func showRegistration() -> Observable<Void> {
        let registrationCoordinator = RegistrationCoordinator(webService, dataService, window, navigationController)
        return coordinate(to: registrationCoordinator)
    }
}
