//
//  GGTabBarCoordinator.swift
//  GoGOA
//
//  Created by m.shilo on 17/02/2019.
//  Copyright © 2019 m.shilo. All rights reserved.
//

import UIKit
import RxSwift


class GGTabBarCoordinator: BaseCoordinator<Void> {
    
    private let webService: WebService
    private let dataService: DataService
    private let authorityService: GGAuthorityService
    private let window: UIWindow
    private let todayFutureEventsCountOnAuth: [String : Int]
    
    enum SectionTab: CaseIterable {
        case events
        case profile
        
        var authorirties: [GGRole] {
            switch self {
            case .events: return [.USER, .TEMP_USER]
            case .profile: return [.USER]
            }
        }
        
        var title: String {
            switch self {
            case .events: return ""
            case .profile: return ""
            }
        }
        
        var image: UIImage {
            switch self {
            case .events: return Constants.icon.party
            case .profile: return Constants.icon.user_man
            }
        }
        
        var tag: Int {
            switch self {
            case .events: return 0
            case .profile: return 1
            }
        }
    }
    
    init(_ webService: WebService, _ dataService: DataService, _ window: UIWindow, _ todayFutureEventsCountOnAuth: [String : Int]) {
        self.webService = webService
        self.dataService = dataService
        self.window = window
        self.authorityService = GGAuthorityService(dataService)
        self.todayFutureEventsCountOnAuth = todayFutureEventsCountOnAuth
    }
    
    override func start() -> Observable<Void> {
        let tabBarController = GGTabBarController()
        
        let tabs: [SectionTab] = SectionTab.allCases.reduce([]) { [weak self] (result, currentTab) -> [SectionTab] in
            guard let strongSelf = self else {
                return result
            }
            if currentTab.authorirties.contains(strongSelf.authorityService.currentRole!) {
                return result + [currentTab]
            }
            return result
        }

        let coordinationResults = Observable.from(configure(tabBarController: tabBarController, with: tabs))
        
        UIView.transition(with: window,
                          duration: 0.3,
                          options: .transitionCrossDissolve,
                          animations: { [weak self] in
                            self?.window.rootViewController = tabBarController
            }, completion: nil)
        
        return coordinationResults.merge()
    }

    
    private func configure(tabBarController: UITabBarController, with tabs: [SectionTab]) -> [Observable<Void>] {
        let navControllers = tabs
            .map { tab -> UINavigationController in
                let navigationController = UINavigationController()
                navigationController.tabBarItem = UITabBarItem(title: tab.title, image: tab.image, tag: tab.tag)
                return navigationController
        }
        
        tabBarController.viewControllers = navControllers
        tabBarController.view.backgroundColor = UIColor.white  // Fix dark shadow in nav bar on segue
        
        return zip(tabs, navControllers)
            .map { [weak self] (tab, navController) in
                guard let strongSelf = self else {
                    return .empty()
                }
                switch tab {
                case .events:
                    let coordinator = EventsCoordinator(strongSelf.webService, strongSelf.dataService, strongSelf.authorityService, window: nil, navigationController: navController, todayFutureEventsCountOnAuth)
                    return coordinate(to: coordinator)
                case .profile:
                    let coordinator = ProfileCoordinator(strongSelf.webService, strongSelf.dataService, navController, strongSelf.authorityService.currentUser)
                    return coordinate(to: coordinator)
                }
        }
    }
}

