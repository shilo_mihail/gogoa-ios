//
//  GGTabBarController.swift
//  GoGOA
//
//  Created by m.shilo on 17/02/2019.
//  Copyright © 2019 m.shilo. All rights reserved.
//

import UIKit

class GGTabBarController: UITabBarController {
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        UITabBar.appearance().shadowImage = UIImage()
        UITabBar.appearance().backgroundImage = generateBluredImage()
        
    }
    
    private func generateTranslucentImage() -> UIImage? {
        let rect = CGRect(origin: .zero, size: CGSize(width: tabBar.frame.size.width, height: tabBar.frame.size.height))
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)
        UIColor.white.setFill()
        UIRectFill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        guard let cgImage = image?.cgImage else { return nil }
        return UIImage(cgImage: cgImage)
    }
    
    private func generateBluredImage() -> UIImage? {
        let rect = CGRect(origin: .zero, size: CGSize(width: tabBar.frame.size.width, height: tabBar.frame.size.height))
        let view = UIView(frame: rect)
        let visualEffectView = UIVisualEffectView(effect: UIBlurEffect(style: .light))
        visualEffectView.frame = rect
        view.addSubview(visualEffectView)
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)
        view.drawHierarchy(in: view.bounds, afterScreenUpdates: true)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return img
    }
    
//    func configureBackgroundImage() {
//        let backgroundImage = Constants.image.background
//        let backgroundImageView = UIImageView(image: backgroundImage)
//
//        backgroundImageView.frame.origin.x = -150
//        backgroundImageView.frame.origin.y = -150
//
//        view.insertSubview(backgroundImageView, at: 0)
//    }
    
}


