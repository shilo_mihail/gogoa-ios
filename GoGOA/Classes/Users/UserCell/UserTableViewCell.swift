//
//  UserTableViewCell.swift
//  GoGOA
//
//  Created by m.shilo on 20/08/2019.
//  Copyright © 2019 m.shilo. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class UserTableViewCell: UITableViewCell {

    typealias ViewModelType = UserCellViewModel
    
    var viewModel: ViewModelType!
    
    var disposeBag = DisposeBag()
    
    
    @IBOutlet weak var blurView: UIVisualEffectView! {
        didSet {
            blurView.layer.cornerRadius = 10
            blurView.clipsToBounds = true
        }
    }
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var iconView: UIView! {
        didSet {
            iconView.layer.cornerRadius = iconView.bounds.height / 2
            iconView.clipsToBounds = true
        }
    }

    
    func configure() {
        nameLabel.text = viewModel.user.name
        
        if let data = viewModel.user.photoIcon?.smallPhotoBinary?.data {
            let imageView = UIImageView(image: UIImage(data: data))
            imageView.frame = CGRect(origin: CGPoint.zero, size: iconView.frame.size)
            iconView.addSubview(imageView)
        } else {
            let imageView = UIImageView(image: UIImage(UIColor(hex: viewModel.user.iconHexColor)))
            imageView.frame = CGRect(origin: CGPoint.zero, size: iconView.frame.size)
            imageView.contentMode = .scaleAspectFill
            iconView.addSubview(imageView)
            let initials = Utils.parseFirstCharacters(viewModel.user.name)
            let initialsLabel = UILabel(frame: CGRect(origin: CGPoint.zero, size: iconView.frame.size))
            initialsLabel.textColor = .white
            initialsLabel.text = initials
            initialsLabel.textAlignment = .center
            iconView.addSubview(initialsLabel)
        }
        
    }
    
}
