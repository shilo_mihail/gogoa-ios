//
//  UserCellViewModel.swift
//  GoGOA
//
//  Created by m.shilo on 20/08/2019.
//  Copyright © 2019 m.shilo. All rights reserved.
//

import Foundation
import RxSwift

class UserCellViewModel: ViewModelProtocol {
    
    struct Input {}
    struct Output {}
    
    let input: Input
    let output: Output
    
    let user: GGUser
    
    private let disposeBag = DisposeBag()
    
    init(_ user: GGUser) {
        input = Input()
        output = Output()
        
        self.user = user
    }
}
