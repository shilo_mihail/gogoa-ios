//
//  UsersCoordinator.swift
//  GoGOA
//
//  Created by m.shilo on 20/08/2019.
//  Copyright © 2019 m.shilo. All rights reserved.
//

import UIKit
import RxSwift

class UsersCoordinator: BaseCoordinator<Void> {
    
    private let webService: WebService
    private let dataService: DataService
    private let navigationController: UINavigationController
    private let users: [GGUser]
    
    init(_ webService: WebService,
         _ dataService: DataService,
         _ navigationController: UINavigationController,
         _ users: [GGUser]) {
        self.webService = webService
        self.dataService = dataService
        self.navigationController = navigationController
        self.users = users
    }
    
    override func start() -> Observable<Void> {
        let viewController = UsersViewController.initFromXib()
        
        let viewModel = UsersViewModel(webService, dataService, users: users)
        viewController.viewModel = viewModel
        
        viewModel.output.selectedUser
            .map({ [weak self] user -> Observable<Void> in
                guard let strongSelf = self else {
                    return .empty()
                }
                return strongSelf.showProfile(in: strongSelf.navigationController, with: user)
            })
            .map({ [weak self] observable in
                guard let strongSelf = self else {
                    return
                }
                observable
                    .subscribe()
                    .disposed(by: strongSelf.disposeBag)
                return
            })
            .subscribe()
            .disposed(by: disposeBag)
        
        
        navigationController.pushViewController(viewController, animated: true)
        
        return viewModel.output.back
            .take(1)
    }
    
    private func showProfile(in navigationController: UINavigationController, with user: GGUser) -> Observable<Void> {
        let profileCoordinator = ProfileCoordinator(webService, dataService, navigationController, user)
        return coordinate(to: profileCoordinator)
    }
}
