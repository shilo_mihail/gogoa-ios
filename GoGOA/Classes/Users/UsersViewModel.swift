//
//  UsersViewModel.swift
//  GoGOA
//
//  Created by m.shilo on 20/08/2019.
//  Copyright © 2019 m.shilo. All rights reserved.
//

import Foundation
import RxSwift

class UsersViewModel: ViewModelProtocol {
    
    struct Input {
        let userDidSelect: AnyObserver<GGUser>
        let backButtonDidTap: AnyObserver<Void>
    }
    struct Output {
        let users: Observable<[GGUsersSection]>
        let selectedUser: Observable<GGUser>
        let back: Observable<Void>
    }
    
    let input: Input
    let output: Output
    
    private let userDidSelectSubject = PublishSubject<GGUser>()
    private let backButtonDidTapSubject = PublishSubject<Void>()
    
    private let usersSubject: BehaviorSubject<[GGUsersSection]>
    private let selectedUserSubject = PublishSubject<GGUser>()
    private let backSubject = PublishSubject<Void>()
    
    private let disposeBag = DisposeBag()
    
    var userViewModels: [UserCellViewModel] = []
    
    init(_ webService: WebService, _ dataService: DataService, users: [GGUser]) {
        for user in users {
            let viewModel = UserCellViewModel(user)
            userViewModels.append(viewModel)
        }
        self.usersSubject = BehaviorSubject<[GGUsersSection]>(value: [GGUsersSection(items: users)])
        input = Input(userDidSelect: userDidSelectSubject.asObserver(),
                      backButtonDidTap: backButtonDidTapSubject.asObserver())
        
        output = Output(users: usersSubject.asObservable(),
                        selectedUser: selectedUserSubject.asObservable(),
                        back: backSubject.asObservable())
        
        
        
        userDidSelectSubject
            .subscribe(onNext: { [weak self] result in
                self?.selectedUserSubject.onNext(result)
            })
            .disposed(by: disposeBag)
        
        
        backButtonDidTapSubject
            .subscribe(onNext: { [weak self] in
                self?.backSubject.onNext(())
            })
            .disposed(by: disposeBag)
    }
    
}
