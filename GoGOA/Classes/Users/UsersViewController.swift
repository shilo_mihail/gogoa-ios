//
//  UsersViewController.swift
//  GoGOA
//
//  Created by m.shilo on 20/08/2019.
//  Copyright © 2019 m.shilo. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import FontAwesome_swift
import RxDataSources

struct GGUsersSection {
    var items: [GGUser]
}
extension GGUsersSection: AnimatableSectionModelType {
    typealias Item = GGUser
    var identity: Int {
        return 0
    }
    init(original: GGUsersSection, items: [GGUser]) {
        self = original
        self.items = items
    }
}

class UsersViewController: ParallaxViewController, ControllerType, XibInitializable {
    
    typealias ViewModelType = UsersViewModel
    
    var viewModel: ViewModelType!
    
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.register(UINib(nibName: String(describing: UserTableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: UserTableViewCell.self))
        }
    }
    
    var disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        bind()
//        configureNavBar()
        
        currentLine = .second
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print("resources: \(RxSwift.Resources.total)")
    }
    
    func bind() {
        
        tableView.rx.modelSelected(GGUser.self).asObservable()
            .bind(to: viewModel.input.userDidSelect)
            .disposed(by: disposeBag)
        
        let dataSource = RxTableViewSectionedAnimatedDataSource<GGUsersSection>(configureCell: { [weak self] dataSource, tableView, indexPath, user in
            guard let strongSelf = self else {
                return UITableViewCell()
            }
            
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: UserTableViewCell.self)) as! UserTableViewCell

            cell.viewModel = strongSelf.viewModel.userViewModels.first(where: {$0.user.id == user.id})
            cell.configure()
            
            return cell
        })
        
        viewModel.output.users
            .map {users -> [GGUsersSection] in
                print(users)
                return users
            }
            .bind(to: tableView.rx.items(dataSource: dataSource))
            .disposed(by: disposeBag)
    }

    
}
