//
//  PhotoViewModel.swift
//  GoGOA
//
//  Created by m.shilo on 11/10/2019.
//  Copyright © 2019 m.shilo. All rights reserved.
//

import RxSwift
import Foundation
import Photos

class GGViewPhotoViewModel: ViewModelProtocol {
    
    struct Input {
        let closeDidTap: AnyObserver<Void>
    }
    struct Output {
        let close: Observable<Void>
        let photoData: Observable<Data>
        let send: Observable<Data>
        let error: Observable<AppError>
        let loading: Observable<Bool>
    }
    
    let input: Input
    let output: Output
    
    //INPUT
    private let closeDidTapSubject = PublishSubject<Void>()
    private let sendDidTapSubject = PublishSubject<Void>()
    //OUTPUT
    private let closeSubject = PublishSubject<Void>()
    private let photoDataSubject = ReplaySubject<Data>.create(bufferSize: 1)
    private let sendSubject = PublishSubject<Data>()
    private let errorSubject = PublishSubject<AppError>()
    private let loadingSubject = PublishSubject<Bool>()
    
    private let disposeBag = DisposeBag()
    
    init() {
        input = Input(closeDidTap: closeDidTapSubject.asObserver())
        output = Output(close: closeSubject.asObservable(),
                        photoData: photoDataSubject.asObservable(),
                        send: sendSubject.asObservable(),
                        error: errorSubject.asObservable(),
                        loading: loadingSubject.asObservable())
    }
    
    convenience init(_ webService: WebService, _ dataService: DataService, _ photo: Photo) {
        self.init()
        
        if photo.photoBinary!.data.isEmpty {
            photoDataSubject.onNext(photo.smallPhotoBinary!.data)
            loadingSubject.onNext(true)
            webService.getPhotoBinary(by: photo.photoBinary!.id)
                .subscribe(onNext: { [weak self] result in
                    guard let strongSelf = self else { return }
                    strongSelf.loadingSubject.onNext(false)
                    switch result {
                    case .success(let photoBinary):
                        dataService.savePhotoBinaryData(photoBinary.data, photoBinary.id)
                        strongSelf.photoDataSubject.onNext(photoBinary.data)
                    default:
                        break
                    }
                })
                .disposed(by: disposeBag)
        } else {
            photoDataSubject.onNext(photo.photoBinary!.data)
        }
        
        closeDidTapSubject
            .bind(to: closeSubject)
            .disposed(by: disposeBag)
    }
    
    convenience init(_ photoAssetService: PhotoAssetService, _ asset: PHAsset) {
        self.init()
        
        photoAssetService.fetchImageData(asset)
            .filter { $0 != nil }
            .map { $0! }
            .bind(to: photoDataSubject)
            .disposed(by: disposeBag)
        
        
        
        closeDidTapSubject
            .bind(to: closeSubject)
            .disposed(by: disposeBag)
        
        sendDidTapSubject
            .withLatestFrom(photoDataSubject)
            .bind(to: sendSubject)
            .disposed(by: disposeBag)
    }
    
    deinit {
        closeDidTapSubject.on(.completed)
        sendDidTapSubject.on(.completed)
        
        closeSubject.on(.completed)
        photoDataSubject.on(.completed)
        sendSubject.on(.completed)
        errorSubject.on(.completed)
        loadingSubject.on(.completed)
    }
}
