//
//  GGPhotoViewController.swift
//  GoGOA
//
//  Created by m.shilo on 11/10/2019.
//  Copyright © 2019 m.shilo. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class GGViewPhotoViewController: GGBasePhotoViewController, ControllerType, XibInitializable {
    
    typealias ViewModelType = GGViewPhotoViewModel
    
    var viewModel: ViewModelType!
    
    var disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bind()
    }


    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print("resources: \(RxSwift.Resources.total)")
    }
    
    func bind() {
        viewModel.output.photoData
            .map({ data -> UIImage in
                return UIImage(data: data)!
            })
            .bind(to: photoImageView.rx.image)
            .disposed(by: disposeBag)
        
        viewModel.output.loading
            .bind(to: loadingActivityIndicator.rx.isAnimating)
            .disposed(by: disposeBag)
        
        backBtn.rx.tap
            .bind(to: viewModel.input.closeDidTap)
            .disposed(by: disposeBag)
    }

}
