//
//  PhotoCoordinator.swift
//  GoGOA
//
//  Created by m.shilo on 11/10/2019.
//  Copyright © 2019 m.shilo. All rights reserved.
//

import UIKit
import RxSwift


class GGViewPhotoCoordinator: BaseCoordinator<GGPhotoCoordinatorResult> {
    
    private lazy var photoPresentationManager = PhotoPresentationManager()
    private let parentViewController: UIViewController
    
    private let webService: WebService
    private let dataService: DataService
    private let photo: Photo
    
    init(_ parentViewController: UIViewController,
         _ webService: WebService,
         _ dataService: DataService,
         _ photo: Photo) {
        self.parentViewController = parentViewController
        self.webService = webService
        self.dataService = dataService
        self.photo = photo
    }
    
    override func start() -> Observable<CoordinationResult> {
        let viewController = GGViewPhotoViewController.initFromXib()
        let viewModel = GGViewPhotoViewModel(webService, dataService, photo)
        viewController.viewModel = viewModel
        
        
        viewController.transitioningDelegate = photoPresentationManager
        viewController.modalPresentationStyle = .custom
        parentViewController.present(viewController, animated: true)
        
        let close = viewModel.output.close.map { CoordinationResult.close }
        
        return close
            .take(1)
            .do(onNext: { [weak self] _ in
                self?.parentViewController.dismiss(animated: true)
            })
    }
    
}
