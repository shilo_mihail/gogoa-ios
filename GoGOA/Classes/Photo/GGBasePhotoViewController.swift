//
//  GGBasePhotoViewController.swift
//  GoGOA
//
//  Created by m.shilo on 13.07.2020.
//  Copyright © 2020 m.shilo. All rights reserved.
//

import UIKit

class GGBasePhotoViewController: UIViewController, UIScrollViewDelegate {
    
    @IBOutlet weak var backgroundView: UIView! {
        didSet {
            backgroundView.alpha = 0
        }
    }
    @IBOutlet weak var backBtn: UIButton! {
        didSet {
            backBtn.alpha = 0
            let templateImage = Constants.image.backArrow.withRenderingMode(.alwaysTemplate)
            backBtn.setImage(templateImage, for: .normal)
            backBtn.tintColor = Constants.color.button.lightOnDark
        }
    }
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var photoImageView: UIImageView! {
        didSet {
            let doubleTap = UITapGestureRecognizer(target: self, action: #selector(minimizeMaximizeZoom))
            doubleTap.numberOfTapsRequired = 2
            photoImageView.addGestureRecognizer(doubleTap)
        }
    }
    @IBOutlet weak var loadingActivityIndicator: UIActivityIndicatorView!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        scrollView.delegate = self
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return photoImageView
    }
        
    @objc func minimizeMaximizeZoom() {
        scrollView.setZoomScale(scrollView.zoomScale == 6 ? 1 : 6, animated: true)
    }

}

