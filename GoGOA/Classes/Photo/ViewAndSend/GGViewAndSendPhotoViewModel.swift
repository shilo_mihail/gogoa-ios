//
//  PhotoViewModel.swift
//  GoGOA
//
//  Created by m.shilo on 11/10/2019.
//  Copyright © 2019 m.shilo. All rights reserved.
//

import RxSwift
import Foundation
import Photos

class GGViewAndSendPhotoViewModel: ViewModelProtocol {
    
    struct Input {
        let closeDidTap: AnyObserver<Void>
        let sendDidTap: AnyObserver<Void>
    }
    struct Output {
        let close: Observable<Void>
        let photoData: Observable<Data>
        let error: Observable<AppError>
        let loading: Observable<Bool>
        let send: Observable<Data>
    }
    
    let input: Input
    let output: Output
    
    //INPUT
    private let closeDidTapSubject = PublishSubject<Void>()
    private let sendDidTapSubject = PublishSubject<Void>()
    //OUTPUT
    private let closeSubject = PublishSubject<Void>()
    private let photoDataSubject = ReplaySubject<Data>.create(bufferSize: 1)
    private let errorSubject = PublishSubject<AppError>()
    private let loadingSubject = PublishSubject<Bool>()
    private let sendSubject = PublishSubject<Data>()
    
    private let disposeBag = DisposeBag()
    
    init(_ photoAssetService: PhotoAssetService,
         _ asset: PHAsset) {
        input = Input(closeDidTap: closeDidTapSubject.asObserver(),
                      sendDidTap: sendDidTapSubject.asObserver())
        output = Output(close: closeSubject.asObservable(),
                        photoData: photoDataSubject.asObservable(),
                        error: errorSubject.asObservable(),
                        loading: loadingSubject.asObservable(),
                        send: sendSubject.asObservable())
        
        //        let targetSize = CGSize(width: PhotoGalleryCollectionViewCell.height(), height: PhotoGalleryCollectionViewCell.height())
        photoAssetService.fetchImageData(asset)
            .filter { $0 != nil }
            .map { $0! }
            .bind(to: photoDataSubject)
            .disposed(by: disposeBag)
        
        commonInit()
    }
    
    init(_ imageData: Data) {
        input = Input(closeDidTap: closeDidTapSubject.asObserver(),
                      sendDidTap: sendDidTapSubject.asObserver())
        output = Output(close: closeSubject.asObservable(),
                        photoData: photoDataSubject.asObservable(),
                        error: errorSubject.asObservable(),
                        loading: loadingSubject.asObservable(),
                        send: sendSubject.asObservable())
        photoDataSubject.onNext(imageData)
        
        commonInit()
    }
    
    private func commonInit() {
        sendDidTapSubject
            .withLatestFrom(photoDataSubject)
            .bind(to: sendSubject)
            .disposed(by: disposeBag)
        
        closeDidTapSubject
            .bind(to: closeSubject)
            .disposed(by: disposeBag)
    }
}
