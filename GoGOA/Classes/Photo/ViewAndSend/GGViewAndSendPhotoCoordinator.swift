//
//  PhotoCoordinator.swift
//  GoGOA
//
//  Created by m.shilo on 11/10/2019.
//  Copyright © 2019 m.shilo. All rights reserved.
//

import UIKit
import RxSwift
import Photos


class GGViewAndSendPhotoCoordinator: BaseCoordinator<GGPhotoCoordinatorResult> {
    
    private lazy var photoPresentationManager = PhotoPresentationManager()
    private let parentViewController: UIViewController

    private let photoAssetService: PhotoAssetService?
    private let asset: PHAsset?
    private let photoData: Data?
    
    init(_ parentViewController: UIViewController,
         _ photoAssetService: PhotoAssetService?,
         _ asset: PHAsset?,
         _ photoData: Data?) {
        self.parentViewController = parentViewController
        self.photoAssetService = photoAssetService
        self.asset = asset
        self.photoData = photoData
    }
    
    override func start() -> Observable<CoordinationResult> {
        let viewController = GGViewAndSendPhotoViewController.initFromXib()
        var viewModel: GGViewAndSendPhotoViewModel!
        if let _ = photoAssetService, let _ = asset {
            viewModel = GGViewAndSendPhotoViewModel(photoAssetService!, asset!)
        } else if let _ = photoData {
            viewModel = GGViewAndSendPhotoViewModel(photoData!)
        }
        viewController.viewModel = viewModel
        
        viewController.transitioningDelegate = photoPresentationManager
        viewController.modalPresentationStyle = .custom
        parentViewController.present(viewController, animated: true)
        
        let close = viewModel.output.close.map { CoordinationResult.close }
        let send = viewModel.output.send.map { CoordinationResult.send($0) }
        
        return Observable.merge(close, send)
            .take(1)
            .do(onNext: { [weak self] _ in
                self?.parentViewController.dismiss(animated: true)
            })
    }
    
}
