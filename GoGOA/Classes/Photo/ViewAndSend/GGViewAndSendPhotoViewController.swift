//
//  GGPhotoViewController.swift
//  GoGOA
//
//  Created by m.shilo on 11/10/2019.
//  Copyright © 2019 m.shilo. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class GGViewAndSendPhotoViewController: GGBasePhotoViewController, ControllerType, XibInitializable {
    
    typealias ViewModelType = GGViewAndSendPhotoViewModel
    
    var viewModel: ViewModelType!
    
    @IBOutlet weak var sendBtn: UIButton! {
        didSet {
            sendBtn.setTitle("Send!", for: .normal)
        }
    }
    

    var disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bind()
    }


    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print("resources: \(RxSwift.Resources.total)")
    }
    
    func bind() {
        viewModel.output.photoData
            .map({ data -> UIImage in
                return UIImage(data: data)!
            })
            .bind(to: photoImageView.rx.image)
            .disposed(by: disposeBag)
        
        viewModel.output.loading
            .bind(to: loadingActivityIndicator.rx.isAnimating)
            .disposed(by: disposeBag)
        
        backBtn.rx.tap
            .bind(to: viewModel.input.closeDidTap)
            .disposed(by: disposeBag)
        
        sendBtn.rx.tap
            .bind(to: viewModel.input.sendDidTap)
            .disposed(by: disposeBag)
        
    }

}
