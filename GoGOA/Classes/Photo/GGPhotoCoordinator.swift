//
//  PhotoCoordinator.swift
//  GoGOA
//
//  Created by m.shilo on 11/10/2019.
//  Copyright © 2019 m.shilo. All rights reserved.
//

import UIKit
import RxSwift
import Photos

enum GGPhotoType {
    case photo
    case asset
}

enum GGPhotoCoordinatorResult {
    case close
    case send(Data)
}

class GGPhotoCoordinator: BaseCoordinator<GGPhotoCoordinatorResult> {
    
    private lazy var photoPresentationManager = PhotoPresentationManager()
    private let parentViewController: UIViewController
    private let type: GGPhotoType
    
    //PHOTO
    private var webService: WebService?
    private var dataService: DataService?
    private var photo: Photo?
    
    //ASSET
    private var photoAssetService: PhotoAssetService?
    private var asset: PHAsset?
    
    private init(_ parentViewController: UIViewController,
                 _ type: GGPhotoType) {
        self.parentViewController = parentViewController
        self.type = type
    }
    
    convenience init(_ parentViewController: UIViewController,
                     _ type: GGPhotoType,
                     _ webService: WebService,
                     _ dataService: DataService,
                     _ photo: Photo) {
        self.init(parentViewController, type)
        
        self.webService = webService
        self.dataService = dataService
        self.photo = photo
    }
    
    convenience init(_ parentViewController: UIViewController,
                     _ type: GGPhotoType,
                     _ photoAssetService: PhotoAssetService,
                     _ asset: PHAsset) {
        self.init(parentViewController, type)
        
        self.photoAssetService = photoAssetService
        self.asset = asset
    }
    
    override func start() -> Observable<CoordinationResult> {
        let viewController = GGPhotoViewController.initFromXib()
        
        var viewModel: GGPhotoViewModel!
        switch type {
        case .photo:
            viewModel = GGPhotoViewModel(webService!, dataService!, photo!)
        case .asset:
            viewModel = GGPhotoViewModel(photoAssetService!, asset!)
        }
        viewController.type = type
        viewController.viewModel = viewModel
        
        viewController.transitioningDelegate = photoPresentationManager
        viewController.modalPresentationStyle = .custom
        
        print(UIApplication.shared.keyWindow?.rootViewController)
        print(parentViewController)
        print(viewController)
        
        parentViewController.present(viewController, animated: true)
        
        let close = viewModel.output.close.map { CoordinationResult.close }
        
        return close
            .take(1)
            .do(onNext: { [weak self] _ in
                self?.parentViewController.dismiss(animated: true)
            })
    }
    
    @objc private func presentController(_ viewController: GGPhotoViewController) {
        parentViewController.present(viewController, animated: true)
    }
}
