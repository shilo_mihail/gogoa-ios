//
//  GGPhotoCoordinatorResult.swift
//  GoGOA
//
//  Created by m.shilo on 13.07.2020.
//  Copyright © 2020 m.shilo. All rights reserved.
//

import Foundation

enum GGPhotoCoordinatorResult {
    case close
    case send(Data)
}
