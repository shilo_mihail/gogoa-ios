//
//  EventViewController.swift
//  GoGOA
//
//  Created by m.shilo on 29/01/2019.
//  Copyright © 2019 m.shilo. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class EventViewController: ParallaxViewController, ControllerType, XibInitializable {
    
    let dateFormat = "dd MMMM HH:mm"
    let topEdgeInset: CGFloat = 250
    
    typealias ViewModelType = EventViewModel
    
    var viewModel: ViewModelType!
    
    var disposeBag = DisposeBag()
    
    
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.register(UINib(nibName: String(describing: EventInfoCell.self), bundle: nil), forCellReuseIdentifier: String(describing: EventInfoCell.self))
            tableView.contentInsetAdjustmentBehavior = .never
            tableView.contentInset = UIEdgeInsets(top: topEdgeInset, left: 0, bottom: 0, right: 0)
        }
    }

    @IBOutlet weak var imageContainerView: UIView!
    @IBOutlet weak var imageContainerViewHeight: NSLayoutConstraint!
    @IBOutlet weak var imageShimmerBlurView: GGShimmerBlurView!
    @IBOutlet weak var imageView: UIImageView!
    
    //    var eventInfoCell = EventInfoCell()
    

    
    override func viewDidLoad(){
        super.viewDidLoad()
        
        bind()
        
        currentLine = .second
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print("resources: \(RxSwift.Resources.total)")
    }
    
    func bind() {
        
        viewModel.output.event
            .map({ [$0] })
            .bind(to: tableView.rx.items(cellIdentifier: String(describing: EventInfoCell.self), cellType: EventInfoCell.self)) { [weak self] (index, event, cell) in
                guard let strongSelf = self else { return }

                strongSelf.navigationItem.title = event.title
                
                if let photoBinaryData = event.photoIcon?.photoBinary?.data, !photoBinaryData.isEmpty {
                    strongSelf.imageView.image = UIImage(data: photoBinaryData)
                } else if let smallPhotoBinaryData = event.photoIcon?.smallPhotoBinary?.data {
                    strongSelf.viewModel.output.image.data
                        .map { data -> UIImage in
                            return UIImage(data: data)!
                        }
                        .bind(to: strongSelf.imageView.rx.image)
                        .disposed(by: strongSelf.disposeBag)
                    
                    strongSelf.viewModel.output.image.loading
                        .do(onNext: { loading in
                            if !loading {
                                strongSelf.imageView.alpha = 1
                            }
                        })
                        .bind(to: strongSelf.imageShimmerBlurView.rx.loading)
                        .disposed(by: strongSelf.disposeBag)
                    
                    strongSelf.imageView.alpha = 0.5
                    strongSelf.imageShimmerBlurView.startAnimating()
                    strongSelf.imageView.image = UIImage(data: smallPhotoBinaryData)
                }

                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = strongSelf.dateFormat
                cell.dateTimeLabel.text = dateFormatter.string(from: event.eventDate)
                cell.locationAddressBtn.setTitle(event.location?.address, for: .normal)

                cell.viewModel = strongSelf.viewModel.eventInfoViewModel
                cell.configure()
            }
            .disposed(by: disposeBag)
        
        tableView.rx.didScroll
            .subscribe(onNext: { [weak self] _ in
                guard let strongSelf = self else { return }
                
                let y = strongSelf.topEdgeInset - (strongSelf.tableView.contentOffset.y + strongSelf.topEdgeInset)
                strongSelf.imageContainerViewHeight.constant = min(max(y, 100), 400)
            })
            .disposed(by: disposeBag)
        
        navigationController?.rx.willShow
            .asObservable()
            .subscribe(onNext: { [weak self] (viewController, _) in
                if let navigationController = viewController.navigationController {
                    let isContainSelf = navigationController.viewControllers.contains(where: { $0 == self })
                    if  !isContainSelf {
                        self?.viewModel.input.backButtonDidTap.onNext(())
                    }
                }
            })
            .disposed(by: disposeBag)
    }
}
