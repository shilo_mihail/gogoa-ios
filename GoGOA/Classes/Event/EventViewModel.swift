//
//  EventViewModel.swift
//  GoGOA
//
//  Created by m.shilo on 29/01/2019.
//  Copyright © 2019 m.shilo. All rights reserved.
//

import RxSwift
import Foundation

class EventViewModel: ViewModelProtocol {
    
    struct Input {
        let backButtonDidTap: AnyObserver<Void>
    }
    struct Output {
        let event: Observable<GGEvent>
        let image: Image
        let error: Observable<AppError>
        let loading: Observable<Bool>
        let back: Observable<Void>
        
        struct Image {
            let data: Observable<Data>
            let loading: Observable<Bool>
        }
    }
    
    let input: Input
    let output: Output
    
    
    //INPUT
    private let backButtonDidTapSubject = PublishSubject<Void>()
    //OUTPUT
    private let eventSubject: BehaviorSubject<GGEvent>
    private let errorSubject = PublishSubject<AppError>()
    private let loadingSubject = PublishSubject<Bool>()
    private let backSubject = PublishSubject<Void>()
    
    private let disposeBag = DisposeBag()
    
    var eventInfoViewModel: EventInfoViewModel!
    
    
    init(_ webService: WebService,
         _ dataService: DataService,
         _ authorityService: GGAuthorityService,
         event: GGEvent,
         outputImage: Output.Image) {
        eventSubject = BehaviorSubject<GGEvent>(value: event)
        input = Input(backButtonDidTap: backButtonDidTapSubject.asObserver())
        
        output = Output(event: eventSubject.asObservable(),
                        image: outputImage,
                        error: errorSubject.asObservable(),
                        loading: loadingSubject.asObservable(),
                        back: backSubject.asObservable())
        
        
        eventInfoViewModel = EventInfoViewModel(webService, dataService, authorityService, event: event)

        backButtonDidTapSubject
            .bind(to: backSubject)
            .disposed(by: disposeBag)
    }
    
    deinit {
        backButtonDidTapSubject.on(.completed)
        
        eventSubject.on(.completed)
        errorSubject.on(.completed)
        loadingSubject.on(.completed)
        backSubject.on(.completed)
    }
    
}

