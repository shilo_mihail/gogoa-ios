//
//  EventCoordinator.swift
//  GoGOA
//
//  Created by m.shilo on 29/01/2019.
//  Copyright © 2019 m.shilo. All rights reserved.
//

import UIKit
import RxSwift

enum EventCoordinationResult {
    case showLocation
    case showError(AppError)
    case back
}

class EventCoordinator: BaseCoordinator<Void> {
    
    private let webService: WebService
    private let dataService: DataService
    private let authorityService: GGAuthorityService
    private let navigationController: UINavigationController
    private let event: GGEvent
    private let outputImage: EventViewModel.Output.Image
    
    
    init(_ webService: WebService,
         _ dataService: DataService,
         _ authorityService: GGAuthorityService,
         _ navigationController: UINavigationController,
         _ event: GGEvent,
         _ outputImage: EventViewModel.Output.Image) {
        self.webService = webService
        self.dataService = dataService
        self.authorityService = authorityService
        self.navigationController = navigationController
        self.event = event
        self.outputImage = outputImage
    }
    
    override func start() -> Observable<Void> {
        let viewController = EventViewController.initFromXib()
        
        let viewModel = EventViewModel(webService, dataService, authorityService, event: event, outputImage: outputImage)
        viewController.viewModel = viewModel
        
        
        
        viewModel.output.error
            .subscribe(onNext: { [weak self] error in
                _ = self?.showError(error, viewController)
            })
            .disposed(by: disposeBag)
        
        viewModel.eventInfoViewModel.output.showLocation
            .map({ [weak self] location -> Observable<Void> in
                guard let strongSelf = self else {
                    return .empty()
                }
                return strongSelf.showLocation(location)
            })
            .subscribe()
            .disposed(by: disposeBag)
        
        viewModel.eventInfoViewModel.output.showUsers
            .map({ [weak self] users -> Observable<Void> in
                guard let strongSelf = self else {
                    return .empty()
                }
                return strongSelf.showUsers(users)
            })
            .subscribe()
            .disposed(by: disposeBag)
        
        
        navigationController.pushViewController(viewController, animated: true)
        
        return viewModel.output.back
            .take(1)
    }
    
    private func showLocation(_ location: Location) -> Observable<Void> {
        let locationCoordinator = GGLocationCoordinator(webService, dataService, navigationController, location)
        return coordinate(to: locationCoordinator)
    }
    
    private func showUsers(_ users: [GGUser]) -> Observable<Void> {
        let usersCoordinator = UsersCoordinator(webService, dataService, navigationController, users)
        return coordinate(to: usersCoordinator)
    }
}
