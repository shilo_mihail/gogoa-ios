//
//  EventInfoCell.swift
//  GoGOA
//
//  Created by m.shilo on 16/06/2019.
//  Copyright © 2019 m.shilo. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class EventInfoCell: UITableViewCell {
    
    typealias ViewModelType = EventInfoViewModel
    
    var viewModel: ViewModelType!
    
    var disposeBag = DisposeBag()
    
    @IBOutlet weak var dateTimeLabel: UILabel!
    @IBOutlet weak var locationAddressBtn: UIButton!
    @IBOutlet weak var imageList: UsersImageList!
    @IBOutlet weak var addRemoveEventUserBtn: GGRoundedButtonView! {
        didSet {
            addRemoveEventUserBtn.roles = [.USER]
            addRemoveEventUserBtn.startColor = Constants.color.button.loginGroup.leftGragient
            addRemoveEventUserBtn.finishColor = Constants.color.button.loginGroup.rightGradient
        }
    }

    
    func configure() {
        
        viewModel.output.users
            .bind(to: imageList.rx.users)
            .disposed(by: disposeBag)
        
        addRemoveEventUserBtn.tap.rx.event
            .map { _ -> Void in }
            .bind(to: viewModel.input.addRemoveEventUserBtnDidTap)
            .disposed(by: disposeBag)
        
        imageList.tap.rx.event
            .map { _ -> Void in
                print()
            }
            .bind(to: viewModel.input.usersDidTap)
            .disposed(by: disposeBag)
        
        locationAddressBtn.rx.tap
            .bind(to: viewModel.input.locationDidTap)
            .disposed(by: disposeBag)
        
        viewModel.output.addRemoveEventUserLoading
            .bind(to: addRemoveEventUserBtn.rx.loading)
            .disposed(by: disposeBag)
        
        viewModel.output.isAllowed
            .bind(to: addRemoveEventUserBtn.rx.isAllowed)
            .disposed(by: disposeBag)
        
        viewModel.output.addRemoveEventUserBtnTitle
            .bind(to: addRemoveEventUserBtn.rx.title)
            .disposed(by: disposeBag)
        
        viewModel.input.allowedRoles.onNext(addRemoveEventUserBtn.roles)
    }
}
