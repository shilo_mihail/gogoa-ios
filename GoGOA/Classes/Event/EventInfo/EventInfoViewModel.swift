//
//  EventInfoViewModel.swift
//  GoGOA
//
//  Created by m.shilo on 17/07/2019.
//  Copyright © 2019 m.shilo. All rights reserved.
//

import Foundation
import RxSwift

class EventInfoViewModel: ViewModelProtocol {
    
    struct Input {
        let locationDidTap: AnyObserver<Void>
        let usersDidTap: AnyObserver<Void>
        let addRemoveEventUserBtnDidTap: AnyObserver<Void>
        let allowedRoles: AnyObserver<[GGRole]>
    }
    struct Output {
        let users: Observable<[GGUser]>
        let error: Observable<AppError>
        let showLocation: Observable<Location>
        let showUsers: Observable<[GGUser]>
        let addRemoveEventUserLoading: Observable<Bool>
        let addRemoveEventUserBtnTitle: Observable<String>
        let usersLoading: Observable<Bool>
        let isAllowed: Observable<Bool>
    }
    
    let input: Input
    let output: Output
    
    //INPUT
    private let locationDidTapSubject = PublishSubject<Void>()
    private let usersDidTapSubject = PublishSubject<Void>()
    private let addRemoveEventUserBtnDidTapSubject = PublishSubject<Void>()
    private let allowedRolesSubject = PublishSubject<[GGRole]>()
    //OUTPUT
    private let usersSubject = PublishSubject<[GGUser]>()
    private let errorSubject = PublishSubject<AppError>()
    private let showLocationSubject = PublishSubject<Location>()
    private let showUsersSubject = PublishSubject<[GGUser]>()
    private let addRemoveEventUserLoadingSubject = PublishSubject<Bool>()
    private let addRemoveEventUserBtnTitleSubject = PublishSubject<String>()
    private let usersLoadingSubject = PublishSubject<Bool>()
    private let isAllowedSubject = PublishSubject<Bool>()
    
    private let disposeBag = DisposeBag()
    
    
    init(_ webService: WebService,
     _ dataService: DataService,
     _ authorityService: GGAuthorityService,
     event: GGEvent) {
        let currentUser = authorityService.currentUser!
        
        input = Input(locationDidTap: locationDidTapSubject.asObserver(),
                      usersDidTap: usersDidTapSubject.asObserver(),
                      addRemoveEventUserBtnDidTap: addRemoveEventUserBtnDidTapSubject.asObserver(),
                      allowedRoles: allowedRolesSubject.asObserver())
        
        output = Output(users: usersSubject.asObservable(),
                        error: errorSubject.asObservable(),
                        showLocation: showLocationSubject.asObservable(),
                        showUsers: showUsersSubject.asObservable(),
                        addRemoveEventUserLoading: addRemoveEventUserLoadingSubject.asObservable(),
                        addRemoveEventUserBtnTitle: addRemoveEventUserBtnTitleSubject.asObservable(),
                        usersLoading: usersLoadingSubject.asObservable(),
                        isAllowed: isAllowedSubject.asObservable())

        getEventUsers(webService, dataService, event, currentUser)
        
        allowedRolesSubject
            .subscribe(onNext: { [weak self] roles in
                let isAllowed = authorityService.isAllowed(for: roles)
                self?.isAllowedSubject.onNext(isAllowed)
            })
            .disposed(by: disposeBag)
        
        locationDidTapSubject
            .withLatestFrom(Observable.just(event))
            .map({ $0.location! })
            .bind(to: showLocationSubject)
            .disposed(by: disposeBag)
        
        usersDidTapSubject
            .withLatestFrom(Observable.just(event))
            .map({ Array($0.users) })
            .bind(to: showUsersSubject)
            .disposed(by: disposeBag)
        
        addRemoveEventUserBtnDidTapSubject
            .flatMapLatest { [weak self] _ -> Observable<GGResult<Bool>> in
                self?.addRemoveEventUserLoadingSubject.onNext(true)
                
                if event.users.contains { $0.id == currentUser.id} {
                    return webService.removeEventUser(from: event.id)
                } else {
                    return webService.addEventUser(to: event.id)
                }
            }
            .subscribe(onNext: { [weak self] result in
                guard let strongSelf = self else {
                    return
                }
                switch result {
                case .success(let isSuccess):
                    if isSuccess {
                        strongSelf.getEventUsers(webService, dataService, event, currentUser)
                    }
                case .failure(let error):
                    strongSelf.errorSubject.onNext(error)
                }
            })
            .disposed(by: disposeBag)
    }
    
    private func getEventUsers(_ webService: WebService, _ dataService: DataService, _ event: GGEvent, _ currentUser: GGUser) {
        usersLoadingSubject.onNext(true)
        addRemoveEventUserLoadingSubject.onNext(true)
        
        webService.getEventUsers(by: event.id)
            .subscribe(onNext: { [weak self] result in
                guard let strongSelf = self else {
                    return
                }
                switch result {
                case .success(let users):
                    strongSelf.setEventUserBtnTitle(users, currentUser)
                    let eventUsers = dataService.saveEventUsers(users, to: event)
                    strongSelf.usersSubject.onNext(eventUsers)
                case .failure(let error):
                    strongSelf.errorSubject.onNext(error)
                }
                strongSelf.usersLoadingSubject.onNext(false)
                strongSelf.addRemoveEventUserLoadingSubject.onNext(false)
            })
            .disposed(by: disposeBag)
    }
    
    private func setEventUserBtnTitle (_ users: [GGUser], _ currentUser: GGUser) {
        let isCurrentUserEntry = users.contains { $0.id == currentUser.id}
        addRemoveEventUserBtnTitleSubject.onNext(isCurrentUserEntry ? "Let me out(" : "LET ME IN!!!")
    }
    
    deinit {
        locationDidTapSubject.on(.completed)
        usersDidTapSubject.on(.completed)
        addRemoveEventUserBtnDidTapSubject.on(.completed)
        allowedRolesSubject.on(.completed)
        
        errorSubject.on(.completed)
        showLocationSubject.on(.completed)
        showUsersSubject.on(.completed)
        addRemoveEventUserLoadingSubject.on(.completed)
        usersLoadingSubject.on(.completed)
        isAllowedSubject.on(.completed)
    }
}

