//
//  EventTableViewCell.swift
//  GoGOA
//
//  Created by m.shilo on 25/01/2019.
//  Copyright © 2019 m.shilo. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class EventTableViewCell: UITableViewCell {
    
    typealias ViewModelType = EventCellViewModel
    
    var viewModel: ViewModelType! {
        didSet {
            viewModel.output.iconData
                .map { data -> UIImage in
                    return UIImage(data: data)!
                }
                .bind(to: iconImageView.rx.image)
                .disposed(by: disposeBag)


            viewModel.output.imageLoading
                .do(onNext: { [weak self] loading in
                    if !loading {
                        self?.iconImageView.alpha = 1
                    }
                })
                .bind(to: imageShimmerBlurView.rx.loading)
                .disposed(by: disposeBag)
            

            viewModel.output.event
                .bind(to: self.rx.event)
                .disposed(by: disposeBag)
        }
    }
    
    var disposeBag = DisposeBag()

    
    @IBOutlet weak var topShimmerBlurView: GGShimmerBlurView!
    @IBOutlet weak var imageShimmerBlurView: GGShimmerBlurView!
    @IBOutlet weak var bottomShimmerBlurView: GGShimmerBlurView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var peopleCountLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var locationButton: UIButton!

    
    override func prepareForReuse() {
        super.prepareForReuse()
        disposeBag = DisposeBag()
    }
    
    
    static private var cellHeight: CGFloat = -1.0
    static func height() -> CGFloat {
        if cellHeight < 0 {
            let cell = Bundle.main.loadNibNamed(String(describing: EventTableViewCell.self), owner: self, options: nil)![0] as! EventTableViewCell
            cellHeight = cell.frame.size.height
        }
        return cellHeight
    }
}
