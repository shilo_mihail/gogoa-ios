//
//  EventCellViewModel.swift
//  GoGOA
//
//  Created by m.shilo on 28/01/2019.
//  Copyright © 2019 m.shilo. All rights reserved.
//

import Foundation
import RxSwift
import RxDataSources

class EventCellViewModel: ViewModelProtocol {
        
    struct Input {
        let event: AnyObserver<GGEvent?>
    }
    struct Output {
        let iconData: Observable<Data>
        let imageLoading: Observable<Bool>
        let event: Observable<GGEvent?>
    }
    
    let input: Input
    let output: Output
    var event: GGEvent?
    let id: Int!
    
    private let eventInSubject = PublishSubject<GGEvent?>()
    
    private let iconDataSubject = PublishSubject<Data>()
    private let imageLoadingSubject = PublishSubject<Bool>()
    private let eventOutSubject = PublishSubject<GGEvent?>()
    
    private let disposeBag = DisposeBag()
    
    
    init(_ webService: WebService, _ dataService: DataService, id: Int) {
        self.id = id
        input = Input(event: eventInSubject.asObserver())
        
        output = Output(iconData: iconDataSubject.asObservable(),
                        imageLoading: imageLoadingSubject.asObservable(),
                        event: eventOutSubject.asObservable())
        
        eventInSubject
            .do(onNext: { [weak self] event in
                self?.event = event
                self?.eventOutSubject.onNext(event)
            })
            .filter({ $0 != nil && $0!.photoIcon != nil && $0!.photoIcon!.photoBinary != nil && $0!.photoIcon!.photoBinary!.data.isEmpty })
            .startLoading(subject: imageLoadingSubject)
            .flatMap { event -> Observable<GGResult<PhotoBinary>> in
                return webService.getPhotoBinary(by: (event!.photoIcon?.photoBinary?.id)!)
            }
            .stopLoading(subject: imageLoadingSubject)
            .subscribe(onNext: { [weak self] result in
                guard let strongSelf = self else { return }
                switch result {
                case .success(let photoBinary):
                    dataService.savePhotoBinaryData(photoBinary.data, photoBinary.id)
                    strongSelf.iconDataSubject.onNext(photoBinary.data)
                default:
                    break
                }
            })
            .disposed(by: disposeBag)
    }
    
    deinit {
        eventInSubject.on(.completed)
        
        iconDataSubject.on(.completed)
        imageLoadingSubject.on(.completed)
        eventOutSubject.on(.completed)
    }
}

extension EventCellViewModel: IdentifiableType {
    var identity: Int {
        return id
    }
}

extension EventCellViewModel: Equatable {
    static func == (lhs: EventCellViewModel, rhs: EventCellViewModel) -> Bool {
        return lhs.id == rhs.id
    }
}
