//
//  EventsViewModel.swift
//  GoGOA
//
//  Created by m.shilo on 25/01/2019.
//  Copyright © 2019 m.shilo. All rights reserved.
//

import Foundation
import RxSwift

class EventsViewModel: ViewModelProtocol {
    
    struct Input {
        let rowDidSelect: AnyObserver<EventCellViewModel>
        let locationDidTap: AnyObserver<Location>
        let scrolledToNewFuture: AnyObserver<Void>
        let scrolledToNewPast: AnyObserver<Void>
    }
    struct Output {
        let eventViewModels: Observable<[GGEventViewModelsSection]>
        let futureEventsCount: Observable<Int>
        let selectedEventCellViewModel: Observable<EventCellViewModel>
        let selectedLocation: Observable<Location>
        let todayEventsLoading: Observable<Bool>
        let error: Observable<AppError>
    }
    struct State {
        struct Params {
            var loading = false
            var currentCount = 0
            var offset = 0
        }
        
        var todayEventsParams = Params()
        var futureEventsParams = Params()
        var pastEventsParams = Params()
    }
    
    let input: Input
    let output: Output
    
    private var state = State()
    
    private let rowDidSelectSubject = PublishSubject<EventCellViewModel>()
    private let locationDidTapSubject = PublishSubject<Location>()
    private let scrolledToNewFutureSubject = PublishSubject<Void>()
    private let scrolledToNewPastSubject = PublishSubject<Void>()
    
    private let eventViewModelsSubject = BehaviorSubject<[GGEventViewModelsSection]>(value: [])
    private let futureEventsCountSubject = PublishSubject<Int>()
    private let selectedEventCellViewModelSubject = PublishSubject<EventCellViewModel>()
    private let selectedLocationSubject = PublishSubject<Location>()
    private let todayEventsLoadingSubject = PublishSubject<Bool>()
    private let errorSubject = PublishSubject<AppError>()
    
    private let disposeBag = DisposeBag()

    
    init(_ webService: WebService, _ dataService: DataService, _ todayFutureEventsCountOnAuth: [String : Int]) {
        var cellViewModels: [EventCellViewModel] = []
        let futureEventsCount = todayFutureEventsCountOnAuth["futureEventsCount"] ?? 0
        let allEventsCountOnAuth = (todayFutureEventsCountOnAuth["todayEventsCount"] ?? 0) + futureEventsCount
        for i in 0..<allEventsCountOnAuth {
            let cellViewModel = EventCellViewModel(webService, dataService, id: i)
            cellViewModels.append(cellViewModel)
        }
        eventViewModelsSubject.onNext([GGEventViewModelsSection(items: cellViewModels)])
        
        state.futureEventsParams.offset = futureEventsCount
        
        input = Input(rowDidSelect: rowDidSelectSubject.asObserver(),
                      locationDidTap: locationDidTapSubject.asObserver(),
                      scrolledToNewFuture: scrolledToNewFutureSubject.asObserver(),
                      scrolledToNewPast: scrolledToNewPastSubject.asObserver())
        
        output = Output(eventViewModels: eventViewModelsSubject.asObservable(),
                        futureEventsCount: futureEventsCountSubject.asObservable(),
                        selectedEventCellViewModel: selectedEventCellViewModelSubject.asObservable(),
                        selectedLocation: selectedLocationSubject.asObservable(),
                        todayEventsLoading: todayEventsLoadingSubject.asObservable(),
                        error: errorSubject.asObservable())
        
        todayEventsLoadingSubject.onNext(true)
        
        webService.getTodayEvents()
            .stopLoading(subject: todayEventsLoadingSubject)
            .map({ [weak self] result -> GGEventsResponse? in
                guard let strongSelf = self else { return nil}
                
                switch result {
                case .success(let eventsResponse):
                    strongSelf.state.todayEventsParams.currentCount = eventsResponse.events.count
                    for (i, event) in eventsResponse.events.enumerated() {
                        let cellViewModel = cellViewModels[i]
                        cellViewModel.input.event.onNext(event)
                    }
                    dataService.saveEvents(eventsResponse.events)
                    return eventsResponse
                case .failure(let error):
                    strongSelf.errorSubject.onNext(error)
                    return nil
                }
            })
            .filter({ $0 != nil })
            .observeOn(MainScheduler.asyncInstance)
            .map({ [weak self] eventsResponse -> ([GGEventViewModelsSection], Int) in
                guard let strongSelf = self else { return ([], 0) }
                
                if let futureEventsCount = eventsResponse!.futureEventsCount {
                    strongSelf.state.futureEventsParams.currentCount = futureEventsCount
                    for i in 0..<futureEventsCount {
                        let cellViewModel = EventCellViewModel(webService, dataService, id: -1 - i)
                        cellViewModels.insert(cellViewModel, at: 0)
                    }
                }
                
                if let pastEventsCount = eventsResponse!.pastEventsCount {
                    strongSelf.state.pastEventsParams.currentCount = pastEventsCount
                    for _ in 0..<pastEventsCount {
                        let cellViewModel = EventCellViewModel(webService, dataService, id: cellViewModels.count)
                        cellViewModels.append(cellViewModel)
                    }
                }
                
                return ([GGEventViewModelsSection(
                    items: cellViewModels,
                    futureEventsCount: strongSelf.state.futureEventsParams.currentCount,
                    pastEventsCount: strongSelf.state.todayEventsParams.currentCount + strongSelf.state.futureEventsParams.currentCount
                )], strongSelf.state.futureEventsParams.currentCount)
            })
            .filter({ !$0.0.isEmpty })
            .map({ [weak self] result -> Int in
                guard let strongSelf = self else { return 0 }
                
                strongSelf.eventViewModelsSubject.onNext(result.0)
                return result.1
            })
            .bind(to: futureEventsCountSubject)
            .disposed(by: disposeBag)
        
        
        
        rowDidSelectSubject
            .filter({ $0.event != nil })
            .bind(to: selectedEventCellViewModelSubject)
            .disposed(by: disposeBag)
        
        
        scrolledToNewFutureSubject
            .filter({ [weak self] _ -> Bool in
                guard let strongSelf = self else { return false }
                return !strongSelf.state.futureEventsParams.loading
            })
            .flatMap({ [weak self] _ -> Observable<GGResult<GGEventsResponse>> in
                guard let strongSelf = self else { return .empty() }
                
                strongSelf.state.futureEventsParams.loading = true
                return webService.getFutureEvents(strongSelf.state.futureEventsParams.offset)
            })
            .map({ [weak self] result -> GGEventsResponse? in
                guard let strongSelf = self else { return nil}
                
                strongSelf.state.futureEventsParams.loading = false
                switch result {
                case .success(let eventsResponse):
                    let reversedEvents: [GGEvent] = eventsResponse.events.reversed()
                    for (i, event) in reversedEvents.enumerated() {
                        let cellViewModel = cellViewModels[i]
                        cellViewModel.input.event.onNext(event)
                    }
                    strongSelf.state.futureEventsParams.offset += eventsResponse.events.count
                    dataService.saveEvents(eventsResponse.events)
                    return eventsResponse
                case .failure(let error):
                    strongSelf.errorSubject.onNext(error)
                    return nil
                }
            })
            .filter({ $0 != nil })
            .observeOn(MainScheduler.asyncInstance)
            .map({ [weak self] eventsResponse -> [GGEventViewModelsSection] in
                guard let strongSelf = self else { return [] }
                
                if let futureEventsCount = eventsResponse!.futureEventsCount {
                    strongSelf.state.futureEventsParams.currentCount = futureEventsCount
                    for i in 0..<futureEventsCount {
                        let cellViewModel = EventCellViewModel(webService, dataService, id: -i - cellViewModels.count)
                        cellViewModels.insert(cellViewModel, at: 0)
                    }
                }
                
                return ([GGEventViewModelsSection(
                    items: cellViewModels,
                    futureEventsCount: strongSelf.state.futureEventsParams.currentCount,
                    pastEventsCount: strongSelf.state.pastEventsParams.currentCount
                )])
            })
            .filter({ !$0.isEmpty })
            .bind(to: eventViewModelsSubject)
            .disposed(by: disposeBag)

        
        scrolledToNewPastSubject
            .filter({ [weak self] _ -> Bool in
                guard let strongSelf = self else { return false }
                return !strongSelf.state.pastEventsParams.loading
            })
            .flatMap({ [weak self] _ -> Observable<GGResult<GGEventsResponse>> in
                guard let strongSelf = self else { return .empty() }
                
                strongSelf.state.pastEventsParams.loading = true
                return webService.getPastEvents(strongSelf.state.pastEventsParams.offset)
            })
            .map({ [weak self] result -> GGEventsResponse? in
                guard let strongSelf = self else { return nil}
                
                strongSelf.state.pastEventsParams.loading = false
                switch result {
                case .success(let eventsResponse):
                    
                    for (i, event) in eventsResponse.events.enumerated() {
                        let cellViewModel = cellViewModels[strongSelf.state.todayEventsParams.currentCount + strongSelf.state.futureEventsParams.currentCount + strongSelf.state.pastEventsParams.offset + i]
                        cellViewModel.input.event.onNext(event)
                    }
                    strongSelf.state.pastEventsParams.offset += eventsResponse.events.count
                    dataService.saveEvents(eventsResponse.events)
                    return eventsResponse
                case .failure(let error):
                    strongSelf.errorSubject.onNext(error)
                    return nil
                }
            })
            .filter({ $0 != nil })
            .observeOn(MainScheduler.asyncInstance)
            .map({ [weak self] eventsResponse -> [GGEventViewModelsSection] in
                guard let strongSelf = self else { return [] }
                
                if let pastEventsCount = eventsResponse!.pastEventsCount {
                    strongSelf.state.pastEventsParams.currentCount = pastEventsCount
                    for _ in 0..<pastEventsCount {
                        let cellViewModel = EventCellViewModel(webService, dataService, id: cellViewModels.count)
                        cellViewModels.append(cellViewModel)
                    }
                }
                
                return ([GGEventViewModelsSection(
                    items: cellViewModels,
                    futureEventsCount: strongSelf.state.futureEventsParams.currentCount,
                    pastEventsCount: strongSelf.state.todayEventsParams.currentCount + strongSelf.state.futureEventsParams.currentCount + strongSelf.state.pastEventsParams.offset
                )])
            })
            .filter({ !$0.isEmpty })
            .bind(to: eventViewModelsSubject)
            .disposed(by: disposeBag)
        
        locationDidTapSubject
            .bind(to: selectedLocationSubject)
            .disposed(by: disposeBag)
    }
    
    deinit {
        rowDidSelectSubject.on(.completed)
        scrolledToNewFutureSubject.on(.completed)
        scrolledToNewPastSubject.on(.completed)
        
        eventViewModelsSubject.on(.completed)
        futureEventsCountSubject.on(.completed)
        selectedEventCellViewModelSubject.on(.completed)
        errorSubject.on(.completed)
    }
    
}
