//
//  EventsViewController.swift
//  GoGOA
//
//  Created by m.shilo on 25/01/2019.
//  Copyright © 2019 m.shilo. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import FontAwesome_swift
//import RxRealmDataSources
import RxDataSources

struct GGEventViewModelsSection {
    var items: [EventCellViewModel]
    var futureEventsCount: Int? = nil
    var pastEventsCount: Int? = nil
}
extension GGEventViewModelsSection: AnimatableSectionModelType {
    typealias Item = EventCellViewModel
    var identity: Int {
        return 0
    }
    init(original: GGEventViewModelsSection, items: [EventCellViewModel]) {
        self = original
        self.items = items
    }
    
    init(original: GGEventViewModelsSection, items: [EventCellViewModel], futureEventsCount: Int, pastEventsCount: Int) {
        self.init(original: original, items: items)
        self.futureEventsCount = futureEventsCount
        self.pastEventsCount = pastEventsCount
    }
}



class EventsViewController: ParallaxViewController, ControllerType, XibInitializable {
    
    typealias ViewModelType = EventsViewModel
    
    var viewModel: ViewModelType!
    
    var topActivityIndicator = UIActivityIndicatorView(style: .whiteLarge)
    var bottomActivityIndicator = UIActivityIndicatorView(style: .whiteLarge)
    
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.register(UINib(nibName: String(describing: EventTableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: EventTableViewCell.self))
        }
    }
    
    private var isDraggingByUser = false
    private var wasDraggingByUserOnTodayLoading = false
    
    var disposeBag = DisposeBag()
    
    override func viewDidLoad(){
        super.viewDidLoad()
        
        bind()
        configureNavBar()
        
        currentLine = .first
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print("resources: \(RxSwift.Resources.total)")
    }
    
    func bind() {
        
        tableView.rx.modelSelected(EventCellViewModel.self)
            .bind(to: viewModel.input.rowDidSelect)
            .disposed(by: disposeBag)
        
        tableView.rx.willBeginDragging
            .subscribe(onNext: { [weak self] _ in
                self?.isDraggingByUser = true
            })
            .disposed(by: disposeBag)
        
        tableView.rx.didEndDecelerating
            .subscribe(onNext: { [weak self] _ in
                self?.isDraggingByUser = false
            })
            .disposed(by: disposeBag)
        
        
        //RxTableViewSectionedReloadDataSource
        //RxTableViewSectionedAnimatedDataSource
        
        let dataSource = RxTableViewSectionedReloadDataSource<GGEventViewModelsSection>(configureCell: { [weak self] dataSource, tableView, indexPath, eventCellViewModel in
            guard let strongSelf = self else { return UITableViewCell() }

            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: EventTableViewCell.self), for: indexPath) as! EventTableViewCell
            
            if let sectionModel = dataSource.sectionModels.first {
                let futureEventsCount = sectionModel.futureEventsCount
                let pastEventsCount = sectionModel.pastEventsCount
                let viewModel = sectionModel.items[indexPath.row]
                
//                if cell.viewModel != nil {
//                    cell.disposeAllSubscribers()
//                }
                
                cell.viewModel = viewModel
                cell.viewModel.input.event.onNext(viewModel.event)
                
                if let location = viewModel.event?.location {
                    cell.locationButton.rx.tap
                        .map({ location })
                        .bind(to: strongSelf.viewModel.input.locationDidTap)
                        .disposed(by: strongSelf.disposeBag)
                }
                
                print("contentOffset \(tableView.contentOffset.y)")
                
                if viewModel.event == nil && strongSelf.isDraggingByUser {
                    if futureEventsCount != nil && futureEventsCount! != 0 && indexPath.row == futureEventsCount! - 1 {
                        strongSelf.viewModel.input.scrolledToNewFuture.onNext(())
                    }
                    if pastEventsCount != nil && pastEventsCount! != 0 && indexPath.row == pastEventsCount! {
                        strongSelf.viewModel.input.scrolledToNewPast.onNext(())
                    }
                }
            }

            return cell
        })

        viewModel.output.eventViewModels
            .bind(to: tableView.rx.items(dataSource: dataSource))
            .disposed(by: disposeBag)
        
        viewModel.output.futureEventsCount
            .subscribe(onNext: { [weak self] count in
                guard let strongSelf = self else { return }
                strongSelf.tableView.selectRow(at: IndexPath(row: count, section: 0), animated: strongSelf.wasDraggingByUserOnTodayLoading, scrollPosition: .top)
            })
            .disposed(by: disposeBag)
        
        Observable.zip(viewModel.output.todayEventsLoading, tableView.rx.willBeginDragging)
            .subscribe(onNext: { [weak self] (_ ,_) in
                self?.wasDraggingByUserOnTodayLoading = true
            })
            .disposed(by: disposeBag)
    }
    
    func configureNavBar() {
        self.navigationItem.title = "Parties"
        self.navigationController?.isNavigationBarHidden = false
        let visualEffectView   = UIVisualEffectView(effect: UIBlurEffect(style: .light))
        visualEffectView.frame =  (self.navigationController?.navigationBar.bounds.insetBy(dx: 0, dy: -30).offsetBy(dx: 0, dy: -30))!
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.insertSubview(visualEffectView, at: 0)
    }
    
}

//fileprivate final class EventsDataSource<S: AnimatableSectionModelType>: RxTableViewSectionedAnimatedDataSource<S> {
//    private let relay = PublishRelay<Void>()
//    var rxRealoded: Signal<Void> {
//        return relay.asSignal()
//    }
//
//    override func tableView(_ tableView: UITableView, observedEvent: Event<[S]>) {
//        super.tableView(tableView, observedEvent: observedEvent)
//        relay.accept(())
//    }
//}
