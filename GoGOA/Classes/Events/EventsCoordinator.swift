//
//  EventsCorrdinator.swift
//  GoGOA
//
//  Created by m.shilo on 25/01/2019.
//  Copyright © 2019 m.shilo. All rights reserved.
//

import UIKit
import RxSwift

enum EventsCoordinationResult {
    case event(GGEvent)
}

class EventsCoordinator: BaseCoordinator<Void> {
    
    private let webService: WebService
    private let dataService: DataService
    private let authorityService: GGAuthorityService
    private var navigationController: UINavigationController!
    private let window: UIWindow?
    private let todayFutureEventsCountOnAuth: [String : Int]
    
    init(_ webService: WebService,
         _ dataService: DataService,
         _ authorityService: GGAuthorityService,
         window: UIWindow?,
         navigationController: UINavigationController?,
         _ todayFutureEventsCountOnAuth: [String : Int]) {
        self.webService = webService
        self.dataService = dataService
        self.authorityService = authorityService
        self.window = window
        self.navigationController = navigationController
        self.todayFutureEventsCountOnAuth = todayFutureEventsCountOnAuth
    }

    
    override func start() -> Observable<Void> {
        let viewModel = EventsViewModel(webService, dataService, todayFutureEventsCountOnAuth)
        let viewController = EventsViewController.initFromXib()
        viewController.viewModel = viewModel
        
        
        viewModel.output.error
            .flatMap({ [weak self] error-> Observable<GGAlertCoordinationResult> in
                guard let strongSelf = self else { return .empty() }
                return strongSelf.showError(error, viewController)
            })
            .subscribe()
            .disposed(by: disposeBag)
        
        viewModel.output.selectedEventCellViewModel
            .map({ [weak self] eventCellViewModel -> Observable<Void> in
                guard let strongSelf = self else {
                    return .empty()
                }
                return strongSelf.showEvent(in: strongSelf.navigationController!, with: eventCellViewModel)
            })
            .subscribe()
            .disposed(by: disposeBag)
        
        viewModel.output.selectedLocation
        .map({ [weak self] location -> Observable<Void> in
            guard let strongSelf = self else {
                return .empty()
            }
            return strongSelf.showLocation(location)
        })
        .subscribe()
        .disposed(by: disposeBag)
        

        if let _ = window {
            navigationController = UINavigationController(rootViewController: viewController)
            window!.rootViewController = navigationController
            window!.makeKeyAndVisible()
        } else if let _ = navigationController {
            navigationController!.pushViewController(viewController, animated: true)
        }

        
        return Observable.never()
    }
    
    private func showEvent(in navigationController: UINavigationController, with eventCellViewModel: EventCellViewModel) -> Observable<Void> {
        let event = eventCellViewModel.event!
        let outputImage = EventViewModel.Output.Image(data: eventCellViewModel.output.iconData, loading: eventCellViewModel.output.imageLoading)
        let eventCoordinator = EventCoordinator(webService, dataService, authorityService, navigationController, event, outputImage)
        return coordinate(to: eventCoordinator)
    }
    
    private func showLocation(_ location: Location) -> Observable<Void> {
        let locationCoordinator = GGLocationCoordinator(webService, dataService, navigationController, location)
        return coordinate(to: locationCoordinator)
    }
}
