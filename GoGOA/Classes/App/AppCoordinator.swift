//
//  AppCoordinator.swift
//  GoGOA
//
//  Created by m.shilo on 16/12/2018.
//  Copyright © 2018 m.shilo. All rights reserved.
//

import UIKit
import RxSwift

class AppCoordinator: BaseCoordinator<Void> {
    
    private let window: UIWindow
    
    init(window: UIWindow) {
        self.window = window
    }
    
    override func start() -> Observable<Void> {
        let webService = WebService()
        let dataService = DataService()
//        dataService.removeAllData()
        let accountService = AccountService()
        
        if let _ = accountService.read() {
//            return self.coordinate(to: GGTabBarCoordinator(webService, dataService, self.window))
            return Observable.empty()
        } else {
            return self.coordinate(to: StartCoordinator(webService, dataService, self.window))
        }        
    }
}
