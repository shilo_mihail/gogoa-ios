//
//  PhotoIconCollectionViewCell.swift
//  GoGOA
//
//  Created by m.shilo on 16/10/2019.
//  Copyright © 2019 m.shilo. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class GGPhotoCollectionViewCell: UICollectionViewCell {
    
    typealias ViewModelType = GGPhotoCollectionCellViewModel
    
    var viewModel: ViewModelType! {
        didSet {
            viewModel.output.photoData
                .map({ data -> UIImage in
                    return UIImage(data: data)!
                })
                .bind(to: photoImageView.rx.image)
                .disposed(by: disposeBag)
            
            viewModel.output.loading
                .bind(to: loadingActivityIndicator.rx.isAnimating)
                .disposed(by: disposeBag)
        }
    }

    
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var loadingActivityIndicator: UIActivityIndicatorView!
    
    var disposeBag = DisposeBag()

    override func prepareForReuse() {
        super.prepareForReuse()
        disposeBag = DisposeBag()
    }
    
    static private var cellHeight: CGFloat = -1.0
    static func height() -> CGFloat {
        if cellHeight < 0 {
            let cell = Bundle.main.loadNibNamed(String(describing: GGPhotoCollectionViewCell.self), owner: self, options: nil)![0] as! GGPhotoCollectionViewCell
            cellHeight = cell.frame.size.height
        }
        return cellHeight
    }

}
