//
//  PhotoGalleryCellViewModel.swift
//  GoGOA
//
//  Created by m.shilo on 17/10/2019.
//  Copyright © 2019 m.shilo. All rights reserved.
//

import Foundation
import RxSwift
import Photos
import RxDataSources

struct GGPhotoCellViewModelsSection {
    var items: [GGPhotoCollectionCellViewModel]
}
extension GGPhotoCellViewModelsSection: AnimatableSectionModelType {
    typealias Item = GGPhotoCollectionCellViewModel
    var identity: Int {
        return 0
    }
    init(original: GGPhotoCellViewModelsSection, items: [GGPhotoCollectionCellViewModel]) {
        self = original
        self.items = items
    }
}


class GGPhotoCollectionCellViewModel: ViewModelProtocol {
    
    struct Input {

    }
    struct Output {
        let photoData: Observable<Data>
        let loading: Observable<Bool>
    }
    
    let input: Input
    let output: Output
    let id: Int!
    
    private var _photo: Photo!
    var photo: Photo {
        get {
            return _photo
        }
    }
    
    //INPUT

    //OUTPUT
    private let photoDataSubject = ReplaySubject<Data>.create(bufferSize: 1)
    private let loadingSubject = ReplaySubject<Bool>.create(bufferSize: 1)

    private let disposeBag = DisposeBag()

    
    init(_ id: Int) {
        self.id = id
        input = Input()
        output = Output(photoData: photoDataSubject.asObservable(),
                        loading: loadingSubject.asObservable())
    }
    
    convenience init(_ webService: WebService, _ dataService: DataService, _ photo: Photo, id: Int) {
        self.init(id)
        self._photo = photo
        
        
        if photo.photoBinary!.data.isEmpty {
            photoDataSubject.onNext(photo.smallPhotoBinary!.data)
            loadingSubject.onNext(true)
            webService.getPhotoBinary(by: photo.photoBinary!.id)
                .stopLoading(subject: loadingSubject)
                .subscribe(onNext: { [weak self] result in
                    guard let strongSelf = self else { return }

                    switch result {
                    case .success(let photoBinary):
                        dataService.savePhotoBinaryData(photoBinary.data, photoBinary.id)
                        strongSelf.photoDataSubject.onNext(photoBinary.data)
                    default:
                        break
                    }
                })
                .disposed(by: disposeBag)
        } else {
            photoDataSubject.onNext(photo.photoBinary!.data)
        }
        
    }
    
    convenience init(_ photoAssetService: PhotoAssetService, _ asset: PHAsset, id: Int) {
        self.init(id)
        
        loadingSubject.onNext(true)
        photoAssetService.fetchImageData(asset)
            .filter { $0 != nil }
            .map { $0! }
            .stopLoading(subject: loadingSubject)
            .bind(to: photoDataSubject)
            .disposed(by: disposeBag)
    }
    
    deinit {
        photoDataSubject.on(.completed)
        loadingSubject.on(.completed)
    }
}

extension GGPhotoCollectionCellViewModel: IdentifiableType {
    var identity: Int {
        return id
    }
}

extension GGPhotoCollectionCellViewModel: Equatable {
    static func == (lhs: GGPhotoCollectionCellViewModel, rhs: GGPhotoCollectionCellViewModel) -> Bool {
        return lhs.id == rhs.id
    }
}
