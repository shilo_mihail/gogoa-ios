//
//  StartViewController.swift
//  GoGOA
//
//  Created by m.shilo on 09/04/2019.
//  Copyright © 2019 m.shilo. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class StartViewController: ParallaxViewController, ControllerType, XibInitializable {
    
    typealias ViewModelType = StartViewModel
    
    var viewModel: ViewModelType!
    
    @IBOutlet weak var logoLabel: UILabel! {
        didSet {
            logoLabel.textColor = .white
            logoLabel.font = UIFont.boldSystemFont(ofSize: 45)
            logoLabel.text = "GoGOA"
        }
    }
    @IBOutlet weak var sloganLabel: UILabel! {
        didSet {
            sloganLabel.textColor = .white
            sloganLabel.font = UIFont.systemFont(ofSize: 35)
            sloganLabel.text = "The best way to discover GOA"
        }
    }
    @IBOutlet weak var letsGoButton: GGRoundedButtonView! {
        didSet {
            letsGoButton.label.text = "LET'S GO"
            letsGoButton.startColor = Constants.color.button.loginGroup.leftGragient
            letsGoButton.finishColor = Constants.color.button.loginGroup.rightGradient
        }
    }
    @IBOutlet weak var loginButton: GGRoundedButtonView! {
        didSet {
            loginButton.label.text = "Log in"
            loginButton.startColor = Constants.color.button.loginGroup.leftGragient
            loginButton.finishColor = Constants.color.button.loginGroup.rightGradient
        }
    }
    @IBOutlet weak var registrationButton: UIButton! {
        didSet {
            registrationButton.setTitle("Sign up", for: .normal)
            registrationButton.setTitleColor(Constants.color.button.loginGroup.title, for: .normal)
        }
    }

    
    var disposeBag = DisposeBag()
    
    
    override func viewDidLoad(){
        super.viewDidLoad()
        
        currentLine = .first
        navigationItem.title = ""
        navigationController?.isNavigationBarHidden = true
        
        hideKeyboardWhenTappedAround()
        bind()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print("resources: \(RxSwift.Resources.total)")
    }
    
    
    func bind() {
        letsGoButton.tap.rx.event
            .map { _ -> Void in }
            .bind(to: viewModel.input.letsGoBtnDidTap)
            .disposed(by: disposeBag)
        
        loginButton.tap.rx.event
            .map { _ -> Void in }
            .bind(to: viewModel.input.loginBtnDidTap)
            .disposed(by: disposeBag)
        
        registrationButton.rx.tap.asObservable()
            .bind(to: viewModel.input.registrationBtnDidTap)
            .disposed(by: disposeBag)
        
        viewModel.output.loading
            .bind(to: letsGoButton.rx.loading)
            .disposed(by: disposeBag)
    }
    
}
