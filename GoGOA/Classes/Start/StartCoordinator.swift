//
//  StartCoordinator.swift
//  GoGOA
//
//  Created by m.shilo on 09/04/2019.
//  Copyright © 2019 m.shilo. All rights reserved.
//

import Foundation
import RxSwift

class StartCoordinator: BaseCoordinator<Void> {
    
    private let webService: WebService
    private let dataService: DataService
    private let window: UIWindow
    
    init(_ webService: WebService, _ dataService: DataService, _ window: UIWindow) {
        self.webService = webService
        self.dataService = dataService
        self.window = window
    }
    
    override func start() -> Observable<CoordinationResult> {
        let viewController = StartViewController.initFromXib()
        let navigationController = UINavigationController(rootViewController: viewController)
        
        let viewModel = StartViewModel(webService, dataService)
        
        viewController.viewModel = viewModel
        
        let showTabBar = viewModel.output.showTabBar
            .flatMap { [weak self] todayFutureEventsCountOnAuth -> Observable<Void> in
                guard let strongSelf = self else {
                    return .empty()
                }
                return strongSelf.showTabBar(todayFutureEventsCountOnAuth)
            }
        
        let showLogin = viewModel.output.showLogin
            .flatMap { [weak self] _ -> Observable<Void> in
                guard let strongSelf = self else {
                    return .empty()
                }
                return strongSelf.showLogin(in: navigationController)
            }
        
        let showRegistration = viewModel.output.showRegistration
            .flatMap { [weak self] _ -> Observable<Void> in
                guard let strongSelf = self else {
                    return .empty()
                }
                return strongSelf.showRegistration(in: navigationController)
            }
        
        viewModel.output.error
            .flatMap({ [weak self] error-> Observable<GGAlertCoordinationResult> in
                guard let strongSelf = self else { return .empty() }
                return strongSelf.showError(error, viewController)
            })
            .subscribe()
            .disposed(by: disposeBag)
        
        
        window.rootViewController = navigationController
        window.makeKeyAndVisible()
        
        
        return Observable
            .merge(showTabBar, showLogin, showRegistration)
        
    }
    
    private func showTabBar(_ todayFutureEventsCountOnAuth: [String : Int]) -> Observable<Void> {
        let tabBarCoordinator = GGTabBarCoordinator(webService, dataService, window, todayFutureEventsCountOnAuth)
        return coordinate(to: tabBarCoordinator)
    }
    
    private func showLogin(in navigationController: UINavigationController) -> Observable<Void> {
        let loginCoordinator = LoginCoordinator(webService, dataService, window, navigationController)
        return coordinate(to: loginCoordinator)
    }
    
    private func showRegistration(in navigationController: UINavigationController) -> Observable<Void> {
        let registrationCoordinator = RegistrationCoordinator(webService, dataService, window, navigationController)
        return coordinate(to: registrationCoordinator)
    }
}
