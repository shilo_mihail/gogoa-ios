//
//  StartViewModel.swift
//  GoGOA
//
//  Created by m.shilo on 09/04/2019.
//  Copyright © 2019 m.shilo. All rights reserved.
//

import RxSwift
import Foundation

class StartViewModel: ViewModelProtocol {
    
    struct Input {
        let letsGoBtnDidTap: AnyObserver<Void>
        let loginBtnDidTap: AnyObserver<Void>
        let registrationBtnDidTap: AnyObserver<Void>
    }
    struct Output {
        let showTabBar: Observable<[String : Int]>
        let loading: Observable<Bool>
        let showLogin: Observable<Void>
        let showRegistration: Observable<Void>
        let error: Observable<AppError>
    }
    
    let input: Input
    let output: Output
    
    private let letsGoBtnDidTapSubject = PublishSubject<Void>()
    private let loginBtnDidTapSubject = PublishSubject<Void>()
    private let registrationBtnDidTapSubject = PublishSubject<Void>()
    
    private let showTabBarSubject = PublishSubject<[String : Int]>()
    private let loadingSubject = PublishSubject<Bool>()
    private let showLoginSubject = PublishSubject<Void>()
    private let showRegistrationSubject = PublishSubject<Void>()
    private let errorSubject = PublishSubject<AppError>()
    
    private let disposeBag = DisposeBag()
    
    
    init(_ webService: WebService, _ dataService: DataService) {
        
        input = Input(letsGoBtnDidTap: letsGoBtnDidTapSubject.asObserver(),
                      loginBtnDidTap: loginBtnDidTapSubject.asObserver(),
                      registrationBtnDidTap: registrationBtnDidTapSubject.asObserver())
        
        output = Output(showTabBar: showTabBarSubject.asObservable(),
                        loading: loadingSubject.asObservable(),
                        showLogin: showLoginSubject.asObservable(),
                        showRegistration: showRegistrationSubject.asObservable(),
                        error: errorSubject.asObservable())
        
        letsGoBtnDidTapSubject
            .flatMap {  [weak self] () -> Observable<GGResult<GGAuthResponse>> in
                self?.loadingSubject.onNext(true)
                
                if let previousUser = dataService.fetchCurrentUser() {
                    return webService.authenticate(previousUser.login, previousUser.password)
                } else {
                    let user = GGUser()
                    user.role = GGRole.TEMP_USER.rawValue
                    user.login = UUID().uuidString
                    
                    return webService.registration(user)
                }
            }
            .subscribe(onNext: { [weak self] result in
                self?.loadingSubject.onNext(false)
                switch result {
                case .success(let authResponse):
                    dataService.saveCurrentUser(authResponse.user)
                    self?.showTabBarSubject.onNext(authResponse.todayFutureEventsCount)
                case .failure(let error):
                    self?.errorSubject.onNext(error)
                }
            })
            .disposed(by: disposeBag)
        
        loginBtnDidTapSubject
            .bind(to: self.showLoginSubject)
            .disposed(by: disposeBag)
        
        registrationBtnDidTapSubject
            .bind(to: self.showRegistrationSubject)
            .disposed(by: disposeBag)
    }
    
    deinit {
        letsGoBtnDidTapSubject.on(.completed)
        loginBtnDidTapSubject.on(.completed)
        registrationBtnDidTapSubject.on(.completed)
        
        showTabBarSubject.on(.completed)
        loadingSubject.on(.completed)
        showLoginSubject.on(.completed)
        showRegistrationSubject.on(.completed)
        errorSubject.on(.completed)
    }
    
    
}

