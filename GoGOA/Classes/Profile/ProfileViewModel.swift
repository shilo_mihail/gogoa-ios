//
//  EventViewModel.swift
//  GoGOA
//
//  Created by m.shilo on 29/01/2019.
//  Copyright © 2019 m.shilo. All rights reserved.
//

import RxSwift
import Foundation
import Photos

class ProfileViewModel: ViewModelProtocol {
    
    struct Input {
        let doneBtnDidTap: AnyObserver<Void>
        let removeUserPhotoBtnDidTap: AnyObserver<Void>
        let viewGalleryPhotoDidTap: AnyObserver<Void>
        let galleryPhotoSent: AnyObserver<Data>
        let backBtnDidTap: AnyObserver<Void>
    }
    struct Output {
        let user: Observable<GGUser?>
        let error: Observable<AppError>
        let loading: Observable<Bool>
        let logout: Observable<Void>
        let isDoneBtnActive: Observable<Bool>
        let showImagePickerActionSheet: Observable<Void>
        let showPhotoLibrary: Observable<Void>
        let back: Observable<Void>
    }
    
    let input: Input
    let output: Output
    
//    struct ImageDataAndPhotoId {
//        let imageData: Data
//        let photoId: Int64
//    }
    
    
    //INPUT
    private let doneBtnDidTapSubject = PublishSubject<Void>()
    private let removeUserPhotoBtnDidTapSubject = PublishSubject<Void>()
    private let viewGalleryPhotoDidTapSubject = PublishSubject<Void>()
    private let galleryPhotoSentSubject = PublishSubject<Data>()
    private let backBtnDidTapSubject = PublishSubject<Void>()
    //OUTPUT
    private let userSubject: BehaviorSubject<GGUser?>
    private let errorSubject = PublishSubject<AppError>()
    private let loadingSubject = PublishSubject<Bool>()
    private let logoutSubject = PublishSubject<Void>()
    private let isDoneBtnActiveSubject = PublishSubject<Bool>()
    private let showImagePickerActionSheetSubject = PublishSubject<Void>()
    private let showPhotoLibrarySubject = PublishSubject<Void>()
    private let backSubject = PublishSubject<Void>()
    
//    private var imageDataAndPhotoIdObservable: Observable<ImageDataAndPhotoId> {
//        return Observable.combineLatest(updatedImageDataSubject.asObservable(), userInfoViewModel.output.updatedUser) { (imageData, user) in
//            return ImageDataAndUser(imageData: imageData, user: user)
//        }
//    }
    
    private let disposeBag = DisposeBag()
    
    var userInfoViewModel: UserInfoViewModel!
    
    
    init(_ webService: WebService, _ dataService: DataService, user: GGUser?) {
        userSubject = BehaviorSubject<GGUser?>(value: user)
        input = Input(doneBtnDidTap: doneBtnDidTapSubject.asObserver(),
                      removeUserPhotoBtnDidTap: removeUserPhotoBtnDidTapSubject.asObserver(),
                      viewGalleryPhotoDidTap: viewGalleryPhotoDidTapSubject.asObserver(),
                      galleryPhotoSent: galleryPhotoSentSubject.asObserver(),
                      backBtnDidTap: backBtnDidTapSubject.asObserver())
        
        output = Output(user: userSubject.asObservable(),
                        error: errorSubject.asObservable(),
                        loading: loadingSubject.asObservable(),
                        logout: logoutSubject.asObservable(),
                        isDoneBtnActive: isDoneBtnActiveSubject.asObservable(),
                        showImagePickerActionSheet: showImagePickerActionSheetSubject.asObservable(),
                        showPhotoLibrary: showPhotoLibrarySubject.asObservable(),
                        back: backSubject.asObservable())
        
        let currentUser = dataService.fetchCurrentUser()
        let isCurrentUserProfile = currentUser == nil ? false : currentUser!.id == user!.id
        
        userInfoViewModel = UserInfoViewModel(webService, dataService, user: user, isCurrentUserProfile: isCurrentUserProfile)
        
        userInfoViewModel.output.error
            .bind(to: errorSubject)
            .disposed(by: disposeBag)
        
        userInfoViewModel.output.logout
            .bind(to: logoutSubject)
            .disposed(by: disposeBag)
        
        userInfoViewModel.output.isDoneBtnActive
            .bind(to: isDoneBtnActiveSubject)
            .disposed(by: disposeBag)
        
        userInfoViewModel.output.showImagePickerActionSheet
            .bind(to: showImagePickerActionSheetSubject)
            .disposed(by: disposeBag)
        
        removeUserPhotoBtnDidTapSubject
            .flatMap({ _ -> Observable<GGResult<Void>> in
                return webService.removeUserPhoto()
            })
            .map({ result -> Void in
                dataService.removeCurrentUserPhoto()
            })
            .bind(to: userInfoViewModel.input.updateUserIconSuccess)
            .disposed(by: disposeBag)
        
        viewGalleryPhotoDidTapSubject
            .bind(to: showPhotoLibrarySubject)
            .disposed(by: disposeBag)
        
        doneBtnDidTapSubject
            .withLatestFrom(userInfoViewModel.output.updatedUser)
            .flatMapLatest { user -> Observable<GGResult<GGUser>> in
                return webService.updateUserInfo(with: user)
            }
            .subscribe(onNext: { [weak self] result in
                print(result)
            })
            .disposed(by: disposeBag)

        
        galleryPhotoSentSubject
            .flatMap { [weak self] data -> Observable<GGResult<Photo>> in
                self?.userInfoViewModel.input.iconLoading.onNext(true)
                return webService.updateUserPhoto(with: data)
            }
            .filter({ [weak self] result -> Bool in
                self?.userInfoViewModel.input.iconLoading.onNext(false)
                switch result {
                case .failure(let error):
                    self?.errorSubject.onNext(error)
                    return false
                case .success(let photo):
                    dataService.saveCurrentUserPhoto(photo)
                    return true
                }
            })
            .withLatestFrom(galleryPhotoSentSubject)
            .subscribe(onNext: { [weak self] data in
                dataService.saveCurrentUserPhotoData(data)
                self?.userInfoViewModel.input.updateUserIconSuccess.onNext(())
            })
            .disposed(by: disposeBag)
    }
    
    deinit {
        doneBtnDidTapSubject.on(.completed)
        removeUserPhotoBtnDidTapSubject.on(.completed)
        viewGalleryPhotoDidTapSubject.on(.completed)
        backBtnDidTapSubject.on(.completed)
        
        userSubject.on(.completed)
        errorSubject.on(.completed)
        loadingSubject.on(.completed)
        logoutSubject.on(.completed)
        isDoneBtnActiveSubject.on(.completed)
        showImagePickerActionSheetSubject.on(.completed)
        showPhotoLibrarySubject.on(.completed)
        galleryPhotoSentSubject.on(.completed)
        backSubject.on(.completed)
    }
    
}

