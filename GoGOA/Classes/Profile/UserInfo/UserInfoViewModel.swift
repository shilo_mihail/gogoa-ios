//
//  UserInfoViewModel.swift
//  GoGOA
//
//  Created by m.shilo on 27/08/2019.
//  Copyright © 2019 m.shilo. All rights reserved.
//

import Foundation
import RxSwift

class UserInfoViewModel: ViewModelProtocol {
    
    struct Input {
        let name: AnyObserver<String?>
        let age: AnyObserver<String?>
        let editPhotoBtnDidTap: AnyObserver<Void>
        let logoutBtnDidTap: AnyObserver<Void>
        let updateUserIconSuccess: AnyObserver<Void>
        let iconLoading: AnyObserver<Bool>
    }
    struct Output {
        let error: Observable<AppError>
        let logoutLoading: Observable<Bool>
        let logout: Observable<Void>
        let isDoneBtnActive: Observable<Bool>
        let showImagePickerActionSheet: Observable<Void>
        let updatedUser: Observable<GGUser>
        let updateUserIcon: Observable<Void>
        let iconLoading: Observable<Bool>
    }
    
    let input: Input
    let output: Output
    let user: GGUser?
    let isCurrentUserProfile: Bool
    
    //INPUT
    private let nameSubject = PublishSubject<String?>()
    private let ageSubject = PublishSubject<String?>()
    private let editPhotoBtnDidTapSubject = PublishSubject<Void>()
    private let logoutBtnDidTapSubject = PublishSubject<Void>()
    private let updateUserIconSuccessSubject = PublishSubject<Void>()
    private let iconLoadingInSubject = PublishSubject<Bool>()
    //OUTPUT
    private let errorSubject = PublishSubject<AppError>()
    private let logoutLoadingSubject = PublishSubject<Bool>()
    private let logoutSubject = PublishSubject<Void>()
    private let isDoneBtnActiveSubject = PublishSubject<Bool>()
    private let showImagePickerActionSheetSubject = PublishSubject<Void>()
    private let updatedUserSubject = PublishSubject<GGUser>()
    private let updateUserIconSubject: BehaviorSubject<Void>
    private let iconLoadingOutSubject = PublishSubject<Bool>()
    
    
    private let disposeBag = DisposeBag()
    
    
    init(_ webService: WebService, _ dataService: DataService, user: GGUser?, isCurrentUserProfile: Bool) {
        updateUserIconSubject = BehaviorSubject<Void>(value: ())
        self.user = user
        self.isCurrentUserProfile = isCurrentUserProfile
        input = Input(name: nameSubject.asObserver(),
                      age: ageSubject.asObserver(),
                      editPhotoBtnDidTap: editPhotoBtnDidTapSubject.asObserver(),
                      logoutBtnDidTap: logoutBtnDidTapSubject.asObserver(),
                      updateUserIconSuccess: updateUserIconSuccessSubject.asObserver(),
                      iconLoading: iconLoadingInSubject.asObserver())
        
        output = Output(error: errorSubject.asObservable(),
                        logoutLoading: logoutLoadingSubject.asObservable(),
                        logout: logoutSubject.asObservable(),
                        isDoneBtnActive: isDoneBtnActiveSubject.asObservable(),
                        showImagePickerActionSheet: showImagePickerActionSheetSubject.asObservable(),
                        updatedUser: updatedUserSubject.asObservable(),
                        updateUserIcon: updateUserIconSubject.asObservable(),
                        iconLoading: iconLoadingOutSubject.asObservable())
        
        Observable.combineLatest(nameSubject, ageSubject)
            .map({ [weak self] (name, age) -> Bool in
                guard let _ = user else {
                    return false
                }
                let updatedUser = GGUser()
                updatedUser.name = name!
                if let cleanedAge = age!.components(separatedBy: CharacterSet.decimalDigits.inverted).first {
                    updatedUser.age = Int(cleanedAge)!
                    return name != user!.name || cleanedAge != String(user!.age)
                }
                self?.updatedUserSubject.onNext(updatedUser)
                return true
            })
            .bind(to: isDoneBtnActiveSubject)
            .disposed(by: disposeBag)


        logoutBtnDidTapSubject
            .flatMap { [weak self] _ -> Observable<GGResult<Void>> in
                self?.logoutLoadingSubject.onNext(true)
                return webService.logout()
            }
            .subscribe(onNext: { [weak self] result in
                guard let strongSelf = self else {
                    return
                }
                switch result {
                case .success():
                    strongSelf.logoutSubject.onNext(())
                case .failure(let error):
                    strongSelf.errorSubject.onNext(error)
                }
                strongSelf.logoutLoadingSubject.onNext(false)
            })
            .disposed(by: disposeBag)
        
        editPhotoBtnDidTapSubject
            .bind(to: showImagePickerActionSheetSubject)
            .disposed(by: disposeBag)
        
        updateUserIconSuccessSubject
            .bind(to: updateUserIconSubject)
            .disposed(by: disposeBag)
        
        iconLoadingInSubject
            .bind(to: iconLoadingOutSubject)
            .disposed(by: disposeBag)
    }

    
    deinit {
        nameSubject.on(.completed)
        ageSubject.on(.completed)
        editPhotoBtnDidTapSubject.on(.completed)
        logoutBtnDidTapSubject.on(.completed)
        
        errorSubject.on(.completed)
        logoutLoadingSubject.on(.completed)
    }
}
