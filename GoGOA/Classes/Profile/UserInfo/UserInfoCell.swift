//
//  UserInfoCell.swift
//  GoGOA
//
//  Created by m.shilo on 27/08/2019.
//  Copyright © 2019 m.shilo. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class UserInfoCell: UITableViewCell {
    
    typealias ViewModelType = UserInfoViewModel
    
    var viewModel: ViewModelType!
    
    var disposeBag = DisposeBag()

    @IBOutlet weak var iconContainerView: ClickableView! {
        didSet {
            iconContainerView.layer.cornerRadius = iconContainerView.bounds.height / 2
            iconContainerView.clipsToBounds = true
        }
    }
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var iconActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var iconPhotoCamImageView: UIImageView! {
        didSet {
            iconPhotoCamImageView.image = Constants.icon.photo_cam.withRenderingMode(.alwaysTemplate)
            iconPhotoCamImageView.tintColor = .white
        }
    }
    @IBOutlet weak var iconPhotoCamBackgroundView: UIView!
    
    @IBOutlet weak var phoneTextField: GGLineTextField! {
        didSet {
            phoneTextField.isEnabled = false
            phoneTextField.setColor(.login)
        }
    }
    @IBOutlet weak var nameTextField: GGLineTextField! {
        didSet {
            nameTextField.setColor(.login)
        }
    }
    @IBOutlet weak var ageTextField: GGLineTextField! {
        didSet {
            ageTextField.setColor(.login)
        }
    }
    @IBOutlet weak var logoutButton: GGRoundedButtonView! {
        didSet {
            logoutButton.label.text = "LOG OUT"
            logoutButton.startColor = Constants.color.button.profileGroup.leftGragient
            logoutButton.finishColor = Constants.color.button.profileGroup.rightGradient
        }
    }
    
    func configure() {
        if !viewModel.isCurrentUserProfile {
            nameTextField.isEnabled = false
            ageTextField.isEnabled = false
            logoutButton.isHidden = true
        }
            
        phoneTextField.text = viewModel.user!.login
        nameTextField.text = viewModel.user!.name
        ageTextField.text = "\(String(viewModel.user!.age)) \(Utils.getYearsCountPluralForm(viewModel.user!.age))"
        
        nameTextField.rx.text.asObservable()
            .subscribe(viewModel.input.name)
            .disposed(by: disposeBag)
        
        ageTextField.rx.text.asObservable()
            .subscribe(viewModel.input.age)
            .disposed(by: disposeBag)
        
        ageTextField.rx.controlEvent([.editingDidBegin, .editingDidEnd])
            .asObservable()
            .subscribe(onNext: { [weak self] _ in
                guard let strongSelf = self else {
                    return
                }
                if strongSelf.ageTextField.isEditing {
                    if let text = strongSelf.ageTextField.text!.components(separatedBy: CharacterSet.decimalDigits.inverted).first {
                        strongSelf.ageTextField.text = text
                    }
                } else {
                    strongSelf.ageTextField.text = strongSelf.ageTextField.text! + " " + Utils.getYearsCountPluralForm(Int(strongSelf.ageTextField.text!)!)
                }
            })
            .disposed(by: disposeBag)
        
        viewModel.output.updateUserIcon
            .subscribe(onNext: { [weak self] _ in
                self?.updatePhotoIcon()
            })
            .disposed(by: disposeBag)
        
        viewModel.output.logoutLoading
            .bind(to: logoutButton.rx.loading)
            .disposed(by: disposeBag)
        
        logoutButton.tap.rx.event
            .map { _ -> Void in }
            .bind(to: viewModel.input.logoutBtnDidTap)
            .disposed(by: disposeBag)
        
        iconContainerView.tap.rx.event
            .map { _ -> Void in }
            .bind(to: viewModel.input.editPhotoBtnDidTap)
            .disposed(by: disposeBag)
        
        viewModel.output.iconLoading
            .subscribe(onNext: { [weak self] loading in
                if loading {
                    self?.iconActivityIndicator.startAnimating()
                    self?.iconPhotoCamImageView.isHidden = true
                } else {
                    self?.iconActivityIndicator.stopAnimating()
                    self?.iconPhotoCamImageView.isHidden = false
                }
            })
            .disposed(by: disposeBag)
    }
    
    private func updatePhotoIcon() {
        if let imageData = viewModel.user!.photoIcon?.smallPhotoBinary?.data {
            iconImageView.image = UIImage(data: imageData)
        } else {
            iconImageView.image = UIImage(UIColor(hex: viewModel.user!.iconHexColor))
            
            //Не показываем инициалы в профиле - они все равно закрыты значком фотоаппарата
            
//            let initials = Utils.parseFirstCharacters(viewModel.user.name)
//            let initialsLabel = UILabel(frame: CGRect(x: 0, y: 0, width: iconContainerView.bounds.height, height: iconContainerView.bounds.height))
//            initialsLabel.textColor = .white
//            initialsLabel.text = initials
//            initialsLabel.textAlignment = .center
//            initialsLabel.font = UIFont.systemFont(ofSize: 25)
//
//            iconContainerView.addSubview(initialsLabel)
        }
    }
    
}
