//
//  EventViewController.swift
//  GoGOA
//
//  Created by m.shilo on 29/01/2019.
//  Copyright © 2019 m.shilo. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Photos

class ProfileViewController: ParallaxViewController, ControllerType, XibInitializable {
    
    
    typealias ViewModelType = ProfileViewModel
    
    var viewModel: ViewModelType!
    
    var disposeBag = DisposeBag()
    
    private var imagePickerObservable: Observable<UIImagePickerController>?
    
    
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.register(UINib(nibName: String(describing: UserInfoCell.self), bundle: nil), forCellReuseIdentifier: String(describing: UserInfoCell.self))
        }
    }
    var doneButton: UIButton! {
        didSet {
            doneButton.titleLabel?.font = UIFont.systemFont(ofSize: 17)
            doneButton.isEnabled = false
            doneButton.alpha = 0.5
            doneButton.setTitle("Done!", for: .normal)
            doneButton.setTitleColor(self.navigationController?.navigationBar.tintColor, for: .normal)
            self.navigationController?.navigationBar.addSubview(doneButton)
            
            doneButton.translatesAutoresizingMaskIntoConstraints = false
            if let navigationController = self.navigationController {
                doneButton.bottomAnchor.constraint(equalTo: navigationController.navigationBar.bottomAnchor).isActive = true
                doneButton.rightAnchor.constraint(equalTo: navigationController.navigationBar.rightAnchor).isActive = true
                doneButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
                doneButton.widthAnchor.constraint(equalToConstant: 70).isActive = true
            }
        }
    }
    

    
    override func viewDidLoad(){
        super.viewDidLoad()
        
        hideKeyboardWhenTappedAround()
        configureNavBar()
        bind()
        
        currentLine = .second
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print("resources: \(RxSwift.Resources.total)")
        
        let photoAuthorizationStatus = PHPhotoLibrary.authorizationStatus()
        switch photoAuthorizationStatus {
            case .authorized: print("Access is granted by user")
            case .notDetermined: PHPhotoLibrary.requestAuthorization({
                (newStatus) in print("status is \(newStatus)")
                if newStatus == PHAuthorizationStatus.authorized {
                    print("success")
                }
            })
            case .restricted: print("User do not have access to photo album.")
            case .denied: print("User has denied the permission.")
        }
    }
    
    func bind() {
        
        doneButton.rx.tap
            .bind(to: viewModel.input.doneBtnDidTap)
            .disposed(by: disposeBag)
        
        viewModel.output.user
            .map({ user -> [GGUser?] in
                return [user]
            })
            .bind(to: tableView.rx.items(cellIdentifier: String(describing: UserInfoCell.self), cellType: UserInfoCell.self)) { [weak self] (index, user, cell) in
                guard let strongSelf = self else {
                    return
                }
                cell.viewModel = strongSelf.viewModel.userInfoViewModel
                cell.configure()
            }
            .disposed(by: disposeBag)
        
        viewModel.output.isDoneBtnActive
            .subscribe(onNext: { [weak self] isActive in
                isActive ? (self?.doneButton.alpha = 1) : (self?.doneButton.alpha = 0.5)
                self?.doneButton.rx.isEnabled.onNext(isActive)
            })
            .disposed(by: disposeBag)
    }
    
    func configureNavBar() {
        self.navigationItem.title = "Profile"
        self.navigationController?.isNavigationBarHidden = false
        let visualEffectView   = UIVisualEffectView(effect: UIBlurEffect(style: .light))
        visualEffectView.frame =  (self.navigationController?.navigationBar.bounds.insetBy(dx: 0, dy: -30).offsetBy(dx: 0, dy: -30))!
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.insertSubview(visualEffectView, at: 0)
        
        doneButton = UIButton(type: .system)
    }
}
