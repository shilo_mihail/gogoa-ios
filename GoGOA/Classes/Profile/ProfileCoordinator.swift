//
//  ProfileCoordinator.swift
//  GoGOA
//
//  Created by m.shilo on 29/01/2019.
//  Copyright © 2019 m.shilo. All rights reserved.
//

import UIKit
import RxSwift
import Photos

enum ProfileCoordinationResult {
    case back
}

class ProfileCoordinator: BaseCoordinator<Void> {
    
    private let webService: WebService
    private let dataService: DataService
    private let navigationController: UINavigationController
    private let user: GGUser?
    
    init(_ webService: WebService,
         _ dataService: DataService,
         _ navigationController: UINavigationController,
         _ user: GGUser?) {
        self.webService = webService
        self.dataService = dataService
        self.navigationController = navigationController
        self.user = user
    }
    
    override func start() -> Observable<Void> {
        let viewController = ProfileViewController.initFromXib()
        
        let photoAssetService = PhotoAssetService()
        let viewModel = ProfileViewModel(webService, dataService, user: user)
        viewController.viewModel = viewModel
        
        viewModel.output.error
            .flatMap({ [weak self] error -> Observable<GGAlertCoordinationResult> in
                guard let strongSelf = self else { return .empty() }
                return strongSelf.showError(error, viewController)
            })
            .subscribe()
            .disposed(by: disposeBag)
        
        viewModel.output.logout
            .map({ [weak self] users -> Observable<Void> in
                guard let strongSelf = self else {
                    return .empty()
                }
                return strongSelf.showStart(strongSelf.webService, strongSelf.dataService)
            })
            .map({ [weak self] observable -> Void in
                guard let strongSelf = self else {
                    return
                }
                observable
                    .subscribe()
                    .disposed(by: strongSelf.disposeBag)
            })
            .subscribe()
            .disposed(by: disposeBag)
        
//        viewModel.output.showPhotoLibrary
//            .flatMapLatest { _ in
//                return UIImagePickerController.rx.createWithParent(viewController) { picker in
//                    picker.sourceType = .photoLibrary
//                    picker.allowsEditing = false
//                    }
//                    .flatMap {
//                        $0.rx.didFinishPickingMediaWithInfo
//                    }
//                    .take(1)
//            }
//            .map {
//                $0[UIImagePickerController.InfoKey.phAsset.rawValue] as? PHAsset
//
//            }
//            .filter { $0 != nil }
//            .flatMap({ [weak self] photoAsset -> Observable<GGPhotoCoordinatorResult> in
//                guard let strongSelf = self else { return .empty() }
//                return strongSelf.showAsset(on: viewController, photoAssetService, photoAsset!)
//            })
//            .subscribe()
//            .disposed(by: disposeBag)
        
        viewModel.output.showImagePickerActionSheet
            .flatMap({ [weak self] _ -> Observable<GGActionSheetCoordinatorResult> in
                guard let strongSelf = self else { return .empty() }
                return strongSelf.showActionSheet(on: viewController, photoAssetService)
            })
            .filter({ result -> Bool in
                switch result {
                case .close:
                    return false
                default:
                    return true
                }
            })
            .flatMapFirst({ [weak self] result  -> Observable<GGPhotoCoordinatorResult> in
                guard let strongSelf = self else { return .empty() }
                switch result {
                case .removePhoto:
                    viewModel.input.removeUserPhotoBtnDidTap.onNext(())
                    return .empty()
                case .viewPhoto:
                    return strongSelf.showViewPhoto(on: viewController)
                case .viewGalleryPhoto:
                    viewModel.input.viewGalleryPhotoDidTap.onNext(())
                    return .empty()
                case .viewQuickPhoto(let photoAsset):
                    return strongSelf.showViewAndSendPhoto(on: viewController, photoAssetService, photoAsset)
                case .close:
                    return .empty()
                }
            })
            .filter({ result -> Bool in
                switch result {
                case .close:
                    return false
                default:
                    return true
                }
            })
            .map { result -> Data in
                switch result {
                case .send(let photoData):
                    return photoData
                default:
                    return Data()
                }
            }
            .bind(to: viewModel.input.galleryPhotoSent)
            .disposed(by: disposeBag)
        
        viewModel.output.showPhotoLibrary
            .flatMapFirst { [weak self] _ -> Observable<GGImagePickerCoordinationResult> in
                guard let strongSelf = self else { return .empty() }
                return strongSelf.showImagePicker(on: viewController)
            }
            .filter({ result -> Bool in
                switch result {
                case .cancel:
                    return false
                default:
                    return true
                }
            })
            .flatMap { [weak self] result -> Observable<GGPhotoCoordinatorResult> in
                guard let strongSelf = self else { return .empty() }
                switch result {
                case .selected(let photoData):
                    return strongSelf.showViewAndSendPhoto(on: viewController, photoData)
                default:
                    return .empty()
                }
            }
            .filter({ result -> Bool in
                switch result {
                case .close:
                    return false
                default:
                    return true
                }
            })
            .map { result -> Data in
                switch result {
                case .send(let photoData):
                    return photoData
                default:
                    return Data()
                }
            }
            .bind(to: viewModel.input.galleryPhotoSent)
            .disposed(by: disposeBag)

        navigationController.pushViewController(viewController, animated: true)
        
        return viewModel.output.back
            .take(1)
    }
    
    private func showActionSheet(on parentViewController: UIViewController, _ photoAssetService: PhotoAssetService) -> Observable<GGActionSheetCoordinatorResult> {
        let actionSheetCoordinator = GGActionSheetCoordinator(parentViewController, photoAssetService, user!.photoIcon?.smallPhotoBinary?.data != nil)
        return coordinate(to: actionSheetCoordinator)
    }
    
    private func showViewPhoto(on parentViewController: UIViewController) -> Observable<GGPhotoCoordinatorResult> {
        let photoCoordinator = GGViewPhotoCoordinator(parentViewController, webService, dataService, user!.photoIcon!)
        return coordinate(to: photoCoordinator)
    }
    
    private func showViewAndSendPhoto(on parentViewController: UIViewController, _ photoAssetService: PhotoAssetService, _ photoAsset: PHAsset) -> Observable<GGPhotoCoordinatorResult> {
        let photoCoordinator = GGViewAndSendPhotoCoordinator(parentViewController, photoAssetService, photoAsset, nil)
        return coordinate(to: photoCoordinator)
    }
    
    private func showViewAndSendPhoto(on parentViewController: UIViewController, _ photoData: Data) -> Observable<GGPhotoCoordinatorResult> {
        let photoCoordinator = GGViewAndSendPhotoCoordinator(parentViewController, nil, nil, photoData)
        return coordinate(to: photoCoordinator)
    }
    
    private func showImagePicker(on parentViewController: UIViewController) -> Observable<GGImagePickerCoordinationResult> {
        let imagePickerCoordinator = GGImagePickerCoordinator(parentViewController)
        return coordinate(to: imagePickerCoordinator)
    }

}
