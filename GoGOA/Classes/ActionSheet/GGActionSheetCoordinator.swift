//
//  GGActionSheetCoordinator.swift
//  GoGOA
//
//  Created by m.shilo on 13/09/2019.
//  Copyright © 2019 m.shilo. All rights reserved.
//

import UIKit
import RxSwift
import Photos

enum GGActionSheetCoordinatorResult {
    case close
    case removePhoto
    case viewPhoto
    case viewGalleryPhoto
    case viewQuickPhoto(PHAsset)
}

class GGActionSheetCoordinator: BaseCoordinator<GGActionSheetCoordinatorResult> {
    
    private lazy var actionSheetPresentationManager = ActionSheetPresentationManager()
    private let parentViewController: UIViewController
    private let photoAssetService: PhotoAssetService
    private let isUserPhotoExist: Bool
    
    init(_ parentViewController: UIViewController, _ photoAssetService: PhotoAssetService, _ isUserPhotoExist: Bool) {
        self.parentViewController = parentViewController
        self.photoAssetService = photoAssetService
        self.isUserPhotoExist = isUserPhotoExist
    }
    
    override func start() -> Observable<CoordinationResult> {
        let viewController = GGActionSheetViewController.initFromXib()
        
        let viewModel = GGActionSheetViewModel(photoAssetService, isUserPhotoExist)
        viewController.viewModel = viewModel

        viewController.transitioningDelegate = actionSheetPresentationManager
        viewController.modalPresentationStyle = .custom
        parentViewController.present(viewController, animated: true)
        
        let close = viewModel.output.close.map { CoordinationResult.close }
        let removePhoto = viewModel.output.removePhoto.map { CoordinationResult.removePhoto }
        let viewPhoto = viewModel.output.viewPhoto.map { CoordinationResult.viewPhoto }
        let viewGalleryPhoto = viewModel.output.viewGalleryPhoto.map { CoordinationResult.viewGalleryPhoto }
        let viewQuickPhoto = viewModel.output.viewQuickPhoto.map { CoordinationResult.viewQuickPhoto($0) }
        
        return Observable.merge(close, removePhoto, viewPhoto, viewGalleryPhoto, viewQuickPhoto)
            .take(1)
            .do(onNext: { [weak self] _ in
                self?.parentViewController.dismiss(animated: true)
            })
    }
    
}

//extension GGActionSheetCoordinatorResult: Equatable {
//    
//    public static func ==(lhs: GGActionSheetCoordinatorResult, rhs:GGActionSheetCoordinatorResult) -> Bool {
//        switch lhs {
//        case .code(let a):
//            switch rhs {
//            case .code(let b):
//                return a == b
//            case .none:
//                return false
//            }
//        case .none:
//            switch rhs {
//            case .none:
//                return true
//            case .code:
//                return false
//            }
//        }
//    }
//}
