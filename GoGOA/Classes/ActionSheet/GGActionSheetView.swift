//
//  GGActionSheetView.swift
//  GoGOA
//
//  Created by m.shilo on 16/09/2019.
//  Copyright © 2019 m.shilo. All rights reserved.
//

import UIKit
import Photos
import RxSwift
import RxCocoa
import RxDataSources

struct GGAssetsSection {
    var items: [PHAsset]
}
extension GGAssetsSection: AnimatableSectionModelType {

    typealias Item = PHAsset
    var identity: Int {
        return 0
    }

    init(original: GGAssetsSection, items: [PHAsset]) {
        self = original
        self.items = items
    }
}



class GGActionSheetView: UIView {
    
    typealias ViewModelType = GGActionSheetViewViewModel
    
    var viewModel: ViewModelType!
    
    @IBOutlet var contentView: UIView!
    
    
    var contentViewTopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var topView: UIView! {
        didSet {
            topView.layer.cornerRadius = 15
            topView.clipsToBounds = true
        }
    }
    @IBOutlet weak var photoCollectionView: UICollectionView! {
        didSet {
            photoCollectionView.register(UINib(nibName: String(describing: PhotoGalleryCollectionViewCell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: PhotoGalleryCollectionViewCell.self))
            photoCollectionView.register(UINib(nibName: String(describing: GGCameraCollectionReusableView.self), bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: String(describing: GGCameraCollectionReusableView.self))
        }
    }
    @IBOutlet weak var stackViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var chooseView: UIView!
    @IBOutlet weak var choosePhotoBtn: UIButton! {
        didSet {
            choosePhotoBtn.setTitle("Choose photo", for: .normal)
        }
    }
    
    @IBOutlet weak var viewView: UIView!
    @IBOutlet weak var viewPhotoBtn: UIButton! {
        didSet {
            viewPhotoBtn.setTitle("View photo", for: .normal)
        }
    }
    
    @IBOutlet weak var removeView: UIView!
    @IBOutlet weak var removePhotoBtn: UIButton! {
        didSet {
            removePhotoBtn.setTitleColor(.red, for: .normal)
            removePhotoBtn.setTitle("Remove photo", for: .normal)
        }
    }
    

    @IBOutlet weak var cancelBtn: UIButton! {
        didSet {
            cancelBtn.titleLabel?.text = "Cancel"
            cancelBtn.layer.cornerRadius = 15
            cancelBtn.clipsToBounds = true
        }
    }
    
    private let disposeBag = DisposeBag()
        
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit() {
        Bundle.main.loadNibNamed(String(describing: GGActionSheetView.self), owner: self, options: [:])
        
        addSubview(contentView)
        
        contentView.translatesAutoresizingMaskIntoConstraints = false
        contentViewTopConstraint = contentView.topAnchor.constraint(equalTo: topAnchor)
        contentViewTopConstraint.isActive = true
//        contentView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        contentView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        contentView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        contentView.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        
    }
    
    func configure() {
        
        removePhotoBtn.rx.tap
            .bind(to: viewModel.input.removePhotoDidTap)
            .disposed(by: disposeBag)
        
        viewPhotoBtn.rx.tap
            .bind(to: viewModel.input.viewPhotoDidTap)
            .disposed(by: disposeBag)
        
        choosePhotoBtn.rx.tap
            .bind(to: viewModel.input.choosePhotoDidTap)
            .disposed(by: disposeBag)
        
        cancelBtn.rx.tap
            .bind(to: viewModel.input.closeDidTap)
            .disposed(by: disposeBag)
        
        viewModel.output.showViewRemoveItems
            .bind(to: rx.isUserPhotoExist)
            .disposed(by: disposeBag)
        
        
//        viewModel.output.galleryPhotos
//            .bind(to: photoCollectionView.rx.items(cellIdentifier: String(describing: PhotoGalleryCollectionViewCell.self), cellType: PhotoGalleryCollectionViewCell.self)) { [weak self] (index, photo, cell) in
//                guard let strongSelf = self else { return }
//
//                cell.viewModel = strongSelf.viewModel.cellViewModels[index]
//                cell.configure()
//
//            }
//            .disposed(by: disposeBag)
        
        
        let dataSource = RxCollectionViewSectionedAnimatedDataSource<GGAssetsSection>(configureCell: { [weak self] dataSource, collectionView, indexPath, event in
            guard let strongSelf = self else { return UICollectionViewCell() }
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: PhotoGalleryCollectionViewCell.self), for: indexPath) as! PhotoGalleryCollectionViewCell
            cell.viewModel = strongSelf.viewModel.cellViewModels[indexPath.row]
            cell.configure()
            
            return cell
        })
        
        dataSource.configureSupplementaryView = { dataSource, collectionView, kind, indexPath -> UICollectionReusableView in
            switch kind {
            case UICollectionView.elementKindSectionHeader:
                
                let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: String(describing: GGCameraCollectionReusableView.self), for: indexPath) as! GGCameraCollectionReusableView
                headerView.setupSession()
                return headerView
                
            default:
                return UICollectionReusableView()
            }
        }
        
        viewModel.output.galleryPhotos
            .bind(to: photoCollectionView.rx.items(dataSource: dataSource))
            .disposed(by: disposeBag)
        
        
    }

}
