//
//  GGActionSheetViewModel.swift
//  GoGOA
//
//  Created by m.shilo on 13/09/2019.
//  Copyright © 2019 m.shilo. All rights reserved.
//

import RxSwift
import Photos
import Foundation

class GGActionSheetViewModel: ViewModelProtocol {
    
    struct Input {
        let backgroundDidTap: AnyObserver<Void>
    }
    struct Output {
        let sheetView: Observable<GGActionSheetViewViewModel>
        let close: Observable<Void>
        let error: Observable<AppError>
        let removePhoto: Observable<Void>
        let viewPhoto: Observable<Void>
        let viewGalleryPhoto: Observable<Void>
        let viewQuickPhoto: Observable<PHAsset>
    }
    
    let input: Input
    let output: Output
    
    //INPUT
    private let backgroundDidTapSubject = PublishSubject<Void>()
    //OUTPUT
    private let sheetViewSubject: BehaviorSubject<GGActionSheetViewViewModel>
    private let closeSubject = PublishSubject<Void>()
    private let errorSubject = PublishSubject<AppError>()
    private let removePhotoSubject = PublishSubject<Void>()
    private let viewPhotoSubject = PublishSubject<Void>()
    private let viewGalleryPhotoSubject = PublishSubject<Void>()
    private let viewQuickPhotoSubject = PublishSubject<PHAsset>()
    
    private let disposeBag = DisposeBag()
    
    
    init(_ photoAssetService: PhotoAssetService, _ isUserPhotoExist: Bool) {
        let actionSheetViewViewModel = GGActionSheetViewViewModel(photoAssetService, isUserPhotoExist)
        sheetViewSubject = BehaviorSubject<GGActionSheetViewViewModel>(value: actionSheetViewViewModel)
        input = Input(backgroundDidTap: backgroundDidTapSubject.asObserver())
        output = Output(sheetView: sheetViewSubject.asObservable(),
                        close: closeSubject.asObservable(),
                        error: errorSubject.asObservable(),
                        removePhoto: removePhotoSubject.asObservable(),
                        viewPhoto: viewPhotoSubject.asObservable(),
                        viewGalleryPhoto: viewGalleryPhotoSubject.asObservable(),
                        viewQuickPhoto: viewQuickPhotoSubject.asObservable())
        
        backgroundDidTapSubject
            .bind(to: closeSubject)
            .disposed(by: disposeBag)
        
        actionSheetViewViewModel.output.close
            .bind(to: closeSubject)
            .disposed(by: disposeBag)
        
        actionSheetViewViewModel.output.removePhoto
            .bind(to: removePhotoSubject)
            .disposed(by: disposeBag)
        
        actionSheetViewViewModel.output.viewPhoto
            .bind(to: viewPhotoSubject)
            .disposed(by: disposeBag)
        
        actionSheetViewViewModel.output.viewGalleryPhoto
            .bind(to: viewGalleryPhotoSubject)
            .disposed(by: disposeBag)
        
        actionSheetViewViewModel.output.viewQuickPhoto
            .map { $0 }
            .bind(to: viewQuickPhotoSubject)
            .disposed(by: disposeBag)
    }
    
    deinit {
        backgroundDidTapSubject.on(.completed)
        
        sheetViewSubject.on(.completed)
        closeSubject.on(.completed)
        errorSubject.on(.completed)
        removePhotoSubject.on(.completed)
        viewPhotoSubject.on(.completed)
        viewGalleryPhotoSubject.on(.completed)
        viewQuickPhotoSubject.on(.completed)
    }
}
