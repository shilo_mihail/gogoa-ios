//
//  GGActionSheetController.swift
//  GoGOA
//
//  Created by m.shilo on 13/09/2019.
//  Copyright © 2019 m.shilo. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Photos



class GGActionSheetViewController: UIViewController, ControllerType, XibInitializable {
    
    typealias ViewModelType = GGActionSheetViewModel
    
    var viewModel: ViewModelType!
    
    @IBOutlet weak var backgroundView: ClickableView! {
        didSet {
            backgroundView.alpha = 0
        }
    }
    @IBOutlet weak var actionSheetView: GGActionSheetView!
    @IBOutlet weak var actionSheetViewTopConstraint: NSLayoutConstraint!
        
    
    var disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        bind()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print("resources: \(RxSwift.Resources.total)")
    }


    func bind() {
        
        backgroundView.tap.rx.event
            .map { _ -> Void in }
            .bind(to: viewModel.input.backgroundDidTap)
            .disposed(by: disposeBag)
        
        viewModel.output.sheetView
            .bind(to: actionSheetView.rx.viewModel)
            .disposed(by: disposeBag)
    }
    
    

}

//fileprivate final class PhotoGalleryDataSource<S: AnimatableSectionModelType>: RxCollectionViewSectionedReloadDataSource<S> {
//    var fetchResult : PHFetchResult<PHAsset>? = nil
//
//    override func collectionView(_ collectionView: UICollectionView, observedEvent: Event<[S]>) {
//
//        if let _ = fetchResult {
//            let count = fetchResult!.count
//            let assets = fetchResult!.objects(at: IndexSet(arrayLiteral: count))
//            let section: GGAssetsSection = GGAssetsSection(items: assets)
//            super.collectionView(collectionView, observedEvent: Event.next(assets))
//        } else {
//            super.collectionView(collectionView, observedEvent: observedEvent)
//        }
//
//    }
//}
