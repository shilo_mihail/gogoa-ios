//
//  PhotoGalleryCellViewModel.swift
//  GoGOA
//
//  Created by m.shilo on 17/10/2019.
//  Copyright © 2019 m.shilo. All rights reserved.
//

import Foundation
import RxSwift
import Photos


class PhotoGalleryCellViewModel: ViewModelProtocol {
    
    struct Input {

    }
    struct Output {
        let image: Observable<UIImage>
    }
    
    let input: Input
    let output: Output
    
    //INPUT

    //OUTPUT
    private let imageSubject = ReplaySubject<UIImage>.create(bufferSize: 1) 

    private let disposeBag = DisposeBag()
    
    
    init(_ photoAssetService: PhotoAssetService, _ asset: PHAsset, _ targetSize: CGSize) {
        input = Input()
        output = Output(image: imageSubject.asObservable())
        
        photoAssetService.fetchImage(asset, targetSize)
            .filter { $0 != nil }
            .map { $0! }
            .bind(to: imageSubject)
            .disposed(by: disposeBag)
    }
    
    deinit {
        imageSubject.on(.completed)
    }
}
