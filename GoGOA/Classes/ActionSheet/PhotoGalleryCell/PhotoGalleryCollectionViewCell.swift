//
//  PhotoIconCollectionViewCell.swift
//  GoGOA
//
//  Created by m.shilo on 16/10/2019.
//  Copyright © 2019 m.shilo. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class PhotoGalleryCollectionViewCell: UICollectionViewCell {
    
    typealias ViewModelType = PhotoGalleryCellViewModel
    
    var viewModel: ViewModelType!

    
    @IBOutlet weak var photoImageView: UIImageView!
    
    var disposeBag = DisposeBag()
    
//    override func draw(_ rect: CGRect) {
//        super.draw(rect)
//        
//        configure()
//    }
    
    func configure() {
        viewModel.output.image
            .bind(to: photoImageView.rx.image)
            .disposed(by: disposeBag)
    }
    
    static private var cellHeight: CGFloat = -1.0
    static func height() -> CGFloat {
        if cellHeight < 0 {
            let cell = Bundle.main.loadNibNamed(String(describing: PhotoGalleryCollectionViewCell.self), owner: self, options: nil)![0] as! PhotoGalleryCollectionViewCell
            cellHeight = cell.frame.size.height
        }
        return cellHeight
    }

}
