//
//  GGActionSheetViewViewModel.swift
//  GoGOA
//
//  Created by m.shilo on 17/10/2019.
//  Copyright © 2019 m.shilo. All rights reserved.
//

import Foundation
import RxSwift
import Photos


class GGActionSheetViewViewModel: ViewModelProtocol {
    
    struct Input {
        let closeDidTap: AnyObserver<Void>
        let removePhotoDidTap: AnyObserver<Void>
        let viewPhotoDidTap: AnyObserver<Void>
        let choosePhotoDidTap: AnyObserver<Void>
    }
    struct Output {
        let close: Observable<Void>
        let galleryPhotos: Observable<[GGAssetsSection]>
        let showViewRemoveItems: Observable<Bool>
        let removePhoto: Observable<Void>
        let viewPhoto: Observable<Void>
        let choosePhoto: Observable<Void>
    }
    
    let input: Input
    let output: Output
    
    //INPUT
    private let closeDidTapSubject = PublishSubject<Void>()
    private let removePhotoDidTapSubject = PublishSubject<Void>()
    private let viewPhotoDidTapSubject = PublishSubject<Void>()
    private let choosePhotoDidTapSubject = PublishSubject<Void>()
    //OUTPUT
    private let closeSubject = PublishSubject<Void>()
    private let galleryPhotosSubject = ReplaySubject<[GGAssetsSection]>.create(bufferSize: 1)
    private let showViewRemoveItemsSubject: BehaviorSubject<Bool>
    private let removePhotoSubject = PublishSubject<Void>()
    private let viewPhotoSubject = PublishSubject<Void>()
    private let choosePhotoSubject = PublishSubject<Void>()
        
    private let disposeBag = DisposeBag()
    
    var cellViewModels: [PhotoGalleryCellViewModel] = []
    
    
    init(_ photoAssetService: PhotoAssetService, _ isUserPhotoExist: Bool) {
        showViewRemoveItemsSubject = BehaviorSubject<Bool>(value: isUserPhotoExist)
        input = Input(closeDidTap: closeDidTapSubject.asObserver(),
                      removePhotoDidTap: removePhotoDidTapSubject.asObserver(),
                      viewPhotoDidTap: viewPhotoDidTapSubject.asObserver(),
                      choosePhotoDidTap: choosePhotoDidTapSubject.asObserver())
        output = Output(close: closeSubject.asObservable(),
                        galleryPhotos: galleryPhotosSubject.asObservable(),
                        showViewRemoveItems: showViewRemoveItemsSubject.asObservable(),
                        removePhoto: removePhotoSubject.asObservable(),
                        viewPhoto: viewPhotoSubject.asObservable(),
                        choosePhoto: choosePhotoSubject.asObservable())
        
        let targetSize = CGSize(width: PhotoGalleryCollectionViewCell.height(), height: PhotoGalleryCollectionViewCell.height())
        
        photoAssetService.fetchPhotoAssets()
            .map({ [weak self] assets -> [GGAssetsSection] in
                for asset in assets {
                    let cellViewModel = PhotoGalleryCellViewModel(photoAssetService, asset, targetSize)
                    self?.cellViewModels.append(cellViewModel)
                }
                return [GGAssetsSection(items: assets)]
            })
            .bind(to: galleryPhotosSubject)
            .disposed(by: disposeBag)
        
        closeDidTapSubject
            .bind(to: closeSubject)
            .disposed(by: disposeBag)
        
        removePhotoDidTapSubject
            .bind(to: removePhotoSubject)
            .disposed(by: disposeBag)
        
        viewPhotoDidTapSubject
            .bind(to: viewPhotoSubject)
            .disposed(by: disposeBag)
        
        choosePhotoDidTapSubject
            .bind(to: choosePhotoSubject)
            .disposed(by: disposeBag)
    }

    
    deinit {
        closeDidTapSubject.on(.completed)
        removePhotoDidTapSubject.on(.completed)
        viewPhotoDidTapSubject.on(.completed)
        choosePhotoDidTapSubject.on(.completed)
        
        closeSubject.on(.completed)
        galleryPhotosSubject.on(.completed)
        showViewRemoveItemsSubject.on(.completed)
        removePhotoSubject.on(.completed)
        viewPhotoSubject.on(.completed)
        choosePhotoSubject.on(.completed)
    }
}
