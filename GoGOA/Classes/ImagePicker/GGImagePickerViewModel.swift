//
//  GGImagePickerViewModel.swift
//  GoGOA
//
//  Created by m.shilo on 15.07.2020.
//  Copyright © 2020 m.shilo. All rights reserved.
//

import RxSwift
import Foundation

class GGImagePickerViewModel: ViewModelProtocol {
    
    struct Input {
        let didCancel: AnyObserver<Void>
        let didFinishPickingMediaWithInfo: AnyObserver<Dictionary<String, Any>>
    }
    struct Output {
        let cancel: Observable<Void>
        let selectedImage: Observable<Data>
    }
    
    let input: Input
    let output: Output
    
    private let didCancelSubject = PublishSubject<Void>()
    private let didFinishPickingMediaWithInfoSubject = PublishSubject<Dictionary<String, Any>>()

    private let cancelSubject = PublishSubject<Void>()
    private let selectedImageSubject = PublishSubject<Data>()
    
    private let disposeBag = DisposeBag()
    
    init() {
        
        input = Input(didCancel: didCancelSubject.asObserver(),
                      didFinishPickingMediaWithInfo: didFinishPickingMediaWithInfoSubject.asObserver())
        output = Output(cancel: cancelSubject.asObservable(),
                        selectedImage: selectedImageSubject.asObservable())
        
        
        didCancelSubject
            .bind(to: cancelSubject)
            .disposed(by: disposeBag)
        
        didFinishPickingMediaWithInfoSubject
            .map { info -> Data? in
                if var image = info[UIImagePickerController.InfoKey.originalImage.rawValue] as? UIImage {
                    if image.imageOrientation != .up, let fixedImage = image.fixedOrientation() {
                        image = fixedImage
                    }
                    if image.pngData()!.count > 1024*1024*10 {
                        return image.jpegData(compressionQuality: 0.5)
                    } else {
                        return image.pngData()
                    }
                }
                return nil
            }
            .filter { $0 != nil }
            .map { $0! }
            .bind(to: selectedImageSubject)
            .disposed(by: disposeBag)

    }
    
    deinit {
        didCancelSubject.on(.completed)
        didFinishPickingMediaWithInfoSubject.on(.completed)
        
        cancelSubject.on(.completed)
        selectedImageSubject.on(.completed)
    }
    
    
}
