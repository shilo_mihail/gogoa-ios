//
//  GGImagePickerController.swift
//  GoGOA
//
//  Created by m.shilo on 15.07.2020.
//  Copyright © 2020 m.shilo. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class GGImagePickerController: UIImagePickerController, ControllerType {
    
    typealias ViewModelType = GGImagePickerViewModel
    
    var viewModel: ViewModelType!

    
    var disposeBag = DisposeBag()
    
    
    override func viewDidLoad(){
        super.viewDidLoad()
        
        sourceType = .photoLibrary
        allowsEditing = false
        bind()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print("resources: \(RxSwift.Resources.total)")
    }

    func bind() {
        self.rx.didCancel
            .bind(to: viewModel.input.didCancel)
            .disposed(by: disposeBag)
        
        self.rx.didFinishPickingMediaWithInfo
            .bind(to: viewModel.input.didFinishPickingMediaWithInfo)
            .disposed(by: disposeBag)
    }
}

