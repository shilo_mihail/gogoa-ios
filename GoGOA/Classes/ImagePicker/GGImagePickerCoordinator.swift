//
//  GGImagePickerCoordinator.swift
//  GoGOA
//
//  Created by m.shilo on 15.07.2020.
//  Copyright © 2020 m.shilo. All rights reserved.
//

import UIKit
import RxSwift

enum GGImagePickerCoordinationResult {
    case cancel
    case selected(Data)
}


class GGImagePickerCoordinator: BaseCoordinator<GGImagePickerCoordinationResult> {
    
    private let parentViewController: UIViewController
    
    init(_ parentViewController: UIViewController) {
        self.parentViewController = parentViewController
    }
    
    override func start() -> Observable<CoordinationResult> {
        let pickerController = GGImagePickerController()
        let viewModel = GGImagePickerViewModel()
        pickerController.viewModel = viewModel
        
        parentViewController.present(pickerController, animated: true)

        let cancel = viewModel.output.cancel.map { CoordinationResult.cancel }
        let selectedImage = viewModel.output.selectedImage.map { CoordinationResult.selected($0) }
        
        return Observable.merge(cancel, selectedImage)
            .take(1)
            .do(onNext: { result in
                pickerController.dismiss(animated: true)
            })
        
    }

}
