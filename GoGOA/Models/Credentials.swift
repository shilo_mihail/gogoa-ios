//
//  Credentials.swift
//  GoGOA
//
//  Created by m.shilo on 09/12/2018.
//  Copyright © 2018 m.shilo. All rights reserved.
//

import Foundation

class Credentials {
    let login: String
    let password: String
    
    init(login: String, password: String) {
        self.login = login
        self.password = password
    }
}
