//
//  Account.swift
//  GoGOA
//
//  Created by m.shilo on 03/02/2019.
//  Copyright © 2019 m.shilo. All rights reserved.
//

import Locksmith
import Foundation

class Account: ReadableSecureStorable, CreateableSecureStorable, DeleteableSecureStorable, GenericPasswordSecureStorable {
    
    var login: String!
    var password: String!
    
    let service = "GoGOA"
    var account: String {
        return login
        
    }
    var data: [String: Any] {
        return [
            "password": password
        ]
    }
    
    convenience init(_ login: String, _ password: String) {
        self.init(login)
        self.password = password
    }
    
    init(_ login: String) {
        self.login = login
    }
}

