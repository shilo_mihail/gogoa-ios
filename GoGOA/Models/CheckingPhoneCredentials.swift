//
//  CheckPhoneCredentials.swift
//  GoGOA
//
//  Created by m.shilo on 05/04/2019.
//  Copyright © 2019 m.shilo. All rights reserved.
//

import Foundation

struct CheckingPhoneCredentials {
    let verificationId: String
    let verificationCode: String
}
