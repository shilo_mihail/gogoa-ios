//
//  GGMarker.swift
//  GoGOA
//
//  Created by m.shilo on 02.09.2020.
//  Copyright © 2020 m.shilo. All rights reserved.
//

import Foundation
import GoogleMaps

class GGMarker: GMSMarker {
    
    private var _isSelected: Bool = false
    var isSelected: Bool {
        get {
            return _isSelected
        }
        set {
            _isSelected = newValue
        }
    }
    
    private var _isDataLoading: Bool = false
    var isDataLoading: Bool {
        get {
            return _isDataLoading
        }
        set {
            _isDataLoading = newValue
        }
    }
    
    convenience init(_ position: CLLocationCoordinate2D) {
        self.init(position: position)
    }
    
}
