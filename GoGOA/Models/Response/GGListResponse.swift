//
//  ResponseArrayModel.swift
//  GoGOA
//
//  Created by m.shilo on 08/12/2018.
//  Copyright © 2018 m.shilo. All rights reserved.
//

import Foundation
import ObjectMapper

class GGListResponse<T: Mappable>: Mappable {
    
    var data: [T]?
    var code: Int = 0
    var total: Int = 0
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        data <- map["data"]
        code <- map["code"]
        total <- map["total"]
    }
}
