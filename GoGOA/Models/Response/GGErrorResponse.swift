//
//  ResponseError.swift
//  GoGOA
//
//  Created by m.shilo on 05/02/2019.
//  Copyright © 2019 m.shilo. All rights reserved.
//

import Foundation
import ObjectMapper

class GGErrorResponse: Mappable {
    
    var message: String = ""
    var code: Int = 0
    
    required init?(map: Map) {}
    
    
    func mapping(map: Map) {
        message <- map["message"]
        code <- map["code"]
    }
}
