//
//  ResponsePage.swift
//  GoGOA
//
//  Created by m.shilo on 05.08.2020.
//  Copyright © 2020 m.shilo. All rights reserved.
//

import Foundation
import ObjectMapper

class GGPageResponse<T: Mappable>: GGListResponse<T> {
    
    var limit: Int = 0
    var offset: Int = 0
    
    required init?(map: Map) {
        super.init(map: map)
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        limit <- map["limit"]
        offset <- map["offset"]
    }
}
