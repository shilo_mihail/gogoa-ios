//
//  Photo.swift
//  GoGOA
//
//  Created by m.shilo on 21/06/2018.
//  Copyright © 2018 m.shilo. All rights reserved.
//

import Foundation
import RealmSwift
import Realm
import ObjectMapper

class Photo: Object, Mappable {
    
   
    @objc dynamic var id = 0
    @objc dynamic var photoBinary: PhotoBinary?
    @objc dynamic var smallPhotoBinary: PhotoBinary?
    
    required init?(map: Map) {
        super.init()
    }
    
    required init() {
        super.init()
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        photoBinary <- map["photoBinary"]
        smallPhotoBinary <- map["smallPhotoBinary"]
    }
    
}

