//
//  Role.swift
//  GoGOA
//
//  Created by m.shilo on 04.07.2020.
//  Copyright © 2020 m.shilo. All rights reserved.
//

import Foundation

enum GGRole: String {
    case USER
    case TEMP_USER
}
