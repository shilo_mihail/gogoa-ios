//
//  User.swift
//  GoGOA
//
//  Created by m.shilo on 08/12/2018.
//  Copyright © 2018 m.shilo. All rights reserved.
//

import Foundation
import RealmSwift
import Realm
import ObjectMapper
import RxDataSources

class GGUser: Object, Mappable {
    
    @objc dynamic var id = 0
    @objc dynamic var deviceId = ""
    @objc dynamic var age = 0
    @objc dynamic var name = ""
    @objc dynamic var login = ""
    @objc dynamic var password = ""
    @objc dynamic var photoIcon: Photo?
    @objc dynamic var iconHexColor = Utils.randomHexInt()
    @objc dynamic var role = ""
    
    
    
    required init() {
        super.init()
    }
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    
    func mapping(map: Map) {
        id <- map["id"]
        deviceId <- map["deviceId"]
        age <- map["age"]
        name <- map["name"]
        login <- map["login"]
        photoIcon  <- map["icon"]
        role  <- map["role"]
    }
}

extension GGUser: IdentifiableType {
    var identity: Int {
        return id
    }
}
