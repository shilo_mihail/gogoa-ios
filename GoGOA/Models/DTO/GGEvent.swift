//
//  Event.swift
//  GoGOA
//
//  Created by m.shilo on 25/01/2019.
//  Copyright © 2019 m.shilo. All rights reserved.
//

import Foundation
import RealmSwift
import Realm
import ObjectMapper
//import RxDataSources


class GGEvent: Object, Mappable {
    
    @objc dynamic var id = 0
    @objc dynamic var title = ""
    @objc dynamic var price = 0
    @objc dynamic var eventDate = Date(timeIntervalSince1970: 1)
    @objc dynamic var creationDate = Date(timeIntervalSince1970: 1)
    @objc dynamic var usersCount = 0
    @objc dynamic var descript = ""
    @objc dynamic var photoIcon: Photo?
    @objc dynamic var location: Location?
    var users = List<GGUser>()
    var comments = List<Comment>()
    var photos = List<Photo>()
    
    
    
    required init?(map: Map) {
        super.init()
    }
    
    required init() {
        super.init()
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        title <- map["title"]
        price <- map["price"]
        eventDate <- (map["eventDate"], DateTransform())
        creationDate  <- (map["creationDate"], DateTransform())
        usersCount <- map["usersCount"]
        descript <- map["description"]
        photoIcon  <- map["icon"]
        location  <- map["location"]
        comments  <- map["comments"]
        photos <- map["photos"]
        users <- map["users"]
    }
    
}

//extension GGEvent: IdentifiableType {
//    var identity: Int {
//        return id
//    }
//}

//extension GGEvent: Equatable {
//    static func == (lhs: GGEvent, rhs: GGEvent) -> Bool {
//        return lhs.id == rhs.id
//    }
//}
