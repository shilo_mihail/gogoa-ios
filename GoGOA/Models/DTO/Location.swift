//
//  Point.swift
//  GoGOA
//
//  Created by m.shilo on 20/01/2018.
//  Copyright © 2018 m.shilo. All rights reserved.
//

import Foundation
import RealmSwift
import Realm
import ObjectMapper

enum LocationType: String {
    case food = "food"
    case travel = "travel"
}

class Location: Object, Mappable {
    
    @objc dynamic var id = 0
    @objc dynamic var latitude = 0.0
    @objc dynamic var longitude = 0.0
    @objc dynamic var type = LocationType.travel.rawValue
    @objc dynamic var title = ""
    @objc dynamic var descript = ""
    @objc dynamic var address = ""
    @objc dynamic var rating = 0
    var comments = List<Comment>()
    var photos = List<Photo>()
    
    var typeEnum: LocationType {
        get {
            return LocationType(rawValue: type)!
        }
        set {
            type = newValue.rawValue
        }
    }
    
    
    required init?(map: Map) {
        super.init()
    }
    
    
    required init() {
        super.init()
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        latitude <- map["latitude"]
        longitude <- map["longitude"]
        type  <- map["type"]
        title <- map["title"]
        descript  <- map["description"]
        address  <- map["address"]
        rating  <- map["rating"]
        comments  <- map["comments"]
        photos <- (map["photos"], ListTransform())
    }
    
    
}
