//
//  PointComment.swift
//  GoGOA
//
//  Created by m.shilo on 20/01/2018.
//  Copyright © 2018 m.shilo. All rights reserved.
//

import Foundation
import RealmSwift
import Realm

class Comment: Object {
    
    @objc dynamic var authorName = ""
    @objc dynamic var authorAge = 0
    @objc dynamic var comment = ""
    @objc dynamic var rating = 0
}
