//
//  PhotoBinary.swift
//  GoGOA
//
//  Created by m.shilo on 28/01/2019.
//  Copyright © 2019 m.shilo. All rights reserved.
//

import Foundation
import RealmSwift
import Realm
import ObjectMapper

class PhotoBinary: Object, Mappable {
    
    @objc dynamic var id = 0
    @objc dynamic var data = Data()
    
    required init?(map: Map) {
        super.init()
    }
    
    init(id: Int, data: Data) {
        super.init()
        
        self.id = id
        self.data = data
    }
    
    required init() {
        super.init()
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        data <- (map["data"], DataTransform())
    }
    
}
