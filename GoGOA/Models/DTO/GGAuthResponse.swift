//
//  GGAuthResponse.swift
//  GoGOA
//
//  Created by m.shilo on 29.07.2020.
//  Copyright © 2020 m.shilo. All rights reserved.
//

import Foundation
import ObjectMapper

class GGAuthResponse: Mappable {

    var user: GGUser!
    var todayFutureEventsCount: [String : Int] = [:]
    
    required init?(map: Map) {
        
    }

    func mapping(map: Map) {
        user <- map["user"]
        todayFutureEventsCount <- map["todayFutureEventsCount"]
    }
}
