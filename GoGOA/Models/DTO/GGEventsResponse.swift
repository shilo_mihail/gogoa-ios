//
//  GGTodayEventsResponse.swift
//  GoGOA
//
//  Created by m.shilo on 31.07.2020.
//  Copyright © 2020 m.shilo. All rights reserved.
//

import Foundation
import ObjectMapper

class GGEventsResponse: Mappable {

    var events: [GGEvent]!
    var futureEventsCount: Int?
    var pastEventsCount: Int?
    
    required init?(map: Map) {
        
    }
    
    init(_ events: [GGEvent], futureEventsCount: Int? = nil, pastEventsCount: Int? = nil) {
        self.events = events
        self.futureEventsCount = futureEventsCount
        self.pastEventsCount = pastEventsCount
    }

    func mapping(map: Map) {
        events <- map["events"]
        futureEventsCount <- map["futureEventsCount"]
        pastEventsCount <- map["pastEventsCount"]
    }
}
