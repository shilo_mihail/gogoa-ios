//
//  ListTransform.swift
//  GoGOA
//
//  Created by m.shilo on 27.08.2020.
//  Copyright © 2020 m.shilo. All rights reserved.
//

import Foundation
import ObjectMapper
import RealmSwift

class ListTransform<T: RealmCollectionValue & BaseMappable>: TransformType {
    
    typealias Object = List<T>
    typealias JSON = Array<T>
    
    init() {}
    
    func transformFromJSON(_ value: Any?) -> List<T>? {
        if let array = Mapper<T>().mapArray(JSONObject: value) {
            let list = List<T>()
            for item in array {
                list.append(item)
            }
            return list
        }
        return nil
    }
    
    func transformToJSON(_ value: List<T>?) -> Array<T>? {
        if let list = value {
            return Array(list)
        }
        return nil
    }
}
