//
//  DataTransform.swift
//  GoGOA
//
//  Created by m.shilo on 21/06/2018.
//  Copyright © 2018 m.shilo. All rights reserved.
//

import Foundation
import ObjectMapper

class DataTransform: TransformType {
    
    typealias Object = Data
    typealias JSON = String
    
    public init() {}
    
    func transformFromJSON(_ value: Any?) -> Data? {
        if let base64str = value as? String {
            return Data(base64Encoded: base64str, options: .ignoreUnknownCharacters)!
        }
        return nil
    }
    
    func transformToJSON(_ value: Data?) -> String? {
        if let data = value {
            return data.base64EncodedString(options: .endLineWithCarriageReturn)
        }
        return nil
    }
}
