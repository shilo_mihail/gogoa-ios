//
//  RegCredentials.swift
//  GoGOA
//
//  Created by m.shilo on 30/01/2019.
//  Copyright © 2019 m.shilo. All rights reserved.
//

import Foundation

class RegCredentials: Credentials {

    let name: String
    
    init(login: String, name: String, password: String) {
        self.name = name
        super.init(login: login, password: password)
    }
}
