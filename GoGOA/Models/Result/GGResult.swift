//
//  Result.swift
//  GoGOA
//
//  Created by m.shilo on 30/12/2018.
//  Copyright © 2018 m.shilo. All rights reserved.
//

import Foundation

enum GGResult<T> {
    case success(T)
    case failure(AppError)
}

extension GGResult {

    func flatMap<U>(_ transform: (T) -> GGResult<U>) -> GGResult<U> {
        switch self {
            case let .success(value): return transform(value)
            case let .failure(error): return .failure(error)
        }
    }
}
