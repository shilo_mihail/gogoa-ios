//
//  XibInitializable.swift
//  GoGOA
//
//  Created by m.shilo on 29/01/2019.
//  Copyright © 2019 m.shilo. All rights reserved.
//

import UIKit

protocol XibInitializable {
    static var xibIdentifier: String { get }
}

extension XibInitializable where Self: UIViewController {
    
    static var xibIdentifier: String {
        return String(describing: Self.self)
    }
    
    static func initFromXib() -> Self {
        return  Self(nibName: xibIdentifier, bundle: nil)
    }
}
