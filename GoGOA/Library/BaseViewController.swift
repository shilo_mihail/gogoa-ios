//
//  BaseViewController.swift
//  GoGOA
//
//  Created by m.shilo on 13/02/2019.
//  Copyright © 2019 m.shilo. All rights reserved.
//

import UIKit

class GGBaseViewController: UIViewController {
    
    var backButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addBackButton()
    }
    
    private func addBackButton() {
        if let backImage = UIImage(named: "back") {
            backButton = UIButton(type: .custom)
            backButton.setImage(backImage.withRenderingMode(.alwaysTemplate), for: .normal)
            backButton.tintColor = view.tintColor

            if #available(iOS 11, *) {
                let widthConstraint = backButton.widthAnchor.constraint(equalToConstant: 32)
                let heightConstraint = backButton.heightAnchor.constraint(equalToConstant: 34)
                heightConstraint.isActive = true
                widthConstraint.isActive = true
            } else {
                backButton.frame = CGRect(x: 0, y: 0, width: 32, height: 34)
            }
            backButton.contentEdgeInsets = UIEdgeInsets(top: 0, left: -18, bottom: 0, right: 18)
            let backBtnItem = UIBarButtonItem(customView: backButton)
            navigationItem.leftBarButtonItem = backBtnItem
        }
    }
}
