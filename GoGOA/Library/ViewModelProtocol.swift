//
//  ViewModelProtocol.swift
//  GoGOA
//
//  Created by m.shilo on 09/12/2018.
//  Copyright © 2018 m.shilo. All rights reserved.
//

import Foundation

protocol ViewModelProtocol: class {
    associatedtype Input
    associatedtype Output
}
