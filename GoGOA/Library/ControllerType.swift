//
//  ControllerType.swift
//  GoGOA
//
//  Created by m.shilo on 09/12/2018.
//  Copyright © 2018 m.shilo. All rights reserved.
//

import UIKit

protocol ControllerType: class {
    associatedtype ViewModelType: ViewModelProtocol
    func bind()
}
