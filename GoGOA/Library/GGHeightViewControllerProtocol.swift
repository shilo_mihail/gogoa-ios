//
//  GGHeightViewControllerProtocol.swift
//  GoGOA
//
//  Created by m.shilo on 20.08.2020.
//  Copyright © 2020 m.shilo. All rights reserved.
//

import UIKit

protocol GGHeightViewControllerProtocol {
    
    func statusBarHeight() -> CGFloat
    func navigationBarHeight() -> CGFloat
}
