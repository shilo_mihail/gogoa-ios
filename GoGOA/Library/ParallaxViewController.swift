//
//  ParallaxNavigationController.swift
//  GoGOA
//
//  Created by m.shilo on 16/07/2019.
//  Copyright © 2019 m.shilo. All rights reserved.
//

import UIKit

enum ParallaxViewControllerLine {
    case first
    case second
    case third
}


class ParallaxViewController: UIViewController {
    
    var currentLine: ParallaxViewControllerLine!
    
    private var backgroundImageView: UIImageView!
    private var backgroundScrollView = UIScrollView(frame: CGRect.zero)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.delegate = self
        configureScrollView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        configureBackgroundImage()
    }
    
    func configureScrollView() {
        backgroundScrollView.isUserInteractionEnabled = false
        backgroundScrollView.translatesAutoresizingMaskIntoConstraints = false
        view.insertSubview(backgroundScrollView, at: 0)
        
        backgroundScrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        backgroundScrollView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        backgroundScrollView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        backgroundScrollView.heightAnchor.constraint(equalTo: view.heightAnchor).isActive = true
    }
    
    func configureBackgroundImage() {
        let backgroundImage = Constants.image.background
        backgroundImageView = UIImageView(image: backgroundImage)
        var originX: CGFloat = 0
        switch currentLine! {
        case .first:
            originX = -150
        case .second:
            originX = -150 - view.frame.width
        case .third:
            originX = -150 - 2 * view.frame.width
        }
        backgroundImageView.frame.origin.x = originX
        backgroundImageView.frame.origin.y = -150
        
        backgroundScrollView.addSubview(backgroundImageView)
        backgroundScrollView.contentSize = backgroundImage.size
    }
}

extension ParallaxViewController: UINavigationControllerDelegate {
    
    func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationController.Operation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        
        switch operation {
        case .push:
            return HorizontalParallaxAnimator(
                duration: TimeInterval(UINavigationController.hideShowBarDuration),
                isPresenting: true,
                originFrame: view.frame,
                backgroundImageView: backgroundImageView)
        default:
            return HorizontalParallaxAnimator(
                duration: TimeInterval(UINavigationController.hideShowBarDuration),
                isPresenting: false,
                originFrame: view.frame,
                backgroundImageView: backgroundImageView)
        }
    }
}

extension ParallaxViewController: GGHeightViewControllerProtocol {
    
    func statusBarHeight() -> CGFloat {
        return UIApplication.shared.statusBarFrame.height
    }
    
    func navigationBarHeight() -> CGFloat {
        return navigationController!.navigationBar.frame.height
    }
    
    
}
