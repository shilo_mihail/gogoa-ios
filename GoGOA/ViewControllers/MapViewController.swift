//
//  ViewController.swift
//  GoGOA
//
//  Created by m.shilo on 20/01/2018.
//  Copyright © 2018 m.shilo. All rights reserved.
//

import UIKit
import GoogleMaps
import Alamofire

class LocationsViewController: UIViewController {
    
    let goaCenterCoordinate = CLLocationCoordinate2D(latitude: 15.614673, longitude: 73.736870)
    

    
    @IBOutlet weak var mapView: GMSMapView! {
        didSet {
            mapView.isHidden = true
        }
    }
    @IBOutlet weak var imageView: UIImageView!
    
    var locationManager = CLLocationManager()
    
    var isMarkerSelect = false
    
    var locations = [Location]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        
        getLocations()
    }
    
    func getLocations() {
        WebService.getLocations()
        .

        
        self?.locations = locations
        self?.setupMap()
        self?.setupMarkers()
    }

    func setupMap() {
        guard locationManager.location != nil else {
            return
        }
        
        mapView.camera = GMSCameraPosition.camera(withLatitude: goaCenterCoordinate.latitude, longitude: goaCenterCoordinate.longitude, zoom: 12.0)
        mapView.delegate = self
        mapView.isMyLocationEnabled = true
        
        
    }
    
    func setupMarkers() {
        mapView.clear()
        
        for location in locations {
            
            let marker = GMSMarker(position: CLLocationCoordinate2D(latitude: location.latitude, longitude: location.longitude))
//            marker.userData = point
//            marker.isTappable = true
            print(location.title)
            marker.title = location.title
            marker.icon = GMSMarker.markerImage(with: .yellow)
            marker.map = mapView
        }
    }

    @IBAction func addPointBtnClicked(_ sender: UIButton) {
        locations.append(Location(latitude: 15.623789, longitude: 73.733116, title: "Точка ДЖИ", type: .travel))
        
        setupMarkers()
    }
    
}

extension MapViewController: GMSMapViewDelegate {
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        if !isMarkerSelect {
            UIView.animate(withDuration: 0.3) {
                mapView.frame.size = CGSize(width: mapView.frame.size.width, height: mapView.frame.size.height * 2 / 3)
            }
        }
        isMarkerSelect = true
        return true
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        if isMarkerSelect {
            UIView.animate(withDuration: 0.3) {
                mapView.frame.size = CGSize(width: mapView.frame.size.width, height: mapView.frame.size.height * 3 / 2)
            }
        }
        isMarkerSelect = false
    }
}

extension MapViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        manager.stopUpdatingLocation()
        setupMap()
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse || status == .authorizedAlways {
            locationManager.startUpdatingLocation()
        } else {
            setupMap()
        }
    }
}

