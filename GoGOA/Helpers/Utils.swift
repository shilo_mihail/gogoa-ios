//
//  Utils.swift
//  GoGOA
//
//  Created by m.shilo on 08/12/2018.
//  Copyright © 2018 m.shilo. All rights reserved.
//

import Foundation

class Utils {
    
    static func saveValue<T>(_ value: T, for key: String) {
        let defaults = UserDefaults.standard
        defaults.set(value, forKey: key)
    }
    
    static func readValue<T>(for key: String) -> T? {
        let defaults = UserDefaults.standard
        return defaults.object(forKey: key) as? T
    }
    
    static func removeObject(for key: String) {
        let defaults = UserDefaults.standard
        defaults.removeObject(forKey: key)
        defaults.synchronize()
    }
    
    static func clean(_ login: String) -> String {
        var cleanLogin = ""
        do {
            let regex = try NSRegularExpression(pattern: "[+0-9]")
            let results = regex.matches(in: login,
                                        range: NSRange(login.startIndex..., in: login))
            results.forEach { result in
                cleanLogin += String(login[Range(result.range, in: login)!])
            }
        } catch {}
        
        return cleanLogin
    }
    
    static func checkLogin(_ login: String) -> Bool {
        do {
            let regex = try NSRegularExpression(pattern: "[0-9]")
            let results = regex.matches(in: login,
                                        range: NSRange(login.startIndex..., in: login))
            return results.count >= Constants.mobilePhoneLength ? true : false
        } catch {
            return false
        }
    }
    
    static func checkName(_ name: String) -> Bool {
        return name.count >= Constants.minNameLength ? true : false
    }
    
    static func checkPassword(_ password: String) -> Bool {
        return password.count >= Constants.minPasswordLength ? true : false
    }
    
    static func checkConfirmPassword(_ password: String, _ confirm: String) -> Bool {
        return password == confirm && confirm.count > 0 ? true : false
    }
    
//    static func generateFirstCharactersImageData(for name: String) -> Data {
//        let initials = parseFirstCharacters(name)
//        let color = UIColor.random()
//        let image = UIImage.imageWithFirstCharacter(initials as NSString, color)
//        return image.pngData()!
//    }

    static func parseFirstCharacters(_ text: String) -> String {
        let words = text.components(separatedBy: " ")
        var result = ""
        for i in 0..<words.count {
            if i >= 2 {break}
            result += String(words[i].first!)
        }
        return result
    }
    
    static func getUsersCountPluralForm(_ count: Int) -> String {
        var form = "";
        let form1 = "человек";
        let form2 = "человека";
        let form5 = "человек";
        let c = count % 100;
        
        let c1 = c % 10;
        let c2 = c / 10;
        if ((c1 == 0) || (c1 >= 5 && c1 <= 9) || (c2 == 1 && (c1 >= 1 && c1 <= 4))) {
            form = form5;
        } else if (c1 == 1) {
            form = form1;
        } else if (c1 >= 2 && c1 <= 4) {
            form = form2;
        }
        return form
    }
    
    static func getYearsCountPluralForm(_ count: Int) -> String {
        var form = "";
        let form1 = "год";
        let form2 = "года";
        let form5 = "лет";
        let c = count % 100;
        
        let c1 = c % 10;
        let c2 = c / 10;
        if ((c1 == 0) || (c1 >= 5 && c1 <= 9) || (c2 == 1 && (c1 >= 1 && c1 <= 4))) {
            form = form5;
        } else if (c1 == 1) {
            form = form1;
        } else if (c1 >= 2 && c1 <= 4) {
            form = form2;
        }
        return form
    }
    
    static func randomHexInt() -> Int {
        let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let randomString = String((0..<10).map{ _ in letters.randomElement()! })
        let hash = abs(randomString.hashValue)
        return hash % (256*256*256)
    }
    
    static func generateHumanDateString(_ date: Date) -> String {
        let diff = calculateEventDateDiffWithNowDate(date)
        var eventDateString = ""
        let fullDateFormatter = DateFormatter()
        fullDateFormatter.dateFormat = "dd MMMM HH:mm"
        if diff.0 == 0 && diff.1 == 0 {
            let timeDateFormatter = DateFormatter()
            timeDateFormatter.dateFormat = "HH:mm"
            switch diff.2 {
            case 0:
                eventDateString = "Today " + timeDateFormatter.string(from: date)
            case 1:
                eventDateString = "Tomorrow " + timeDateFormatter.string(from: date)
            case -1:
                eventDateString = "Yesterday " + timeDateFormatter.string(from: date)
            default:
                eventDateString = fullDateFormatter.string(from: date)
            }
        } else {
            eventDateString = fullDateFormatter.string(from: date)
        }
        
        return eventDateString
    }
    
    static func compareEventDateWithNowDate(_ eventDate: Date) -> Bool {
        let diff = calculateEventDateDiffWithNowDate(eventDate)
        return diff.0 == 0 && diff.1 == 0 && diff.2 == 0
    }
    
    static func calculateEventDateDiffWithNowDate(_ eventDate: Date) -> (Int, Int, Int) {
        let eventYearComponent = Calendar.current.component(.year, from: eventDate)
        let eventMonthComponent = Calendar.current.component(.month, from: eventDate)
        let eventDayComponent = Calendar.current.component(.day, from: eventDate)
        
        let now = Date()
        let nowYearComponent = Calendar.current.component(.year, from: now)
        let nowMonthComponent = Calendar.current.component(.month, from: now)
        let nowDayComponent = Calendar.current.component(.day, from: now)
        
        let diffYear = eventYearComponent - nowYearComponent
        let diffMonth = eventMonthComponent - nowMonthComponent
        let diffDay = eventDayComponent - nowDayComponent
        
        return (diffYear, diffMonth, diffDay)
    }
}
