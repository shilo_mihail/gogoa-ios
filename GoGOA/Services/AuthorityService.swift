//
//  GGAuthorityViewModel.swift
//  GoGOA
//
//  Created by m.shilo on 07.07.2020.
//  Copyright © 2020 m.shilo. All rights reserved.
//

import RxSwift
import Foundation

class GGAuthorityService {
    
    let currentUser: GGUser?
    var currentRole: GGRole? {
        get {
            return GGRole(rawValue: currentUser!.role) ?? nil
        }
    }

    init(_ dataService: DataService) {
        self.currentUser = dataService.fetchCurrentUser()
    }
    
    func isAllowed(for roles: [GGRole]) -> Bool {
        return roles.contains(currentRole!)
    }
}
