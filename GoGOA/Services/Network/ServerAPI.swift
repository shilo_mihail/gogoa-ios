//
//  WebEndpoint.swift
//  GoGOA
//
//  Created by m.shilo on 13/06/2018.
//  Copyright © 2018 m.shilo. All rights reserved.
//

import Foundation
import Moya

enum ServerAPI {
    case authenticate(String, String)
    case logout
    case registration(GGUser)
    case getLocation(Int)
    case getTodayEvents
    case getPastEvents(Int)
    case getFutureEvents(Int)
    case getEventUsers(Int)
    case addEventUser(Int)
    case removeEventUser(Int)
    case getPhotoBinary(Int)
    case getLocationPhotoIcons(Int)
    case updateUserInfo(GGUser)
    case updateUserPhoto(Data)
    case removeUserPhoto
}

extension ServerAPI: TargetType {
    var baseURL: URL {
        return URL(string: "http://localhost:8080/gogoa")!
    }
    
    var xAuthToken: String? {
        return Utils.readValue(for: "xAuthToken")
    }
    
    var path: String {
        switch self {
            case .authenticate(_, _),
                 .logout: return "/authenticate"
            case .registration(_): return "/mobile/registration"
            case .getLocation(let locationId): return "/mobile/locations/\(locationId)"
            case .getTodayEvents: return "/mobile/events/today"
            case .getPastEvents: return "/mobile/events/past"
            case .getFutureEvents: return "/mobile/events/future"
            case .getEventUsers(let eventId): return "/mobile/events/\(eventId)/users"
            case .addEventUser(let eventId): return "/mobile/events/\(eventId)/user"
            case .removeEventUser(let eventId): return "/mobile/events/\(eventId)/user"
            case .getPhotoBinary(let id): return "/photos/\(id)"
            case .getLocationPhotoIcons(let locationId): return "/mobile/locations/\(locationId)/photoIcons"
            case .updateUserInfo(_): return "/mobile/user"
            case .updateUserPhoto(_),
                 .removeUserPhoto: return "/mobile/user/photo"
        }
    }
    
    var method: Moya.Method {
        switch self {
            case .registration(_),
                 .updateUserPhoto(_): return .post
            case .addEventUser(_),
                 .updateUserInfo(_): return .put
            case .removeEventUser(_),
                 .removeUserPhoto,
                 .logout: return .delete
            case .authenticate(_, _),
                 .getLocation(_),
                 .getTodayEvents,
                 .getPastEvents,
                 .getFutureEvents,
                 .getEventUsers(_),
                 .getPhotoBinary(_),
                 .getLocationPhotoIcons: return .get
        }
    }
    
    var sampleData: Data {
        switch self {
            case .authenticate(_, _),
                 .logout,
                 .registration(_),
                 .getLocation(_),
                 .getTodayEvents,
                 .getPastEvents,
                 .getFutureEvents,
                 .addEventUser(_),
                 .removeEventUser(_),
                 .getEventUsers(_),
                 .getPhotoBinary(_),
                 .getLocationPhotoIcons,
                 .removeUserPhoto,
                 .updateUserInfo(_),
                 .updateUserPhoto(_): return "".data(using: .utf8)!
        }
    }
    
    var task: Task {
        switch self {
            case .getPastEvents,
                 .getFutureEvents: return .requestParameters(parameters: parameters!, encoding: URLEncoding.queryString)
            case .authenticate(_, _),
                 .logout,
                 .getLocation(_),
                 .getEventUsers(_),
                 .getTodayEvents,
                 .addEventUser(_),
                 .removeEventUser(_),
                 .removeUserPhoto,
                 .getPhotoBinary(_),
                 .getLocationPhotoIcons(_): return .requestPlain
            case .registration(_),
                 .updateUserInfo(_): return .requestParameters(parameters: parameters!, encoding: JSONEncoding.default)
            case .updateUserPhoto(let imageData):
                let formData: [Moya.MultipartFormData] = [Moya.MultipartFormData(provider: .data(imageData), name: "file", fileName: "image.png", mimeType: "image/jpeg")]
                return .uploadMultipart(formData)
        }
    }
    
    var headers: [String : String]? {
        switch self {
        case .authenticate(let login, let password):
            return ["Authorization" : "Basic " + generateBase64LoginString(login, password)]
        case .registration(_): return nil
        default:
            if xAuthToken != nil && !xAuthToken!.isEmpty {
                return ["x-auth-token" : xAuthToken!]
            } else {
                return nil
            }
            
        }
        
    }
    
    var parameters: [String : Any]? {
        switch self {
            case .getPastEvents(let offset): return [
                    "offset": offset,
                    "limit": 10,
                    "direction": "DESC",
                    "sortColumn": "eventDate"
                 ]
            case .getFutureEvents(let offset): return [
                    "offset": offset,
                    "limit": 10,
                    "direction": "ASC",
                    "sortColumn": "eventDate"
                ]
            case .registration(let user),
                 .updateUserInfo(let user): return user.toJSON()
            case .authenticate(_, _),
                 .logout,
                 .getLocationPhotoIcons(_),
                 .getLocation(_),
                 .getEventUsers(_),
                 .getTodayEvents,
                 .addEventUser(_),
                 .removeEventUser(_),
                 .removeUserPhoto,
                 .getPhotoBinary(_),
                 .updateUserPhoto(_): return nil
        }
        
    }
    
    

}

extension ServerAPI {
    fileprivate func generateBase64LoginString(_ login: String, _ password: String) -> String {
        let loginString = String(format: "%@:%@", login, password)
        let loginData = loginString.data(using: String.Encoding.utf8)!
        return loginData.base64EncodedString()
    }
}
