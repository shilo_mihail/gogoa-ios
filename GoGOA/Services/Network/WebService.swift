//
//  WevService.swift
//  GoGOA
//
//  Created by m.shilo on 15/06/2018.
//  Copyright © 2018 m.shilo. All rights reserved.
//

import Foundation
import Moya
import RxSwift
import SwiftyJSON
import ObjectMapper

struct WebService {
    
    let provider = MoyaProvider<ServerAPI>() //plugins: [NetworkLoggerPlugin(verbose: true)]
    
    func authenticate(_ username: String, _ password: String) -> Observable<GGResult<GGAuthResponse>> {
        return provider.rx.requestWait(for: 1, .authenticate(username, password))
            .asObservable()
//            .filter(statusCode: 200)
            .map({ response -> GGResult<GGAuthResponse> in
                let json = JSON(response.data)
                if let xAuthToken = response.response?.allHeaderFields["x-auth-token"] as? String {
                    print(xAuthToken)
                    Utils.saveValue(xAuthToken, for: "xAuthToken")
                    print(json)
                    if let responseModel = Mapper<GGResponse<GGAuthResponse>>().map(JSONString: json.rawString()!) {
                        if let authResponse = responseModel.data {
                            return .success(authResponse)
                        }
                    }
                    return .failure(.mappingError)
                }
                let responseError = Mapper<GGErrorResponse>().map(JSONString: json.rawString()!)
                return .failure(.serverError(responseError!.message))
            })
            .catchErrorJustReturn(.failure(.networkError))
    }
    
    func logout() -> Observable<GGResult<Void>> {
        return provider.rx.request(.logout)
            .asObservable()
            .filter(statusCode: 204)
            .map({ response -> GGResult<Void> in
                Utils.removeObject(for: "xAuthToken")
                return .success(())
            })
            .catchErrorJustReturn(.failure(.networkError))
    }
    
    func registration(_ user: GGUser) -> Observable<GGResult<GGAuthResponse>> {
        return provider.rx.request(.registration(user))
            .asObservable()
            .filter(statusCode: 200)
            .map({ response -> GGResult<GGAuthResponse> in
                if let xAuthToken = response.response?.allHeaderFields["x-auth-token"] as? String {
                    print(xAuthToken)
                    Utils.saveValue(xAuthToken, for: "xAuthToken")
                    let json = JSON(response.data)
                    print(json)
                    if let responseModel = Mapper<GGResponse<GGAuthResponse>>().map(JSONString: json.rawString()!) {
                        if let authResponse = responseModel.data {
                            return .success(authResponse)
                        }
                    }
                    return .failure(.mappingError)
                } else {
                    return .failure(.authHeaderNotExist)
                }
            })
            .catchErrorJustReturn(.failure(.networkError))
    }
    
    func getTodayEvents() -> Observable<GGResult<GGEventsResponse>> {
        return provider.rx.requestWait(for: 1, .getTodayEvents)
            .asObservable()
            .filter(statusCode: 200)
            .map({ response -> GGResult<GGEventsResponse> in
                let json = JSON(response.data)
                print(json)
                if let response = Mapper<GGResponse<GGEventsResponse>>().map(JSONString: json.rawString()!) {
                    if let todayEventsResponse = response.data {
                        return .success(todayEventsResponse)
                    }
                }
                return .failure(.mappingError)
            })
            .catchErrorJustReturn(.failure(.networkError))
    }
    
    func getPastEvents(_ offset: Int) -> Observable<GGResult<GGEventsResponse>> {
        return provider.rx.requestWait(for: 5, .getPastEvents(offset))
            .asObservable()
            .filter(statusCode: 200)
            .map({ response -> GGResult<GGEventsResponse> in
                let json = JSON(response.data)
                print(json)
                if let pageResponse = Mapper<GGPageResponse<GGEvent>>().map(JSONString: json.rawString()!) {
                    if let events = pageResponse.data {
                        let pastEventsCount = pageResponse.total - (pageResponse.offset + pageResponse.limit)
                        return .success(GGEventsResponse(events, pastEventsCount: pastEventsCount < 0 ? 0 : pastEventsCount))
                    }
                }
                return .failure(.mappingError)
            })
            .catchErrorJustReturn(.failure(.networkError))
    }
    
    func getFutureEvents(_ offset: Int) -> Observable<GGResult<GGEventsResponse>> {
        return provider.rx.requestWait(for: 5, .getFutureEvents(offset))
            .asObservable()
            .filter(statusCode: 200)
            .map({ response -> GGResult<GGEventsResponse> in
                let json = JSON(response.data)
                print(json)
                if let pageResponse = Mapper<GGPageResponse<GGEvent>>().map(JSONString: json.rawString()!) {
                    if let events = pageResponse.data {
                        let futureEventsCount = pageResponse.total - (pageResponse.offset + pageResponse.limit)
                        return .success(GGEventsResponse(events, futureEventsCount: futureEventsCount < 0 ? 0 : futureEventsCount))
                    }
                }
                return .failure(.mappingError)
            })
            .catchErrorJustReturn(.failure(.networkError))
    }
    
    func getEventUsers(by eventId: Int) -> Observable<GGResult<[GGUser]>> {
        return provider.rx.request(.getEventUsers(eventId))
            .asObservable()
            .filter(statusCode: 200)
            .map({ response -> GGResult<[GGUser]> in
                let json = JSON(response.data)
                print(json)
                if let responseList = Mapper<GGListResponse<GGUser>>().map(JSONString: json.rawString()!) {
                    if let users = responseList.data {
                        return .success(users)
                    }
                }
                return .failure(.mappingError)
            })
            .catchErrorJustReturn(.failure(.networkError))
    }
    
    func getLocation(by locationId: Int) -> Observable<GGResult<Location>> {
        return provider.rx.request(.getLocation(locationId))
            .asObservable()
            .filter(statusCode: 200)
            .map({ response -> GGResult<Location> in
                let json = JSON(response.data)
                print(json)
                if let responseModel = Mapper<GGResponse<Location>>().map(JSONString: json.rawString()!) {
                    if let location = responseModel.data {
                        print(location.photos.count)
                        return .success(location)
                    }
                }
                return .failure(.mappingError)
            })
            .catchErrorJustReturn(.failure(.networkError))
    }
    
    func addEventUser(to eventId: Int) -> Observable<GGResult<Bool>> {
        return provider.rx.requestWait(for: 1, .addEventUser(eventId))
            .asObservable()
            .filter(statusCode: 200)
            .map({ response -> GGResult<Bool> in
                print((response.request?.httpMethod)!, (response.request?.url)!)
                let json = JSON(response.data)
                print(json)
                if let isSuccess = json["data"].bool {
                    return .success(isSuccess)
                }
              
                return .failure(.mappingError)
            })
            .catchErrorJustReturn(.failure(.networkError))
    }
    
    func removeEventUser(from eventId: Int) -> Observable<GGResult<Bool>> {
        return provider.rx.requestWait(for: 1, .removeEventUser(eventId))
            .asObservable()
            .filter(statusCode: 200)
            .map({ response -> GGResult<Bool> in
                print((response.request?.httpMethod)!, (response.request?.url)!)
                let json = JSON(response.data)
                print(json)
                if let isSuccess = json["data"].bool {
                    return .success(isSuccess)
                }
                
                return .failure(.mappingError)
            })
            .catchErrorJustReturn(.failure(.networkError))
    }
    
    func updateUserInfo(with user: GGUser) -> Observable<GGResult<GGUser>> {
        return provider.rx.request(.updateUserInfo(user))
            .asObservable()
            .filter(statusCode: 200)
            .map({ response -> GGResult<GGUser> in
                let json = JSON(response.data)
                print(json)
                if let responseModel = Mapper<GGResponse<GGUser>>().map(JSONString: json.rawString()!) {
                    if let user = responseModel.data {
                        return .success(user)
                    }
                }
                
                return .failure(.mappingError)
            })
            .catchErrorJustReturn(.failure(.networkError))
    }
    
    func updateUserPhoto(with imageData: Data) -> Observable<GGResult<Photo>> {
        return provider.rx.request(.updateUserPhoto(imageData))
            .asObservable()
            .filter(statusCode: 200)
            .map({ response -> GGResult<Photo> in
                let json = JSON(response.data)
                print(json)
                if let responseModel = Mapper<GGResponse<Photo>>().map(JSONString: json.rawString()!) {
                    if let user = responseModel.data {
                        return .success(user)
                    }
                }
                
                return .failure(.mappingError)
            })
            .catchErrorJustReturn(.failure(.networkError))
    }
    
    func removeUserPhoto() -> Observable<GGResult<Void>> {
        return provider.rx.request(.removeUserPhoto)
            .asObservable()
            .filter(statusCode: 200)
            .map({ response -> GGResult<Void> in
                print("removeUserPhoto success")
                return .success(())
            })
            .catchErrorJustReturn(.failure(.networkError))
    }
    
    func getPhotoBinary(by id: Int) -> Observable<GGResult<PhotoBinary>> {
        return provider.rx.requestWait(for: 1, .getPhotoBinary(id))
            .asObservable()
            .filter(statusCode: 200)
            .map({ response -> GGResult<PhotoBinary> in
                let json = JSON(response.data)
//                print(json)
                if let responseModel = Mapper<GGResponse<PhotoBinary>>().map(JSONString: json.rawString()!) {
                    if let photo = responseModel.data {
                        return .success(photo)
                    }
                }
                return .failure(.mappingError)
            })
            .catchErrorJustReturn(.failure(.networkError))
    }
    
    
}
