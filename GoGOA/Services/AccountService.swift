//
//  AccountService.swift
//  GoGOA
//
//  Created by m.shilo on 03/02/2019.
//  Copyright © 2019 m.shilo. All rights reserved.
//

import Foundation
import RxSwift

class AccountService {
    
    private let loginKey = "login"
    private let passwordKey = "password"
    
    func read() -> Credentials? {
        if let login: String = Utils.readValue(for: self.loginKey) {
            let account = Account(login)
            if let result = account.readFromSecureStore(), let data = result.data, let password = data[self.passwordKey] {
                return Credentials(login: login, password: password as! String)
            }
            return nil
        }
       return nil
    }
    
    func save(_ login: String, _ password: String) {
        Utils.saveValue(login, for: "login")
        
        let account = Account(login, password)
        do {
            try account.createInSecureStore()
        } catch {
            do {
                try account.updateInSecureStore()
            } catch {
                print("Equals login generated!!! Try again")
            }
        }
    }
}

