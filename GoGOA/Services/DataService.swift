//
//  DataService.swift
//  GoGOA
//
//  Created by m.shilo on 28/06/2018.
//  Copyright © 2018 m.shilo. All rights reserved.
//

import Foundation
import RealmSwift
import Realm

struct DataService {

    func saveCurrentUser(_ user: GGUser) {
        let realm = try! Realm()
        Utils.saveValue(user.id, for: "currentUserId")
        try! realm.write {
            realm.add(user, update: .modified)
        }
    }
    
    func fetchCurrentUser() -> GGUser? {
        let realm = try! Realm()
        print(Realm.Configuration.defaultConfiguration.fileURL)
        if let userId: Int = Utils.readValue(for: "currentUserId") {
            let user = realm.object(ofType: GGUser.self, forPrimaryKey: userId)
            return user!
        }
        return nil
    }
    
    func removeCurrentUserPhoto() {
        let realm = try! Realm()
        let userId: Int = Utils.readValue(for: "currentUserId")!
        let user = realm.object(ofType: GGUser.self, forPrimaryKey: userId)
        
        try! realm.write {
            if let photo = user?.photoIcon {
                realm.delete(photo)
            }
            user?.photoIcon = nil
        }
    }
    
    func saveCurrentUserPhoto(_ photo: Photo) {
        let realm = try! Realm()
        let userId: Int = Utils.readValue(for: "currentUserId")!
        let user = realm.object(ofType: GGUser.self, forPrimaryKey: userId)
        
        print(realm.configuration.fileURL)
        
        try! realm.write {
            realm.add(photo, update: .modified)
            user?.photoIcon = photo
        }
    }
    
    func saveCurrentUserPhotoData(_ data: Data) {
        let realm = try! Realm()
        let userId: Int = Utils.readValue(for: "currentUserId")!
        let user = realm.object(ofType: GGUser.self, forPrimaryKey: userId)
        
        try! realm.write {
            user?.photoIcon?.photoBinary?.data = data
            user?.photoIcon?.smallPhotoBinary?.data = data
        }
    }

    func saveEvents(_ events: [GGEvent]) {
        let realm = try! Realm()
        try! realm.write {
            realm.add(events, update: .modified)
        }
    }

    func fetchEvents() -> [GGEvent] {
        let realm = try! Realm()
        let events = Array(realm.objects(GGEvent.self).sorted(byKeyPath: "eventDate", ascending: false))
        return events
    }
    
    func saveLocation(_ location: Location) {
        let realm = try! Realm()
        try! realm.write {
            realm.add(location, update: .modified)
        }
    }
    
    func saveLocationPhotos(_ photos: [Photo], to location: Location) {
        let realm = try! Realm()
        try! realm.write {
            location.photos.removeAll()
        }
        for photo in photos {
            saveLocationPhoto(location, photo)
        }
    }
    
    private func saveLocationPhoto(_ location: Location, _ photo: Photo) {
        if let realm = location.realm {
            if let foundedPhoto = realm.object(ofType: Photo.self, forPrimaryKey: photo.id) {
                try! realm.write {
                    location.photos.append(foundedPhoto)
                }
            } else {
                try! realm.write {
                    let createdPhoto = realm.create(Photo.self, value: photo, update: .modified)
                    location.photos.append(createdPhoto)
                }
            }
        }
    }

    func savePhotoBinaryData(_ data: Data, _ photoBinaryId: Int) {
        let realm = try! Realm()
//        print("save \(String(describing: realm.configuration.fileURL))")
        let photoBinary = realm.object(ofType: PhotoBinary.self, forPrimaryKey: photoBinaryId)!
        try! realm.write {
            photoBinary.data = data
        }
    }

    func fetchPhotoBinaryData(_ photoBinaryId: Int) -> Data? {
        let realm = try! Realm()
        let photoBinary = realm.object(ofType: PhotoBinary.self, forPrimaryKey: photoBinaryId)
        return photoBinary?.data
    }
    
    
    //EVENT USERS
    
    func saveEventUsers(_ users: [GGUser], to event: GGEvent) -> [GGUser] {
        let realm = try! Realm()
        try! realm.write {
            event.users.removeAll()
        }
        for user in users {
            saveEventUser(event, user)
        }
        return Array(event.users)
    }
    
    func addEventUser(_ user: GGUser, to event: GGEvent) {
        saveEventUser(event, user)
    }
    
    private func saveEventUser(_ event: GGEvent, _ user: GGUser) {
        if let realm = event.realm {
            if let foundedUser = realm.object(ofType: GGUser.self, forPrimaryKey: user.id) {
                try! realm.write {
                    event.users.append(foundedUser)
                }
            } else {
                try! realm.write {
                    let createdUser = realm.create(GGUser.self, value: user, update: .modified)
                    event.users.append(createdUser)
                }
            }
        }
    }
    
    func removeEventUser(_ user: GGUser, from event: GGEvent) {
        let realm = try! Realm()
        if let index = event.users.firstIndex(of: user) {
            try! realm.write {
                event.users.remove(at: index)
            }
        }
    }
    
    func fetchEventUsers(_ eventId: Int) -> [GGUser] {
        let realm = try! Realm()
        if let event = realm.object(ofType: GGEvent.self, forPrimaryKey: eventId) {
            return Array(event.users)
        }
        return []
    }
    
    func removeAllData() {
        let realm = try! Realm()
        try! realm.write {
            realm.deleteAll()
        }
    }



//    @TODO удалять локации ,которые не пришли в ответе
//    static func saveLocations(_ locations: [Location]) {
//        let realm = try! Realm()
//        try! realm.write {
//            realm.add(locations, update: true)
//        }
//    }
//
//    static func fetchLocations() -> [Location] {
//        let realm = try! Realm()
//        let locations = Array(realm.objects(Location.self))
//        return locations
//    }
//
//    static func clearLocationPhotoIcons(_ locationId: Int) {
//        let realm = try! Realm()
//        let location = realm.object(ofType: Location.self, forPrimaryKey: locationId)!
//
//        var photos = realm.objects(Photo.self)
//        print(photos.count)
//        print(location.photoIcons.count)
//
//        for photoIcon in location.photoIcons {
//            try! realm.write {
//                realm.delete(realm.object(ofType: Photo.self, forPrimaryKey: photoIcon.id)!)
//            }
//        }
//
//        print(photos.count)
//        print(location.photoIcons.count)
////        try! realm.write {
////            realm.delete(photos)
////        }
//    }
//
//    static func saveLocationPhotoIcons(_ photoIcons: [Photo], _ locationId: Int) {
//        let realm = try! Realm()
//        print("save \(realm.configuration.fileURL)")
//        let location = realm.object(ofType: Location.self, forPrimaryKey: locationId)!
//        try! realm.write {
//            realm.add(photoIcons, update: true)
//            location.photoIcons.append(objectsIn: photoIcons)
//        }
//        print(location.photoIcons.count)
//    }
}
