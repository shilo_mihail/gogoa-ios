//
//  PhotoAssetService.swift
//  GoGOA
//
//  Created by m.shilo on 18/10/2019.
//  Copyright © 2019 m.shilo. All rights reserved.
//

import Photos
import RxSwift

struct PhotoAssetService {
    
    func fetchPhotoAssets() -> Observable<[PHAsset]> {
        return Observable.create({ observer -> Disposable in
            PHPhotoLibrary.requestAuthorization { (status) in
                switch status {
                case .authorized:
                    print("Request photo success")
                    let fetchOptions = PHFetchOptions()
                    let fetchResult = PHAsset.fetchAssets(with: .image, options: fetchOptions)
                    let count = fetchResult.count
                    let assets = fetchResult.objects(at: IndexSet(integersIn: 0...count - 1))
                    observer.onNext(assets)
                    observer.onCompleted()
                default:
                    print("Request photo failure")
                    observer.onError(AppError.photoGalleryAuthError)
                }
            }
            return Disposables.create()
        })
    }
    
    func fetchImage(_ asset: PHAsset, _ targetSize: CGSize) -> Observable<UIImage?> {
        return Observable.create({ observer -> Disposable in
            let options = PHImageRequestOptions()
            options.version = .original
            PHImageManager.default().requestImage(for: asset, targetSize: targetSize, contentMode: .aspectFit, options: options) { image, _ in
                observer.onNext(image)
                observer.onCompleted()
            }
            return Disposables.create()
        })
    }
    
    func fetchImageData(_ asset: PHAsset) -> Observable<Data?> {
            return Observable.create({ observer -> Disposable in
                let options = PHImageRequestOptions()
                options.version = .original
                PHImageManager.default().requestImageData(for: asset, options: options) { (data, _, _, _) in
                    observer.onNext(data)
                    observer.onCompleted()
                }
                return Disposables.create()
            })
        }
}
