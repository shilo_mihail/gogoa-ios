//
//  GGShimmerView+Rx.swift
//  GoGOA
//
//  Created by m.shilo on 29.07.2020.
//  Copyright © 2020 m.shilo. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

extension Reactive where Base: GGShimmerBlurView {
    
    var loading: Binder<Bool> {
        return Binder(self.base) { view, loading -> Void in
            loading ? view.startAnimating() : view.stopAnimating()
        }
    }
}
