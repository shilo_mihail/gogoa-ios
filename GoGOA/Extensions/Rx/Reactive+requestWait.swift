//
//  Reactive+requestWait.swift
//  GoGOA
//
//  Created by m.shilo on 28.07.2020.
//  Copyright © 2020 m.shilo. All rights reserved.
//

import Foundation
import RxSwift
//#if !COCOAPODS
import Moya
//#endif


public extension Reactive where Base: MoyaProviderType {

    /// Designated request-making method.
    ///
    /// - Parameters:
    ///   - seconds: seconds count.
    ///   - token: Entity, which provides specifications necessary for a `MoyaProvider`.
    ///   - callbackQueue: Callback queue. If nil - queue from provider initializer will be used.
    /// - Returns: Single response object.
    func requestWait(for seconds: Int, _ token: Base.Target, callbackQueue: DispatchQueue? = nil) -> Single<Moya.Response> {
        let wait = Single.just(()).delay(RxTimeInterval.seconds(seconds), scheduler: MainScheduler.instance)
        let request = Single<Moya.Response>.create { [weak base] single in
            let cancellableToken = base?.request(token, callbackQueue: callbackQueue, progress: nil) { result in
                switch result {
                case let .success(response):
                    print("URL: \(response.request?.url) \(response.request?.httpMethod)\n \(response.request?.allHTTPHeaderFields)\n\(response.request?.httpBody)")
                    single(.success(response))
                case let .failure(error):
                    single(.error(error))
                }
            }

            return Disposables.create {
                cancellableToken?.cancel()
            }
        }
                
        return Single.zip(wait, request)
            .flatMap({ (_, result) -> Single<Moya.Response> in
                return Single.just(result)
            })
    }
}
