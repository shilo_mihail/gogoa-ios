//
//  GGActionSheetView+Rx.swift
//  GoGOA
//
//  Created by m.shilo on 09/10/2019.
//  Copyright © 2019 m.shilo. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

extension Reactive where Base: GGActionSheetView  {
    
    var isUserPhotoExist: Binder<Bool> {
        return Binder<Bool>(self.base) { view, isUserPhotoExist in
            if !isUserPhotoExist {
                view.stackViewHeightConstraint.constant /= 3
                view.contentViewTopConstraint.constant += view.stackViewHeightConstraint.constant * 2
                view.viewPhotoView.isHidden = true
                view.removeView.isHidden = true
            }
        }
    }
    
    var viewModel: Binder<GGActionSheetViewViewModel> {
        return Binder<GGActionSheetViewViewModel>(self.base) { view, viewModel in
            view.viewModel = viewModel
        }
    }
}
