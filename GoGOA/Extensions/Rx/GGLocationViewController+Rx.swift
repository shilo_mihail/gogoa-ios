//
//  LocationViewController+Rx.swift
//  GoGOA
//
//  Created by m.shilo on 09.08.2020.
//  Copyright © 2020 m.shilo. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import GoogleMaps

extension Reactive where Base: GGLocationViewController {
    
    var marker: Binder<Location> {
        return Binder(self.base) { controller, location -> Void in
            let position = CLLocationCoordinate2D(latitude: location.latitude, longitude: location.longitude)
            let marker = GGMarker(position: position)
            marker.title = location.title
            marker.isDraggable = false
            marker.map = controller.mapView
            marker.userData = location
            
            let markerImage = Constants.icon.location.withRenderingMode(.alwaysTemplate)
            let markerIconView = UIImageView(image: markerImage)
            markerIconView.tintColor = .gray
            
            let container = UIView(frame: CGRect(origin: .zero, size: markerIconView.bounds.size))
            container.addSubview(markerIconView)
            marker.iconView = container
            
            controller.marker = marker
        }
    }
    
    var markerDisabled: Binder<Bool> {
        return Binder(self.base) { controller, loading -> Void in
            if let marker = controller.marker, let container = marker.iconView, let imageView = container.subviews.first as? UIImageView {
                
                marker.isDataLoading = loading
                UIView.animate(withDuration: TimeInterval(UINavigationController.hideShowBarDuration)) {
                    imageView.tintAdjustmentMode = .dimmed
                }
                imageView.tintAdjustmentMode = .normal
                imageView.tintColor = loading ? .gray : .red
            }
        }
    }
    
    var infoViewPosition: Binder<GGLocationInfoPosition> {
        return Binder(self.base) { controller, infoViewPosition -> Void in
            UIView.animate(withDuration: TimeInterval(10), animations: {
                switch infoViewPosition {
                    case .middle(let height):
                        let tabBarHeight = controller.tabBarController!.tabBar.frame.size.height
                        controller.locationInfoViewTopConstraint.constant = -height! - tabBarHeight
                        controller.locationInfoViewHeightConstraint.constant = height!
                        controller.backgroundView.alpha = 0
                    case .none:
                        controller.locationInfoViewTopConstraint.constant = 0
//                        controller.locationInfoViewHeightConstraint.constant = 0
                        controller.backgroundView.alpha = 0
                    case .full(let gap):
                        let height = controller.view.bounds.height - controller.statusBarHeight() - controller.navigationBarHeight() - gap!
                        controller.locationInfoViewTopConstraint.constant = -height
                        controller.locationInfoViewHeightConstraint.constant = height
                        controller.backgroundView.alpha = 0.4
                }
                controller.view.layoutIfNeeded()
            }) { _ in
                if infoViewPosition == .none {
                    controller.locationInfoView.resetDisposeBag()
                }
            }
        }
    }
}
