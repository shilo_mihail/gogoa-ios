//
//  UIButton+Rx.swift
//  GoGOA
//
//  Created by m.shilo on 05/02/2019.
//  Copyright © 2019 m.shilo. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

extension Reactive where Base: UIButton {

    var title: Binder<String> {
        return Binder(self.base) { button, title -> Void in
            button.setTitle(title, for: .normal)
        }
    }
}
