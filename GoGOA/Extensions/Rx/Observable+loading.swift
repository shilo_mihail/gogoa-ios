//
//  Observable+loading.swift
//  GoGOA
//
//  Created by m.shilo on 28.07.2020.
//  Copyright © 2020 m.shilo. All rights reserved.
//

import Foundation
import RxSwift

extension Observable where Element: Any {
    func startLoading<T: Observable<Bool> & SubjectType & ObserverType>(subject: T) -> Observable<Element> {
        return self.do(onNext: { _ in
            subject.onNext(true)
        })
    }
    
    func stopLoading<T: Observable<Bool> & SubjectType & ObserverType>(subject: T) -> Observable<Element> {
        return self.do(onNext: { _ in
            subject.onNext(false)
        })
    }
}
