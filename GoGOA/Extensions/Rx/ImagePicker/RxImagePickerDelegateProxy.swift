//
//  RxImagePickerDelegateProxy.swift
//  GoGOA
//
//  Created by m.shilo on 15/10/2019.
//  Copyright © 2019 m.shilo. All rights reserved.
//

import RxSwift
import RxCocoa
import UIKit

open class RxImagePickerDelegateProxy: RxNavigationControllerDelegateProxy, UIImagePickerControllerDelegate {

    public init(imagePicker: UIImagePickerController) {
        super.init(navigationController: imagePicker)
    }
}
