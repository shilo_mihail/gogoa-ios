////
////  GMSMapView+Rx.swift
////  GoGOA
////
////  Created by m.shilo on 07/02/2019.
////  Copyright © 2019 m.shilo. All rights reserved.
////
//
//import Foundation
//import GoogleMaps
//import RxSwift
//import RxCocoa
//
//extension GMSMapView: HasDelegate {
//    public typealias Delegate = GMSMapViewDelegate
//}
//
//class RxGMSMapViewDelegateProxy: DelegateProxy<GMSMapView, GMSMapViewDelegate>, DelegateProxyType, GMSMapViewDelegate {
//    
//    public init(mapView: GMSMapView) {
//        super.init(parentObject: mapView, delegateProxy: RxGMSMapViewDelegateProxy.self)
//    }
//    
//    static func registerKnownImplementations() {
//        self.register { RxGMSMapViewDelegateProxy(mapView: $0) }
//    }
//    
//    internal lazy var didTapSubject = PublishSubject<GMSMarker>()
//    internal lazy var didTapAtSubject = PublishSubject<CLLocationCoordinate2D>()
//    
//    public func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
//        _ = _forwardToDelegate?.mapView?(mapView, didTap: marker)
//        didTapSubject.onNext(marker)
//        return true
//    }
//    
//    public func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
//        _forwardToDelegate?.mapView?(mapView, didTapAt: coordinate)
//        didTapAtSubject.onNext(coordinate)
//    }
//    
//    deinit {
//        self.didTapSubject.on(.completed)
//        self.didTapAtSubject.on(.completed)
//    }
//}
//
//extension Reactive where Base: GMSMapView {
//    var delegate: DelegateProxy<GMSMapView, GMSMapViewDelegate> {
//        return RxGMSMapViewDelegateProxy.proxy(for: base)
//    }
//    
//    var didTap: Observable<GMSMarker> {
//        return RxGMSMapViewDelegateProxy.proxy(for: base).didTapSubject.asObservable()
//    }
//    
//    var didTapAt: Observable<CLLocationCoordinate2D> {
//        return RxGMSMapViewDelegateProxy.proxy(for: base).didTapAtSubject.asObservable()
//    }
//    
//}
