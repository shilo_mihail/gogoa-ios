//
//  CosmosView+Rx.swift
//  GoGOA
//
//  Created by m.shilo on 07/02/2019.
//  Copyright © 2019 m.shilo. All rights reserved.
//

import RxSwift
import RxCocoa
import Cosmos

extension Reactive where Base: CosmosView  {
    
    var rating: Binder<Int> {
        return Binder<Int>(self.base) { view, rating in
            view.rating = Double(rating)
        }
    }
}
