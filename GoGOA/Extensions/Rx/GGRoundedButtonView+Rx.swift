//
//  GGRoundedButtonView+Rx.swift
//  GoGOA
//
//  Created by m.shilo on 10/04/2019.
//  Copyright © 2019 m.shilo. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

extension Reactive where Base: GGRoundedButtonView {
    
    var isActive: Binder<Bool> {
        return Binder(self.base) { view, isActive -> Void in
            view.tap.isEnabled = isActive
            UIView.animate(withDuration: 0.3, animations: {
                view.alpha = isActive ?  1 : 0.6
            })
        }
    }
    
    var loading: Binder<Bool> {
        return Binder(self.base) { view, loading -> Void in
            view.tap.isEnabled = !loading
            view.label.isHidden = loading
            loading ? view.activityIndicator.startAnimating() : view.activityIndicator.stopAnimating()
        }
    }
    
    var title: Binder<String> {
        return Binder(self.base) { view, title -> Void in
            view.label.text = title
        }
    }
}
