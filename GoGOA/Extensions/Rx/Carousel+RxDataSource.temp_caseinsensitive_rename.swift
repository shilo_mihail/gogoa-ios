//
//  Carousel+RxDatasource.swift
//  GoGOA
//
//  Created by m.shilo on 09/02/2019.
//  Copyright © 2019 m.shilo. All rights reserved.
//

import Foundation
import iCarousel
import RxSwift
import RxCocoa

extension iCarousel: HasDataSource {
    public typealias DataSource = iCarouselDataSource
}

class iCarouselDelegateProxy: DelegateProxy<iCarousel, iCarouselDelegate>, DelegateProxyType, iCarouselDelegate {
    
    public init(carousel: iCarousel) {
        super.init(parentObject: carousel, delegateProxy: iCarouselDelegateProxy.self)
    }
    
    static func registerKnownImplementations() {
        self.register { iCarouselDelegateProxy(carousel: $0) }
    }
    
    internal lazy var viewForItemAtSubject = PublishSubject<Int>()
    
    public func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView {
        viewForItemAtSubject.onNext(index)
        return _forwardToDelegate?.carousel?(carousel, viewForItemAt: index, reusing: view) ?? UIView()
    }
    
    deinit {
        self.viewForItemAtSubject.on(.completed)
    }
}

extension Reactive where Base: iCarousel {
    var delegate: DelegateProxy<iCarousel, iCarouselDelegate> {
        return iCarouselDelegateProxy.proxy(for: base)
    }
    
    var viewForItemAt: Observable<Int> {
        return iCarouselDelegateProxy.proxy(for: base).viewForItemAtSubject.asObservable()
    }
    
}
