//
//  OverlappingImageList+Rx.swift
//  GoGOA
//
//  Created by m.shilo on 26/07/2019.
//  Copyright © 2019 m.shilo. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

extension Reactive where Base: UsersImageList {
    
    var users: Binder<[GGUser]> {
        return Binder(self.base) { view, users -> Void in
            view.showUserViewsAndText(users)
        }
    }
}
