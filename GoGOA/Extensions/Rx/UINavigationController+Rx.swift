//
//  UINavigationController+Rx.swift
//  GoGOA
//
//  Created by m.shilo on 09/04/2019.
//  Copyright © 2019 m.shilo. All rights reserved.
//

//import UIKit
//import RxSwift
//import RxCocoa
//
//
//class RxUINavigationControllerDelegateProxy: DelegateProxy<UINavigationController, UINavigationControllerDelegate>, DelegateProxyType, UINavigationControllerDelegate {
//    
//    public init(navigationController: UINavigationController) {
//        super.init(parentObject: navigationController, delegateProxy: RxUINavigationControllerDelegateProxy.self)
//    }
//    
//    static func registerKnownImplementations() {
//        self.register { RxUINavigationControllerDelegateProxy(navigationController: $0) }
//    }
//    
//    internal lazy var animationControllerForSubject = PublishSubject<UINavigationController.Operation>()
//    
//    public func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationController.Operation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
//        animationControllerForSubject.onNext(operation)
//        return _forwardToDelegate?.navigationController(navigationController, animationControllerFor: operation, from: fromVC, to: toVC)
//    }
//    
//    
//    deinit {
//        self.animationControllerForSubject.on(.completed)
//    }
//}
//
//extension Reactive where Base: UINavigationController {
//    var delegate: DelegateProxy<UINavigationController, UINavigationControllerDelegate> {
//        return RxUINavigationControllerDelegateProxy.proxy(for: base)
//    }
//    
//    var animationControllerFor: Observable<UINavigationController.Operation> {
//        return RxUINavigationControllerDelegateProxy.proxy(for: base).animationControllerForSubject.asObservable()
//    }
//    
//}
