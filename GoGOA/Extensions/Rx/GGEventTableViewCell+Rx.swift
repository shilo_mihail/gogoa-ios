//
//  GGEventTableViewCell+Rx.swift
//  GoGOA
//
//  Created by m.shilo on 29.07.2020.
//  Copyright © 2020 m.shilo. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import FontAwesome_swift

extension Reactive where Base: EventTableViewCell {
    
    var event: Binder<GGEvent?> {
        return Binder(self.base) { view, optionalEvent -> Void in
            
            if let event = optionalEvent {
                view.titleLabel.text = event.title
                view.titleLabel.backgroundColor = UIColor(white: 1, alpha: 0)
                
                if event.price == 0 {
                    view.priceLabel.text = "free"
                } else {
                    let priceString = NSMutableAttributedString(string: String(event.price) + " " + String.fontAwesomeIcon(name: .rupeeSign), attributes: [.font : UIFont.systemFont(ofSize: 17)])
                    priceString.addAttribute(.font, value: UIFont.fontAwesome(ofSize: 15, style: FontAwesomeStyle.solid), range: NSRange(location:String(event.price).count + 1, length: 1))
                    view.priceLabel.attributedText = priceString
                }
                view.priceLabel.backgroundColor = UIColor(white: 1, alpha: 0)
                
                if let photoBinaryData = event.photoIcon?.photoBinary?.data, !photoBinaryData.isEmpty {
                    view.iconImageView.image = UIImage(data: photoBinaryData)
                    view.iconImageView.alpha = 1
                } else if let photoIconData = event.photoIcon?.smallPhotoBinary?.data {
                    view.iconImageView.image = UIImage(data: photoIconData)
                }

                let dateString = NSMutableAttributedString(string: String.fontAwesomeIcon(name: .clock) + " " + Utils.generateHumanDateString(event.eventDate), attributes: [.font : UIFont.systemFont(ofSize: 17)])
                dateString.addAttribute(.font, value: UIFont.fontAwesome(ofSize: 17, style: FontAwesomeStyle.regular), range: NSRange(location:0,length:1))
                view.dateLabel.attributedText = dateString
                view.dateLabel.backgroundColor = UIColor(white: 1, alpha: 0)
                
                let peopleCountString = NSMutableAttributedString(string: String.fontAwesomeIcon(name: .userFriends) + " " + String(event.usersCount) + " joined", attributes: [.font : UIFont.systemFont(ofSize: 17)])
                peopleCountString.addAttribute(.font, value: UIFont.fontAwesome(ofSize: 17, style: FontAwesomeStyle.solid), range: NSRange(location:0,length:1))
                view.peopleCountLabel.attributedText = peopleCountString
                view.peopleCountLabel.backgroundColor = UIColor(white: 1, alpha: 0)
                
                view.locationButton.setTitle("", for: .normal)
                if let locationTitle = event.location?.title {
                    let locationString = NSMutableAttributedString(string: String.fontAwesomeIcon(name: .locationArrow) + " " + locationTitle, attributes: [.font : UIFont.systemFont(ofSize: 17)])
                    locationString.addAttribute(.font, value: UIFont.fontAwesome(ofSize: 17, style: FontAwesomeStyle.solid), range: NSRange(location: 0,length: 1))
                    view.locationButton.setAttributedTitle(locationString, for: .normal)
                    view.locationButton.isHidden = false
                } else {
                    view.locationButton.isHidden = true
                }
                view.locationButton.backgroundColor = UIColor(white: 1, alpha: 0)
                
                view.topShimmerBlurView.stopAnimating()
                view.bottomShimmerBlurView.stopAnimating()
                
            } else {
                view.topShimmerBlurView.startAnimating()
                view.imageShimmerBlurView.startAnimating()
                view.bottomShimmerBlurView.startAnimating()
                
                view.titleLabel.text = ""
                view.titleLabel.backgroundColor = UIColor(white: 1, alpha: 0.2)
                view.titleLabel.layer.cornerRadius = 7
                view.titleLabel.clipsToBounds = true
                
                view.priceLabel.text = ""
                view.priceLabel.backgroundColor = UIColor(white: 1, alpha: 0.2)
                view.priceLabel.layer.cornerRadius = 7
                view.priceLabel.clipsToBounds = true
                
                view.iconImageView.alpha = 0.5
                view.iconImageView.image = nil
                
                view.peopleCountLabel.text = ""
                view.peopleCountLabel.backgroundColor = UIColor(white: 1, alpha: 0.2)
                view.peopleCountLabel.layer.cornerRadius = 7
                view.peopleCountLabel.clipsToBounds = true
                
                view.dateLabel.text = ""
                view.dateLabel.backgroundColor = UIColor(white: 1, alpha: 0.2)
                view.dateLabel.layer.cornerRadius = 7
                view.dateLabel.clipsToBounds = true
                
                view.locationButton.isHidden = false
                view.locationButton.setTitle("", for: .normal)
                view.locationButton.setAttributedTitle(NSMutableAttributedString(), for: .normal)
                view.locationButton.backgroundColor = UIColor(white: 1, alpha: 0.2)
                view.locationButton.layer.cornerRadius = 7
                view.locationButton.clipsToBounds = true
            }
        }
    }
}
