//
//  GGLocationInfoView+Rx.swift
//  GoGOA
//
//  Created by m.shilo on 20.08.2020.
//  Copyright © 2020 m.shilo. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

extension Reactive where Base: GGLocationInfoView  {
    
    var viewModel: Binder<GGLocationInfoViewViewModel> {
        return Binder<GGLocationInfoViewViewModel>(self.base) { view, viewModel in
            view.viewModel = viewModel
        }
    }
}
