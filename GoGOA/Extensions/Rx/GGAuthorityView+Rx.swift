//
//  GGRolableView+Rx.swift
//  GoGOA
//
//  Created by m.shilo on 06.07.2020.
//  Copyright © 2020 m.shilo. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

extension Reactive where Base: GGAuthorityView {
    
    var isAllowed: Binder<Bool> {
        return Binder(self.base) { view, isAllowed -> Void in
            if !isAllowed {
                view.removeFromSuperview()
            }
        }
    }
}
