//
//  UITextField+isWarning.swift
//  GoGOA
//
//  Created by m.shilo on 01/02/2019.
//  Copyright © 2019 m.shilo. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

extension Reactive where Base: GGLineTextField  {
    
    var isWarning: Binder<Bool> {
        return Binder<Bool>(self.base) { view, isWarning in
            isWarning ? view.setColor(.warning) : view.setColor(.login)
        }
    }
}
