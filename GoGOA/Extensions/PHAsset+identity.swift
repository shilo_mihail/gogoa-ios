//
//  PHAsset+.swift
//  GoGOA
//
//  Created by m.shilo on 16/10/2019.
//  Copyright © 2019 m.shilo. All rights reserved.
//

import Photos
import RxDataSources

extension PHAsset: IdentifiableType {
    public var identity: Int {
        return localIdentifier.hashValue
    }
}
