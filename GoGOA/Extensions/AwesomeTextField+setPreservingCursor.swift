//
//  AwesomeTextField+setPreservingCursor.swift
//  GoGOA
//
//  Created by m.shilo on 04/02/2019.
//  Copyright © 2019 m.shilo. All rights reserved.
//

import UIKit

extension AwesomeTextField {
    
    //make unereased prefix as code
    func setPreservingCursor() -> (_ newText: String?) -> Void {
        return {
            newText in
            let cursorPosition = self.offset(from: self.beginningOfDocument, to: self.selectedTextRange!.start) + newText!.count - (self.text?.count ?? 0)
            if cursorPosition <= Constants.mobilePhoneCode.count {
                self.text = Constants.mobilePhoneCode
                self.position(from: self.beginningOfDocument, offset: cursorPosition)
            }
        }
    }
}
