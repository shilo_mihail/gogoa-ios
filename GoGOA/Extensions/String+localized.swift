//
//  String+localized.swift
//  GoGOA
//
//  Created by m.shilo on 12/09/2019.
//  Copyright © 2019 m.shilo. All rights reserved.
//

import Foundation

extension String {
    
    var localized: String {
        return NSLocalizedString(self, comment: "")
    }
}
