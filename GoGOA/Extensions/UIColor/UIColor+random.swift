//
//  UIColor+random.swift
//  GoGOA
//
//  Created by m.shilo on 26/07/2019.
//  Copyright © 2019 m.shilo. All rights reserved.
//

import UIKit

extension UIColor {
    static func random() -> UIColor {
        return UIColor(red:   .random(),
                       green: .random(),
                       blue:  .random(),
                       alpha: 1.0)
    }
}
