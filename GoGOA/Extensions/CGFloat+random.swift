//
//  CGFloat+random.swift
//  GoGOA
//
//  Created by m.shilo on 26/07/2019.
//  Copyright © 2019 m.shilo. All rights reserved.
//

import UIKit

extension CGFloat {
    static func random() -> CGFloat {
        return CGFloat(arc4random()) / CGFloat(UInt32.max)
    }
}
