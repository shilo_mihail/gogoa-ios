//
//  UIViewController+hideKeyboardWhenTappedAround.swift
//  GoGOA
//
//  Created by m.shilo on 02/02/2019.
//  Copyright © 2019 m.shilo. All rights reserved.
//

import UIKit

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
