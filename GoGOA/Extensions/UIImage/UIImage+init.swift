//
//  UIImage+imageWithColor.swift
//  GoGOA
//
//  Created by m.shilo on 24/07/2019.
//  Copyright © 2019 m.shilo. All rights reserved.
//

import UIKit

extension UIImage {
    
    public convenience init?(_ color: UIColor) {
        let size: CGSize = CGSize(width: 1, height: 1)
        let rect = CGRect(origin: .zero, size: size)
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)
        color.setFill()
        UIRectFill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        guard let cgImage = image?.cgImage else { return nil }
        self.init(cgImage: cgImage)
    }
    
}
