//
//  MobilePhoneTextField.swift
//  GoGOA
//
//  Created by m.shilo on 02/02/2019.
//  Copyright © 2019 m.shilo. All rights reserved.
//

import UIKit

class GGMobilePhoneTextField: GGLineTextField {
    
    override func draw(_ rect: CGRect) {
//        underLineColor = Constants.color.textField.unselectedColorItem
//        selectedUnderLineColorColor = Constants.color.textField.selectedUnderlineColor
//
//        placeholderTextColor = .gray
//        selectedPlaceholderTextColor = Constants.color.textField.unselectedColorItem
//        
//        underLineWidth = 1
//        underLineAlphaBefore = 1
//        underLineAlphaAfter = 1
        
        super.draw(rect)
        
//        if subviews.count > 0, let label = subviews[0] as? UILabel {
//            placeholderLabel.textAlignment = label.textAlignment
//        }
        
        let _inputAccessoryView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 1))
        _inputAccessoryView.backgroundColor = .lightGray
        inputAccessoryView = _inputAccessoryView
        
    }
}
