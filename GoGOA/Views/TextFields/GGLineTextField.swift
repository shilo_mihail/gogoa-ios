//
//  LineTextField.swift
//  birch-ios
//
//  Created by m.shilo on 04/10/2018.
//  Copyright © 2018 m.shilo. All rights reserved.
//

import UIKit
import SwiftMaskTextfield

class GGLineTextField: SwiftMaskTextfield {
    
    enum Color {
        
        case login
        
        case warning
    }
    
    enum LinePosition {
        case top
        case bottom
    }
    
    private var lineView: UIView = UIView()

    override func draw(_ rect: CGRect) {
        self.borderStyle = .none
        addLine(position: .bottom)
    }
    
    func setColor(_ color: Color) {
        switch color {
        case .login:
            lineView.backgroundColor = Constants.color.loginGroup.dark
            textColor = Constants.color.loginGroup.dark
            
        case .warning:
            lineView.backgroundColor = Constants.color.textField.warningBorderColor
        }
    }
    
    private func addLine(position : LinePosition) {
//        lineView.backgroundColor = Constants.color.textField.nonActiveItemColor
        lineView.translatesAutoresizingMaskIntoConstraints = false // This is important!
        self.addSubview(lineView)
        
        let metrics = ["width" : 1.5]
        let views = ["lineView" : lineView]
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[lineView]|", options:NSLayoutConstraint.FormatOptions(rawValue: 0), metrics:metrics, views:views as [String : Any]))
        
        switch position {
        case .top:
            self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[lineView(width)]", options:NSLayoutConstraint.FormatOptions(rawValue: 0), metrics:metrics, views:views as [String : Any]))
        case .bottom:
            self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[lineView(width)]|", options:NSLayoutConstraint.FormatOptions(rawValue: 0), metrics:metrics, views:views as [String : Any]))
        }
    }

}
