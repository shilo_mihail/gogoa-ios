//
//  GGSwipableLine.swift
//  GoGOA
//
//  Created by m.shilo on 20.08.2020.
//  Copyright © 2020 m.shilo. All rights reserved.
//

import UIKit

class GGPanableLine: UIView {
    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var lineView: UIVisualEffectView! {
        didSet {
            lineView.layer.cornerRadius = lineView.bounds.height / 2
            lineView.clipsToBounds = true
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    var tap = UITapGestureRecognizer()
    var pan = UIPanGestureRecognizer()
    
    func commonInit() {
        Bundle.main.loadNibNamed(String(describing: GGPanableLine.self), owner: self, options: [:])
        
        addSubview(contentView)
        
        contentView.translatesAutoresizingMaskIntoConstraints = false
        contentView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        contentView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        contentView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        contentView.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        
        addGestureRecognizer(tap)
        addGestureRecognizer(pan)
    }
}
