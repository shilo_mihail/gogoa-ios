//
//  GradientView.swift
//  Directory
//
//  Created by Nikita on 12/01/2018.
//  Copyright © 2018 shilomv. All rights reserved.
//

import UIKit

class GGGradientView: GGAuthorityView {

    @IBInspectable var startColor: UIColor?
    @IBInspectable var finishColor: UIColor?
    @IBInspectable var directionAdapter: Int {
        get {
            return self.direction.rawValue
        }
        set(directionIndex) {
            self.direction = Direction(rawValue: directionIndex) ?? .fromTopToBottom
        }
    }
    var direction: Direction = .fromTopToBottom
    
    enum Direction: Int {
        case fromTopToBottom = 0
        case fromLeftToRight = 1
        case fromTopLeftToBottomRight = 2
        case fromBottomLeftToTopRight = 3
    }
    
    
    override func draw(_ rect: CGRect) {
        guard let context = UIGraphicsGetCurrentContext() else {
            return
        }
        if startColor == nil || finishColor == nil {
            return
        }
        
        let colors = [startColor!.cgColor, finishColor!.cgColor]
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let colorLocations: [CGFloat] = [0.0, 1.0]
        let gradient = CGGradient(colorsSpace: colorSpace,
                                  colors: colors as CFArray,
                                  locations: colorLocations)!
        
        var startPoint = CGPoint.zero
        var endPoint = CGPoint.zero
        
        switch direction {
        case .fromTopToBottom:
            endPoint = CGPoint(x: 0, y: bounds.height)
        case .fromLeftToRight:
            endPoint = CGPoint(x: bounds.width, y: 0)
        case .fromTopLeftToBottomRight:
            endPoint = CGPoint(x: bounds.width, y: bounds.height)
        case .fromBottomLeftToTopRight:
            startPoint = CGPoint(x: 0, y: bounds.height)
            endPoint = CGPoint(x: bounds.width, y: 0)
        }
        
        context.drawLinearGradient(gradient,
                                   start: startPoint,
                                   end: endPoint,
                                   options: [])
    }

}
