//
//  ClickableView.swift
//  GoGOA
//
//  Created by m.shilo on 12/09/2019.
//  Copyright © 2019 m.shilo. All rights reserved.
//

import UIKit

class ClickableView: UIView {
    var tap: UITapGestureRecognizer!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit() {
        tap = UITapGestureRecognizer(target: self, action: nil)
        addGestureRecognizer(tap)
    }
}
