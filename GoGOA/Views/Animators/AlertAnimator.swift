//
//  ErrorAnimator.swift
//  GoGOA
//
//  Created by m.shilo on 13/09/2019.
//  Copyright © 2019 m.shilo. All rights reserved.
//

//import UIKit
//
//class AlertAnimator: NSObject, UIViewControllerAnimatedTransitioning {
//
//    var duration: TimeInterval
//    var isPresenting: Bool
//    var originFrame: CGRect
//
//    init(duration : TimeInterval, isPresenting : Bool, originFrame : CGRect) {
//        self.duration = duration
//        self.isPresenting = isPresenting
//        self.originFrame = originFrame
//    }
//
//    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
//        return duration
//    }
//
//    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
//        let container = transitionContext.containerView
//
//        guard let fromView = transitionContext.view(forKey: UITransitionContextViewKey.from) else { return }
//        guard let toView = transitionContext.view(forKey: UITransitionContextViewKey.to) else { return }
//
//        self.isPresenting ? container.addSubview(toView) : container.insertSubview(toView, belowSubview: fromView)
//
//        let currentView = isPresenting ? toView : fromView
//
//        toView.frame = isPresenting ?  CGRect(x: fromView.frame.width, y: 0, width: toView.frame.width, height: toView.frame.height) : toView.frame
//        toView.alpha = isPresenting ? 0 : 1
//        toView.layoutIfNeeded()
//
//        UIView.animate(withDuration: duration, animations: {
//            if self.isPresenting {
//                currentView.frame = fromView.frame
//                currentView.alpha = 1
//            } else {
//                currentView.frame = CGRect(x: toView.frame.width, y: 0, width: toView.frame.width, height: toView.frame.height)
//                currentView.alpha = 0
//            }
//        }, completion: { (finished) in
//            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
//        })
//    }
//
//}
