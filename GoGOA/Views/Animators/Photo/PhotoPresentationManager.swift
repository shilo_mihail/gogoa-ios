//
//  PhotoPresentationManager.swift
//  GoGOA
//
//  Created by m.shilo on 11/10/2019.
//  Copyright © 2019 m.shilo. All rights reserved.
//

import UIKit

class PhotoPresentationManager: NSObject, UIViewControllerTransitioningDelegate {
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return PhotoPresentationAnimator(duration: TimeInterval(UINavigationController.hideShowBarDuration), isPresenting: true)
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return PhotoPresentationAnimator(duration: TimeInterval(UINavigationController.hideShowBarDuration), isPresenting: false)
    }
}
