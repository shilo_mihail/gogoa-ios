//
//  PhotoPresentationAnimator.swift
//  GoGOA
//
//  Created by m.shilo on 11/10/2019.
//  Copyright © 2019 m.shilo. All rights reserved.
//

import UIKit

final class PhotoPresentationAnimator: NSObject {
    
    var duration: TimeInterval
    var isPresenting: Bool
    
    init(duration : TimeInterval, isPresenting : Bool) {
        self.duration = duration
        self.isPresenting = isPresenting
    }
}

extension PhotoPresentationAnimator: UIViewControllerAnimatedTransitioning {
    func transitionDuration(
        using transitionContext: UIViewControllerContextTransitioning?
        ) -> TimeInterval {
        return duration
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        
        guard let toController = transitionContext.viewController(forKey: .to) else { return }
        guard let fromController = transitionContext.viewController(forKey: .from) else { return }
        
        if isPresenting {
            transitionContext.containerView.addSubview(toController.view)
        }
        
        let currentController = isPresenting ? toController : fromController
        let animationDuration = transitionDuration(using: transitionContext)
        
        currentController.view.layoutIfNeeded()
        
        UIView.animate(
            withDuration: animationDuration,
            animations: {
                currentController.view.frame = fromController.view.frame
                if self.isPresenting {
                    if let photoController = currentController as? GGBasePhotoViewController {
                        photoController.backgroundView.alpha = 1
                        photoController.backBtn.alpha = 1
                        if let viewAndSendPhotoController = photoController as? GGViewAndSendPhotoViewController {
                            viewAndSendPhotoController.sendBtn.alpha = 1
                        }
                    }
                }
        }, completion: { finished in
            transitionContext.completeTransition(finished)
        })
    }
}

