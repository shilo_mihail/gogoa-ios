//
//  ActionSheetPresentationManager.swift
//  GoGOA
//
//  Created by m.shilo on 16/09/2019.
//  Copyright © 2019 m.shilo. All rights reserved.
//

import UIKit

class ActionSheetPresentationManager: NSObject, UIViewControllerTransitioningDelegate {
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return ActionSheetPresentationAnimator(duration: TimeInterval(UINavigationController.hideShowBarDuration), isPresenting: true)
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return ActionSheetPresentationAnimator(duration: TimeInterval(UINavigationController.hideShowBarDuration), isPresenting: false)
    }
}
