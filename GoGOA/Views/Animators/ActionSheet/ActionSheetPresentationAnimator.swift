//
//  ActionSheetPresentationAnimator.swift
//  GoGOA
//
//  Created by m.shilo on 16/09/2019.
//  Copyright © 2019 m.shilo. All rights reserved.
//

import UIKit

final class ActionSheetPresentationAnimator: NSObject {
    
    var duration: TimeInterval
    var isPresenting: Bool
    
    init(duration : TimeInterval, isPresenting : Bool) {
        self.duration = duration
        self.isPresenting = isPresenting
    }
}

extension ActionSheetPresentationAnimator: UIViewControllerAnimatedTransitioning {
    func transitionDuration(
        using transitionContext: UIViewControllerContextTransitioning?
        ) -> TimeInterval {
        return duration
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        
        guard let toController = transitionContext.viewController(forKey: .to) else { return }
        guard let fromController = transitionContext.viewController(forKey: .from) else { return }
        
        if isPresenting {
            transitionContext.containerView.addSubview(toController.view)
        }
        
        let currentController = isPresenting ? toController : fromController
        let animationDuration = transitionDuration(using: transitionContext)
        
        currentController.view.layoutIfNeeded()
        
        UIView.animate(
            withDuration: animationDuration,
            animations: {
                currentController.view.frame = fromController.view.frame
                if let actionSheetController = currentController as? GGActionSheetViewController {
                    actionSheetController.backgroundView.alpha = self.isPresenting ? 0.3 : 0
                    actionSheetController.actionSheetViewTopConstraint.constant = self.isPresenting ? -actionSheetController.actionSheetView.bounds.height : 0
                    actionSheetController.view.layoutIfNeeded()
                }
//                if self.isPresenting {
//                    if let actionSheetController = currentController as? GGActionSheetViewController {
//                        actionSheetController.backgroundView.alpha = 0.3
//                        actionSheetController.actionSheetViewTopConstraint.constant = -actionSheetController.actionSheetView.bounds.height
//                        actionSheetController.view.layoutIfNeeded()
//                    }
//                }
        }, completion: { finished in
            transitionContext.completeTransition(finished)
        })
    }
}
