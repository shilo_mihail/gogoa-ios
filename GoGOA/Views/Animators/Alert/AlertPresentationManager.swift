//
//  AlertPresentationManager.swift
//  GoGOA
//
//  Created by m.shilo on 13/09/2019.
//  Copyright © 2019 m.shilo. All rights reserved.
//

import UIKit

class AlertPresentationManager: NSObject, UIViewControllerTransitioningDelegate {
    
//    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
//        let presentationController = AlertPresentationController(presentedViewController: presented, presenting: presenting)
//        return presentationController
//    }
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return AlertPresentationAnimator(duration: TimeInterval(UINavigationController.hideShowBarDuration), isPresenting: true)
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return AlertPresentationAnimator(duration: TimeInterval(UINavigationController.hideShowBarDuration), isPresenting: false)
    }
}
