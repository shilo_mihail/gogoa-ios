//
//  HorizontalParallaxAnimator.swift
//  GoGOA
//
//  Created by m.shilo on 09/04/2019.
//  Copyright © 2019 m.shilo. All rights reserved.
//

import UIKit

class HorizontalParallaxAnimator: NSObject, UIViewControllerAnimatedTransitioning {
    
    var duration: TimeInterval
    var isPresenting: Bool
    var originFrame: CGRect
    var backgroundImageView: UIImageView
    
    init(duration : TimeInterval, isPresenting : Bool, originFrame : CGRect, backgroundImageView: UIImageView) {
        self.duration = duration
        self.isPresenting = isPresenting
        self.originFrame = originFrame
        self.backgroundImageView = backgroundImageView
    }
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return duration
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        guard let fromView = transitionContext.view(forKey: UITransitionContextViewKey.from) else { return }
        guard let toView = transitionContext.view(forKey: UITransitionContextViewKey.to) else { return }
        
        let container = transitionContext.containerView
        
        self.isPresenting ? container.addSubview(toView) : container.insertSubview(toView, belowSubview: fromView)
        
        let currentView = isPresenting ? toView : fromView
        
        toView.frame = isPresenting ?  CGRect(x: fromView.frame.width, y: 0, width: toView.frame.width, height: toView.frame.height) : toView.frame
        toView.alpha = isPresenting ? 0 : 1
        toView.layoutIfNeeded()
        
        UIView.animate(withDuration: duration, animations: {
            if self.isPresenting {
                currentView.frame = fromView.frame
                currentView.alpha = 1
                self.backgroundImageView.frame.origin.x -= toView.frame.width
            } else {
                currentView.frame = CGRect(x: toView.frame.width, y: 0, width: toView.frame.width, height: toView.frame.height)
                currentView.alpha = 0
                self.backgroundImageView.frame.origin.x += toView.frame.width
            }
        }, completion: { (finished) in
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
        })
    }
    
    
    
}
