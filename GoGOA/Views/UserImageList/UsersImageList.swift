//
//  UserImageList.swift
//  GoGOA
//
//  Created by m.shilo on 26/07/2019.
//  Copyright © 2019 m.shilo. All rights reserved.
//

import UIKit

class UsersImageList: UIView {
        
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var usersPikcStackView: UIStackView! {
        didSet {

        }
    }
    @IBOutlet weak var usersPickStackViewWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var infoLabel: UILabel!
    
    var tap: UITapGestureRecognizer!
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit() {
        Bundle.main.loadNibNamed(String(describing: UsersImageList.self), owner: self, options: [:])
        
        addSubview(contentView)
        
        contentView.translatesAutoresizingMaskIntoConstraints = false
        contentView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        contentView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        contentView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        contentView.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        
        tap = UITapGestureRecognizer(target: self, action: nil)
        addGestureRecognizer(tap)
    }

    
    func showUserViewsAndText(_ users: [GGUser]) {
        for subView in usersPikcStackView.subviews {
//            UIView.animate(withDuration: 1, animations: {
//                subView.isHidden = true
//            }) { _ in
                subView.removeFromSuperview()
//            }
        }
        
        let imageListWidth = users.count >= 3 ? 2 * usersPikcStackView.bounds.height : usersPikcStackView.bounds.height + CGFloat(users.count - 1) * usersPikcStackView.bounds.height / 2
        let needUsers = filterUsersWithIcon(users)
        
        for (i, user) in needUsers.enumerated() {
            let view = createUserView(index: i, user)
            view.tag = user.id
            
            var deletedView: UIView? = nil
            var isExistView = false
            for subView in usersPikcStackView.subviews {
                if subView.tag == user.id {
                    isExistView = true
                }
            }
            
//            if !isExistView {
//                deletedView = subView
//            }
            view.layer.zPosition = CGFloat(users.count - i - 1)
            usersPikcStackView.addArrangedSubview(view)
        }
        
        

        
        usersPickStackViewWidthConstraint.constant = imageListWidth
        
        setInfoLabelText(needUsers.count, needUsers.first!.name)
    }
    
    private func filterUsersWithIcon(_ users: [GGUser]) -> [GGUser] {
        var usersWithIcon: [GGUser] = []
        var usersWithoutIcon: [GGUser] = []
        for user in users {
            if usersWithIcon.count >= 3 {break}
            if user.photoIcon == nil {
                usersWithoutIcon.append(user)
            } else {
                usersWithIcon.append(user)
            }
        }
        switch usersWithIcon.count {
        case 0:
            usersWithIcon.append(contentsOf: usersWithoutIcon.prefix(3))
        case 1:
            usersWithIcon.append(contentsOf: usersWithoutIcon.prefix(2))
        case 2:
            usersWithIcon.append(contentsOf: usersWithoutIcon.prefix(1))
        default:
            break
        }
        return usersWithIcon
    }
    
    private func setInfoLabelText(_ usersCount: Int, _ firstUserName: String) {
        infoLabel!.text = "\(firstUserName) " + String.localizedStringWithFormat("people count go to a party".localized, usersCount - 1)
    }
    
    private func createUserView(index: Int, _ user: GGUser) -> UIView {
        let rect = CGRect(x: 0, y: 0, width: usersPikcStackView.bounds.height, height: usersPikcStackView.bounds.height)
        let view = UIView(frame: rect)
        let imageView = UIImageView(frame: rect)
        if let imageData = user.photoIcon?.smallPhotoBinary?.data {
            imageView.image = UIImage(data: imageData)
            view.addSubview(imageView)
        } else {
            print(user.iconHexColor)
            imageView.image = UIImage(UIColor(hex: user.iconHexColor))
            imageView.contentMode = .scaleAspectFill
            view.addSubview(imageView)
            
            let initials = Utils.parseFirstCharacters(user.name)
            let initialsLabel = UILabel(frame: CGRect(x: 0, y: 0, width: usersPikcStackView.bounds.height, height: usersPikcStackView.bounds.height))
            initialsLabel.textColor = .white
            initialsLabel.text = initials
            initialsLabel.textAlignment = .center
            
            view.addSubview(initialsLabel)
            //            initialsLabel.center = view.center
        }
        
        view.layer.cornerRadius = view.bounds.height / 2
        view.clipsToBounds = true
        view.layer.borderWidth = 1
        view.layer.borderColor = UIColor.clear.cgColor
        
        view.heightAnchor.constraint(equalToConstant: usersPikcStackView.bounds.height).isActive = true
        view.widthAnchor.constraint(equalToConstant: usersPikcStackView.bounds.height).isActive = true
        
        return view
    }
}
