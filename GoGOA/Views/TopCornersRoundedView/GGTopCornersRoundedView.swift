//
//  GGTopCornersRoundedView.swift
//  GoGOA
//
//  Created by m.shilo on 16.08.2020.
//  Copyright © 2020 m.shilo. All rights reserved.
//

import UIKit

class GGTopCornersRoundedView: UIView {
    
    var _cornerRadius: CGFloat!
    var cornerRadius: CGFloat {
        get {
            return _cornerRadius
        }
        set {
            _cornerRadius = newValue
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        roundCorners(corners: [.topLeft, .topRight], radius: cornerRadius)
    }
    
    private func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
}
