//
//  UIView+authorities.swift
//  GoGOA
//
//  Created by m.shilo on 06.07.2020.
//  Copyright © 2020 m.shilo. All rights reserved.
//

import UIKit

class GGAuthorityView: UIView {
    var roles: [GGRole] = [.USER, .TEMP_USER]
}
