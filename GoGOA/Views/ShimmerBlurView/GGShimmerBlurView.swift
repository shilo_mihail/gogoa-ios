//
//  GGShimmerView.swift
//  GoGOA
//
//  Created by m.shilo on 29.07.2020.
//  Copyright © 2020 m.shilo. All rights reserved.
//

import UIKit

class GGShimmerBlurView: UIVisualEffectView {

    var gradientColorOne : CGColor = UIColor(white: 0.85, alpha: 0).cgColor
    var gradientColorTwo : CGColor = UIColor(white: 0.95, alpha: 0.3).cgColor
    
    private var gradientLayer: CAGradientLayer?
    
    func startAnimating() {
        if self.layer.sublayers?.first(where: { $0 == gradientLayer }) == nil {
            addGradientLayer()
            let animation = addAnimation()
            gradientLayer!.add(animation, forKey: animation.keyPath)
        }
    }
    
    func stopAnimating() {
        gradientLayer?.removeAllAnimations()
        gradientLayer?.removeFromSuperlayer()
    }
    
    private func addGradientLayer() {
        
        gradientLayer = CAGradientLayer()
        
        gradientLayer!.frame = self.bounds
        gradientLayer!.startPoint = CGPoint(x: 0.0, y: 1.0)
        gradientLayer!.endPoint = CGPoint(x: 1.0, y: 1.0)
        gradientLayer!.colors = [gradientColorOne, gradientColorTwo, gradientColorOne]
        gradientLayer!.locations = [0.0, 0.5, 1.0]
        self.layer.addSublayer(gradientLayer!)
    }
    
    private func addAnimation() -> CABasicAnimation {
       
        let animation = CABasicAnimation(keyPath: "locations")
        animation.fromValue = [-1.0, -0.5, 0.0]
        animation.toValue = [1.0, 1.5, 2.0]
        animation.repeatCount = .infinity
        animation.duration = 0.9
        return animation
    }
    
    

}
