//
//  BorderButtonView.swift
//  birch-ios
//
//  Created by m.shilo on 03/10/2018.
//  Copyright © 2018 m.shilo. All rights reserved.
//

import UIKit

class GGRoundedButtonView: GGGradientView {
    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var label: UILabel! {
        didSet {
            label.textColor = .white
        }
    }
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var tap: UITapGestureRecognizer!
    

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit() {
        Bundle.main.loadNibNamed(String(describing: GGRoundedButtonView.self), owner: self, options: [:])
        
        addSubview(contentView)
        
        contentView.translatesAutoresizingMaskIntoConstraints = false
        contentView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        contentView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        contentView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        contentView.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        
        
        layer.cornerRadius = bounds.height / 2
        clipsToBounds = true
        directionAdapter = 1
        tap = UITapGestureRecognizer(target: self, action: nil)
        addGestureRecognizer(tap)
    }
    
//    func gestureRecognizerInit(target: Any?, selector: Selector?) {
//        tap = UITapGestureRecognizer(target: target, action: selector)
//        addGestureRecognizer(tap!)
//    }

}
