//
//  GGActionSheetView.swift
//  GoGOA
//
//  Created by m.shilo on 16/09/2019.
//  Copyright © 2019 m.shilo. All rights reserved.
//

import UIKit
import Photos
import RxSwift
import RxCocoa
import RxDataSources



class GGActionSheetView: UIView {
    
    typealias ViewModelType = GGActionSheetViewViewModel
    
    var viewModel: ViewModelType! {
        didSet {
            removePhotoBtn.rx.tap
                .bind(to: viewModel.input.removePhotoDidTap)
                .disposed(by: disposeBag)
            
            viewPhotoBtn.rx.tap
                .bind(to: viewModel.input.viewPhotoDidTap)
                .disposed(by: disposeBag)
            
            viewGalleryPhotoBtn.rx.tap
                .bind(to: viewModel.input.viewGalleryPhotoDidTap)
                .disposed(by: disposeBag)
            
            cancelBtn.rx.tap
                .bind(to: viewModel.input.closeDidTap)
                .disposed(by: disposeBag)
            
            viewModel.output.showViewRemoveItems
                .bind(to: rx.isUserPhotoExist)
                .disposed(by: disposeBag)
            
            photoCollectionView.rx.modelSelected(PHAsset.self)
                .bind(to: viewModel.input.viewQuickPhotoDidTap)
                .disposed(by: disposeBag)
            
            
            let dataSource = RxCollectionViewSectionedAnimatedDataSource<GGPhotoCellViewModelsSection>(configureCell: { dataSource, collectionView, indexPath, photoCellViewModel in
                
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: GGPhotoCollectionViewCell.self), for: indexPath) as! GGPhotoCollectionViewCell
                
                if let sectionModel = dataSource.sectionModels.first {
                    let viewModel = sectionModel.items[indexPath.row]
                    cell.viewModel = viewModel
                }
                
                return cell
            })
            
            dataSource.configureSupplementaryView = { dataSource, collectionView, kind, indexPath -> UICollectionReusableView in
                switch kind {
                case UICollectionView.elementKindSectionHeader:
                    
                    let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: String(describing: GGCameraCollectionReusableView.self), for: indexPath) as! GGCameraCollectionReusableView
                    headerView.setupSession()
                    return headerView
                    
                default:
                    return UICollectionReusableView()
                }
            }
            
            viewModel.output.photoCellViewModels
                .bind(to: photoCollectionView.rx.items(dataSource: dataSource))
                .disposed(by: disposeBag)
            
            swipeActionSheetView.rx.event
                .map { _ -> Void in }
                .bind(to: viewModel.input.closeDidTap)
                .disposed(by: disposeBag)
        }
    }
    
    @IBOutlet var contentView: UIView!
    
    
    var swipeActionSheetView = UISwipeGestureRecognizer()
    var contentViewTopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var topView: UIView! {
        didSet {
            topView.layer.cornerRadius = 15
            topView.clipsToBounds = true
        }
    }
    @IBOutlet weak var photoCollectionView: UICollectionView! {
        didSet {
            photoCollectionView.register(UINib(nibName: String(describing: GGPhotoCollectionViewCell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: GGPhotoCollectionViewCell.self))
            photoCollectionView.register(UINib(nibName: String(describing: GGCameraCollectionReusableView.self), bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: String(describing: GGCameraCollectionReusableView.self))
        }
    }
    @IBOutlet weak var stackViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var viewGalleryPhotoView: UIView!
    @IBOutlet weak var viewGalleryPhotoBtn: UIButton! {
        didSet {
            viewGalleryPhotoBtn.setTitle("Choose photo", for: .normal)
        }
    }
    
    @IBOutlet weak var viewPhotoView: UIView!
    @IBOutlet weak var viewPhotoBtn: UIButton! {
        didSet {
            viewPhotoBtn.setTitle("View photo", for: .normal)
        }
    }
    
    @IBOutlet weak var removeView: UIView!
    @IBOutlet weak var removePhotoBtn: UIButton! {
        didSet {
            removePhotoBtn.setTitleColor(.red, for: .normal)
            removePhotoBtn.setTitle("Remove photo", for: .normal)
        }
    }
    

    @IBOutlet weak var cancelBtn: UIButton! {
        didSet {
            cancelBtn.titleLabel?.text = "Cancel"
            cancelBtn.layer.cornerRadius = 15
            cancelBtn.clipsToBounds = true
        }
    }
    
    private let disposeBag = DisposeBag()
        
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit() {
        Bundle.main.loadNibNamed(String(describing: GGActionSheetView.self), owner: self, options: [:])
        
        addSubview(contentView)
        
        contentView.translatesAutoresizingMaskIntoConstraints = false
        contentViewTopConstraint = contentView.topAnchor.constraint(equalTo: topAnchor)
        contentViewTopConstraint.isActive = true
//        contentView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        contentView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        contentView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        contentView.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        
        swipeActionSheetView.direction = .down
        addGestureRecognizer(swipeActionSheetView)
    }

}
