//
//  GGActionSheetViewViewModel.swift
//  GoGOA
//
//  Created by m.shilo on 17/10/2019.
//  Copyright © 2019 m.shilo. All rights reserved.
//

import Foundation
import RxSwift
import Photos


class GGActionSheetViewViewModel: ViewModelProtocol {
    
    struct Input {
        let closeDidTap: AnyObserver<Void>
        let removePhotoDidTap: AnyObserver<Void>
        let viewPhotoDidTap: AnyObserver<Void>
        let viewGalleryPhotoDidTap: AnyObserver<Void>
        let viewQuickPhotoDidTap: AnyObserver<PHAsset>
    }
    struct Output {
        let close: Observable<Void>
        let photoCellViewModels: Observable<[GGPhotoCellViewModelsSection]>
        let showViewRemoveItems: Observable<Bool>
        let removePhoto: Observable<Void>
        let viewPhoto: Observable<Void>
        let viewGalleryPhoto: Observable<Void>
        let viewQuickPhoto: Observable<PHAsset>
    }
    
    let input: Input
    let output: Output
    
    //INPUT
    private let closeDidTapSubject = PublishSubject<Void>()
    private let removePhotoDidTapSubject = PublishSubject<Void>()
    private let viewPhotoDidTapSubject = PublishSubject<Void>()
    private let viewGalleryPhotoDidTapSubject = PublishSubject<Void>()
    private let viewQuickPhotoDidTapSubject = PublishSubject<PHAsset>()
    //OUTPUT
    private let closeSubject = PublishSubject<Void>()
    private let photoCellViewModelsSubject = ReplaySubject<[GGPhotoCellViewModelsSection]>.create(bufferSize: 1)
    private let showViewRemoveItemsSubject: BehaviorSubject<Bool>
    private let removePhotoSubject = PublishSubject<Void>()
    private let viewPhotoSubject = PublishSubject<Void>()
    private let viewGalleryPhotoSubject = PublishSubject<Void>()
    private let viewQuickPhotoSubject = PublishSubject<PHAsset>()
        
    private let disposeBag = DisposeBag()
    
//    var cellViewModels: [GGPhotoCollectionCellViewModel] = []
    
    
    init(_ photoAssetService: PhotoAssetService, _ isUserPhotoExist: Bool) {
        var cellViewModels: [GGPhotoCollectionCellViewModel] = []
        showViewRemoveItemsSubject = BehaviorSubject<Bool>(value: isUserPhotoExist)
        input = Input(closeDidTap: closeDidTapSubject.asObserver(),
                      removePhotoDidTap: removePhotoDidTapSubject.asObserver(),
                      viewPhotoDidTap: viewPhotoDidTapSubject.asObserver(),
                      viewGalleryPhotoDidTap: viewGalleryPhotoDidTapSubject.asObserver(),
                      viewQuickPhotoDidTap: viewQuickPhotoDidTapSubject.asObserver())
        output = Output(close: closeSubject.asObservable(),
                        photoCellViewModels: photoCellViewModelsSubject.asObservable(),
                        showViewRemoveItems: showViewRemoveItemsSubject.asObservable(),
                        removePhoto: removePhotoSubject.asObservable(),
                        viewPhoto: viewPhotoSubject.asObservable(),
                        viewGalleryPhoto: viewGalleryPhotoSubject.asObservable(),
                        viewQuickPhoto: viewQuickPhotoSubject.asObservable())
        
//        let targetSize = CGSize(width: GGPhotoCollectionViewCell.height(), height: GGPhotoCollectionViewCell.height())
        
        photoAssetService.fetchPhotoAssets()
            .map({ assets -> [GGPhotoCellViewModelsSection] in
                for (i, asset) in assets.enumerated() {
                    let cellViewModel = GGPhotoCollectionCellViewModel(photoAssetService, asset, id: i)
                    cellViewModels.append(cellViewModel)
                }
                return [GGPhotoCellViewModelsSection(items: cellViewModels)]
            })
            .bind(to: photoCellViewModelsSubject)
            .disposed(by: disposeBag)
        
        closeDidTapSubject
            .bind(to: closeSubject)
            .disposed(by: disposeBag)
        
        removePhotoDidTapSubject
            .bind(to: removePhotoSubject)
            .disposed(by: disposeBag)
        
        viewPhotoDidTapSubject
            .bind(to: viewPhotoSubject)
            .disposed(by: disposeBag)
        
        viewGalleryPhotoDidTapSubject
            .bind(to: viewGalleryPhotoSubject)
            .disposed(by: disposeBag)
        
        viewQuickPhotoDidTapSubject
            .bind(to: viewQuickPhotoSubject)
            .disposed(by: disposeBag)
    }

    
    deinit {
        closeDidTapSubject.on(.completed)
        removePhotoDidTapSubject.on(.completed)
        viewPhotoDidTapSubject.on(.completed)
        viewGalleryPhotoDidTapSubject.on(.completed)
        viewQuickPhotoDidTapSubject.on(.completed)
        
        closeSubject.on(.completed)
        photoCellViewModelsSubject.on(.completed)
        showViewRemoveItemsSubject.on(.completed)
        removePhotoSubject.on(.completed)
        viewPhotoSubject.on(.completed)
        viewGalleryPhotoSubject.on(.completed)
        viewQuickPhotoSubject.on(.completed)
    }
}
