//
//  AlertView.swift
//  GoGOA
//
//  Created by m.shilo on 04/04/2019.
//  Copyright © 2019 m.shilo. All rights reserved.
//

import UIKit


class GGAlertView: UIView {
    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var codeTextField: UITextField!
    @IBOutlet weak var okBtn: UIButton!
    
    var alertType: GGAlertType! {
        didSet {
            switch alertType! {
            case .error(let error):
                titleLabel.text = error.title
                codeTextField.isHidden = true
            case .code:
                titleLabel.text = "Введите код"
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit() {
        Bundle.main.loadNibNamed(String(describing: GGAlertView.self), owner: self, options: [:])

        addSubview(contentView)
        
        contentView.translatesAutoresizingMaskIntoConstraints = false
        contentView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        contentView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        contentView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        contentView.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        
        layer.cornerRadius = 15
        clipsToBounds = true
        
        
        
    }

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
